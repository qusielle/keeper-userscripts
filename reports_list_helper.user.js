// ==UserScript==
// @name         RuTracker.org Curator Topic Helper
// @namespace    http://tampermonkey.net/
// @version      0.3.1
// @description  Help with keepers topics in subforum!
// @author       Horo
// @namespace    https://gitlab.com/qusielle/keeper-userscripts
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/master/reports_list_helper.user.js
// @include      https://rutracker.*/forum/viewforum.php?f=1584*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=rutracker.net
// @resource     cat_forum_tree http://api.rutracker.cc/v1/static/cat_forum_tree
// @resource     forum_size http://api.rutracker.cc/v1/static/forum_size
// @grant        GM_getResourceText
// ==/UserScript==

(function() {
    var cat_forum_tree = JSON.parse(GM_getResourceText ("cat_forum_tree"));
    var forum_size = JSON.parse(GM_getResourceText ("forum_size"));
    var topic_names = new Array();
    var topic_urls = new Array();
    var forum_names = new Array();

    var found_count = 0;
    var not_found_count = 0;
    var bad_topics_count = 0;

    var start = 0;
    var searchId = "";

    var searchableStr = document.URL + '&';
    var domain = searchableStr.match (/\/\/([w\.]*[^\/]+)/i) [1];
    var cat = searchableStr.match (/[\?\&]cat=([^\&\#]+)[\&\#]/i);
    if (cat) cat = cat[1];

    function do_ajax(url, event_handler) {
        var handler_wrapper = function() {
            if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                event_handler.call(this);
            }
        }
        var req = new XMLHttpRequest();
        req.onreadystatechange = handler_wrapper;
        req.open('GET', url, true);
        req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
        req.send();
    }

    function add_line(topic_names, name, shortname, subforum_id){
        let found = topic_names.filter(topic => topic == "[Список] "+name);
        let count_releases = forum_size.result[subforum_id];
        if (found.length > 0)
        {
            $("#div-found").append($('<tr>', {id: `sub_id_${subforum_id}`}));
            found_count++;
        }
        else
        {
            if (count_releases){
                $("#div-not-found").append($('<tr>', {id: `sub_id_${subforum_id}`}));
                not_found_count++;
            }
        }

        $(`#sub_id_${subforum_id}`).append($('<td>', { style: "padding: 0px 3px;" }).append($('<a>', { href: "viewforum.php?f="+subforum_id, text: shortname })));
        $(`#sub_id_${subforum_id}`).append($('<td>', { style: "padding: 0px 3px;", text: name }));

        if (found.length == 0 && count_releases){
            $(`#sub_id_${subforum_id}`).append($('<td>', { style: "padding: 0px 3px;", text: count_releases[0]+" раздач в подразделе" }));
        }
    }

    function get_name(cat, forum_id, subforum_id){
        let name = subforum_id ?
            `${cat_forum_tree.result.c[cat]} » ${cat_forum_tree.result.f[forum_id]} » ${cat_forum_tree.result.f[subforum_id]}` :
        `${cat_forum_tree.result.c[cat]} » ${cat_forum_tree.result.f[forum_id]}`;

        name = name.replace('&#9917;', '⚽')
            .replace('&#127936;', '🏀')
            .replace('&#127954;', '🏒');
        forum_names.push(name);
        return name;
    }

    function run_analyze(){
        let cat_forum = cat_forum_tree.result.tree[cat];

        Object.keys(cat_forum).forEach(function callback(forum_id) {
            let name = get_name(cat, forum_id);
            let shortname = `${forum_id}`;
            add_line(topic_names, name, shortname, forum_id);

            Object.values(cat_forum[forum_id]).forEach(function callback(subforum_id) {
                let name = get_name(cat, forum_id, subforum_id);
                let shortname =  `${forum_id} > ${subforum_id}`;
                add_line(topic_names, name, shortname, subforum_id);
            });
        });

        for (let i = 0; i < topic_names.length; i++){
            let topic_name = topic_names[i].slice(9);
            let found = forum_names.filter(forum_name => forum_name == topic_name);

            if (found.length == 0 && !topic_name.includes("Подразделы форума и их хранители") && !topic_name.includes("Кандидаты"))
            {
                $("#bad-topics").append($('<tr>')
                                        .append($('<td>', {style: "padding: 0px 3px;" })
                                                .append($('<a>', { href: topic_urls[i], text: topic_name }))));
                bad_topics_count++;
            }
        }

        $('#div-found-span')[0].innerText += ` [${found_count} шт.]`;
        $('#div-not-found-span')[0].innerText += ` [${not_found_count} шт.]`;
        $('#bad-topics-span')[0].innerText += ` [${bad_topics_count} шт.]`;
    }

    function create_body(){
        $(".vf-table").before($('<table>', {class: 'forumline', id: 'topic-analyze'}));
        $("#topic-analyze").append($('<tr>'));
        $("#topic-analyze > tr").append($('<td>', {class: 'row4 bw_TRL pad_6'}));
        $("#topic-analyze > tr > td").append($('<select>', {
            id: 'select-cat',
            onchange:
            "$('#search-f-text')[0].value = '\"Список '+$('#select-cat')[0].options[$('#select-cat')[0].selectedIndex].text+'\"';"
            + "$('#search-f-form')[0].action += '&tpp=250&cat='+$('#select-cat')[0].value;"
            + "$('#search-f-form').submit();"
        }));
        if (cat){
            $("#topic-analyze > tr > td").append($('<br>'));
            $("#topic-analyze > tr > td")
                .append($('<div>', {class: 'sp-wrap'})
                        .append($('<div>', {class: 'sp-head folded'}).append($('<span>', {id: 'div-found-span', text: 'темы списков найдены'})))
                        .append($('<div>', {class: 'sp-body'}).append($('<table>', {id: 'div-found'})))
                       );
            $("#topic-analyze > tr > td")
                .append($('<div>', {class: 'sp-wrap'})
                        .append($('<div>', {class: 'sp-head folded'}).append($('<span>', {id: 'div-not-found-span', text: 'темы списков не найдены'})))
                        .append($('<div>', {class: 'sp-body'}).append($('<table>', {id: 'div-not-found'})))
                       );
            $("#topic-analyze > tr > td")
                .append($('<div>', {class: 'sp-wrap'})
                        .append($('<div>', {class: 'sp-head folded'}).append($('<span>', {id: 'bad-topics-span', text: 'найдена лишняя тема списков'})))
                        .append($('<div>', {class: 'sp-body'}).append($('<table>', {id: 'bad-topics'})))
                       );
        }
        $("#select-cat").append($('<option>', {value: 0, text: "выберите категорию"}));

        for(let i = 0; i < 50; i++){
            if(cat_forum_tree.result.c[i]){
                if (cat && i == cat)
                    $("#select-cat").append($('<option>', {value: i, text: cat_forum_tree.result.c[i], selected: ''}));
                else
                    $("#select-cat").append($('<option>', {value: i, text: cat_forum_tree.result.c[i]}));
            }
        }
    }

    function start_loading_topics(){
        let nm = '\"Список '+$('#select-cat')[0].options[$('#select-cat')[0].selectedIndex].text+'\"';
        let url = "https://"+domain+"/forum/search.php?f=1584&start="+start+"&nm="+nm;
        do_ajax(url, parce_page)
    }

    function parce_page(){
        if (this.status >= 400) {
            console.log('Returned ' + this.status + ': ' + this.responseText);
            return
        }

        if(searchId == ""){
            var jumps = $(this.responseText).find('.vBottom > .small > b > .pg');
            if(jumps.length > 0) {
                var aobj = jumps[jumps.length-1];
                var link = aobj+"&";
                searchId = link.match (/[\?\&]id=([^\&\#]+)[\&\#]/i)[1];
            }
        }

        let topics = $(this.responseText).find('.topictitle > .ts-text');
        if (topics.length > 0)
        {
            for (let i = 0; i < topics.length; i++){
                if($('#select-cat')[0].options[$('#select-cat')[0].selectedIndex].text != "Музыка" ||
                   !topics[i].innerText.includes("Музыкальное"))
                {
                    topic_names.push(topics[i].innerText);
                    topic_urls.push(topics[i].href);
                }
            }

            start += 50;

            let url = "https://"+domain+"/forum/search.php?id="+searchId+"&start="+start;
            if (searchId != "")
                do_ajax(url, parce_page);
            else
                run_analyze();
        }
        else
        {
            run_analyze();
        }
    }

    create_body();
    if (cat) start_loading_topics();
    BB.initSpoilers($("#topic-analyze"));
})();