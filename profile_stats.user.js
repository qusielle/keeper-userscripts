// ==UserScript==
// @name         RuTracker.org Keeper's stats in profile
// @version      1.4.10
// @description  Show seed and report statistics in the keeper's profile
// @description:ru Отображение статистики по отдаче и спискам в профиле хранителя
// @author       Sunni2
// @namespace    https://gitlab.com/qusielle/keeper-userscripts
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/master/profile_stats.user.js
// @supportURL   https://rutracker.org/forum/viewtopic.php?t=5373769
// @include      https://rutracker.*/forum/profile.php?mode=viewprofile*
// @include      https://rutracker.*/forum/viewtopic.php?t=*
// @include      https://rutracker.*/forum/viewtopic.php?p=*
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.widgets.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/extras/jquery.tablesorter.pager.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/widgets/widget-alignChar.min.js
// @require      reports_topic_stat.js
// @require      stat_page_table.js
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

var stat_topics = {
  'seed_stat': {
    'id': '5413252',
    'timeout': 24 * 60 * 60 * 1000,
  },
  'seed_stat_per_user': {
    'id': '5413252&start=30',
    'timeout': 24 * 60 * 60 * 1000,
  },
  'reports_stat': {
    'id': '5471390',
    'timeout': 12 * 60 * 60 * 1000,
  },
  'quarter_stat': {
    'id': '5758660',
    'timeout': 12 * 60 * 60 * 1000,
  },
};

var storage_prefix = 'stat'
var page_title_suffix = 'title'
var stats_table_id = 'stats-table'
var update_button_id = 'keeper-stats-get-data'
var whats_new_button_id = 'keeper-stats-whats-new-toggle'
var update_status_label_id = 'keeper-stats-update-status'
var plot_links_id = 'keeper-stats-plot-links'

const rotationDaysCount = 90;
const nowUTC = new Date(new Date().getTime() + new Date().getTimezoneOffset() * 60000);
var daysSinceReference = Math.ceil((nowUTC.getTime() - new Date(2021, 0, 31).getTime()) / (1000 * 60 * 60 * 24));
if (nowUTC.getHours() < 9) daysSinceReference--;
const todayIndex = daysSinceReference % rotationDaysCount;
var rotation_plots_url;


$.fn.nextUntilWithTextNodes = function (until) {
  var matched = $.map(this, function (elem, i, until) {
      var matched = [];
      while ((elem = elem.nextSibling) && elem.nodeType !== 9) {
          if (elem.nodeType === 1 || elem.nodeType === 3) {
              if (until && jQuery(elem).is(until)) {
                  break;
              }
              matched.push(elem);
          }
      }
      return matched;
  }, until);

  return this.pushStack(matched);
};


function do_ajax(url, event_handler) {
  var handler_wrapper = function() {
    if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
      event_handler.call(this);
    }
  }
  var req = new XMLHttpRequest();
  req.onreadystatechange = handler_wrapper;
  req.open('GET', url, true);
  req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
  req.send();
}


function get_user_id() {
  return $('#profile-uname').attr('data-uid');
}


function set_value(name, value) {
  GM_setValue(name, value);
}


function get_value(name, default_value=null) {
  return GM_getValue(name, default_value);
}


function set_stat(topic_name, value) {
  set_value([storage_prefix, topic_name].join('-'), value);
  set_value([storage_prefix, topic_name, 'date'].join('-'), Date.now());
  topic_title = $('#topic-title', value).text()
  set_value([storage_prefix, topic_name, page_title_suffix].join('-'), topic_title);
}


function get_stat(topic_name) {
  var value_date = get_stat_date_raw(topic_name);
  if (!value_date) return;

  return get_value([storage_prefix, topic_name].join('-'), null);
}


function get_stat_date_raw(topic_name) {
  return get_value([storage_prefix, topic_name, 'date'].join('-'), 0);
}


function get_stat_date_from_title(topic_name) {
  topic_title = get_value([storage_prefix, topic_name, page_title_suffix].join('-'), '');
  title_date = topic_title.match(/\[.+?\]/)
  if (!title_date.length) return '[No date]';

  return title_date[0] + ' ';
}


function set_stat_value(topic_name, user_id, key, value) {
  set_value([storage_prefix, topic_name, user_id, key].join('-'), value);
  set_value([storage_prefix, topic_name, user_id, key, 'date'].join('-'), Date.now());
}


function get_stat_value(topic_name, user_id, key) {
  var value_date = get_value([storage_prefix, topic_name, user_id, key, 'date'].join('-'), 0);
  if (value_date < get_stat_date_raw(topic_name)) return;

  return get_value([storage_prefix, topic_name, user_id, key].join('-'), null);
}


function find_user_spoiler_in_stat(topic_name, user_id) {
  key = 'spoiler';
  saved_spoiler = get_stat_value(topic_name, user_id, key);
  if (!saved_spoiler) {
    topic = get_stat(topic_name);
    let user_spoiler_element = $('#' + user_id, topic).next();
    if (user_spoiler_element.length) {
      $('.sp-head span:first', user_spoiler_element).prepend(get_stat_date_from_title(topic_name));
      set_stat_value(topic_name, user_id, key, user_spoiler_element[0].outerHTML);
      saved_spoiler = user_spoiler_element[0];
    }
  }
  return saved_spoiler;
}


function render_whats_new(stats_table) {
  function show_whats_new() {
    $('#whats_new').show()
    $('#' + whats_new_button_id)
      .click(hide_whats_new)
      .text('[Скрыть новости]')
    set_value('show_whats_new', true)
  }
  function hide_whats_new() {
    $('#whats_new').hide()
    $('#' + whats_new_button_id)
      .click(show_whats_new)
      .text('[Показать новости]')
    set_value('show_whats_new', false)
  }

  if (get_value('show_whats_new') == null) {
    show_whats_new()
  }
  var whats_new = $(find_user_spoiler_in_stat('reports_stat', 'news'))
  var page_news_date = (whats_new.find('.sp-head').text().match(/\d\d\.\d\d\.\d\d\d\d/) || [])[0]
  whats_new = stats_table.append(whats_new.attr('id', 'whats_new'))
  BB.initSpoilers(whats_new)

  if (page_news_date != get_value('whats_new_date') || get_value('show_whats_new')) {
    set_value('whats_new_date', page_news_date)
    show_whats_new()
  } else {
    hide_whats_new()
  }
}


function render_plot_links(user_id) {
  let user_spoiler = find_user_spoiler_in_stat('reports_stat', user_id)
  let two_years_plot = $('var.postImg[title*="' + user_id + '_2y.png"]:first', user_spoiler).attr('title')
  let all_years_plot = two_years_plot && two_years_plot.replace('_2y', '')
  let instant_plot = $('var.postImg[title*="' + user_id + '_latest.png"]:first', user_spoiler).attr('title')

  if (!two_years_plot) {
    let topic = get_stat('reports_stat');
    let latest_reference_url = $('var.postImg[title*="_latest.png"]:first', topic).attr('title')
    let reference_user_id = latest_reference_url.split('/').pop().split('_')[0]
    let two_years_reference_url = $('var.postImg[title*="_2y.png"]:first', topic).attr('title')
    instant_plot = latest_reference_url.replace(reference_user_id, user_id)
    two_years_plot = two_years_reference_url.replace(reference_user_id, user_id)
    all_years_plot = two_years_reference_url.replace(reference_user_id, user_id).replace('_2y', '')
  }

  if (!instant_plot) return

  $('#' + plot_links_id)
  .empty()
  .text('[ 📊: ')
  .append(
    $('<a>', {class: 'txtb', href: two_years_plot}).text('2 года'),
    ' / ',
    $('<a>', {class: 'txtb', href: all_years_plot}).text('весь'),
    ' || ',
    $('<a>', {class: 'txtb', href: instant_plot}).text('подразделы'),
    ' ]',
  )
}


function updatePlot(index) {
  var rotationIndex = todayIndex + index - rotationDaysCount;
  if (rotationIndex >= 0) {
    rotationIndex = rotationIndex % rotationDaysCount;
  } else {
    rotationIndex = (rotationDaysCount + rotationIndex) % rotationDaysCount;
  }
  const targetUrl = `${rotation_plots_url}/${String(rotationIndex).padStart(2, '0')}.png`;
  $('#instant_plot_img')
    .attr('src', targetUrl)
    .attr('alt', `График хранимого юзера. Если вы видите эту надпись, графика за эту дату нет.`)
}

function createScrollbar() {
  const scrollbar = $('<div id="plot_scrollbar"></div>').css({
      display: 'flex',
      alignItems: 'center',
      marginTop: '10px',
      overflowX: 'auto'
  });

  const leftButton = $('<button>&lt;</button>').click(function() {
      const currentIndex = parseInt($('#instant_plot_img').data('index')) || (rotationDaysCount - 1);
      const newIndex = Math.max(currentIndex - 1, 0);
      updatePlot(newIndex);
      $('#instant_plot_img').data('index', newIndex);
      $('#plot_scrollbar input[type="range"]').val(newIndex);
  });

  const rightButton = $('<button>&gt;</button>').click(function() {
      const currentIndex = parseInt($('#instant_plot_img').data('index')) || (rotationDaysCount - 1);
      const newIndex = Math.min(currentIndex + 1, rotationDaysCount - 1);
      updatePlot(newIndex);
      $('#instant_plot_img').data('index', newIndex);
      $('#plot_scrollbar input[type="range"]').val(newIndex);
  });

  const slider = $('<input type="range" min="0" max="' + (rotationDaysCount - 1) + '" value="' + (rotationDaysCount - 1) + '">').css({
      flexGrow: '1',
      margin: '0 10px'
  }).on('input', function() {
      const newIndex = parseInt($(this).val());
      updatePlot(newIndex);
      $('#instant_plot_img').data('index', newIndex);
  });

  scrollbar.append(leftButton, slider, rightButton);
  return scrollbar;
}

function render_stats(user_id) {
  var stats_table = $('#' + stats_table_id)
  if (!$('#' + stats_table_id).length) {
    stats_table = $('<table class="bordered w100 row1" id="' + stats_table_id + '"></table>').insertAfter('.user_profile');
  }
  stats_table.empty();

  for (const topic_name in stat_topics) {
    if (topic_name == 'quarter_stat' || topic_name == 'seed_stat') continue
    let user_spoiler = find_user_spoiler_in_stat(topic_name, user_id);
    if (user_spoiler) {
      var result_spoiler = stats_table.append(user_spoiler);
      BB.initSpoilers(result_spoiler);
    } else {
      stats_table.append('Статистика по ID ' + user_id + ' из ' + topic_name + ' не найдена.<br/>');
    }
  }

  let instant_plot_url = $('var.postImg[title*="' + user_id + '_latest.png"]:first').attr('title')
  if (instant_plot_url) {
    instant_plot_url = instant_plot_url.replace('_latest', '_oneline_latest')
    stats_table.prepend($('<img>', {
      id: "instant_plot_img",
      class: "postImg",
      alt: `One-line plot of kept subforums for user ${user_id}`,
      src: instant_plot_url,
    }))

    rotation_plots_url = instant_plot_url.substring(0, instant_plot_url.lastIndexOf('/')) + '/rotation'

    const scrollbar = createScrollbar();
    $('#instant_plot_img').before(scrollbar);
    // updatePlot(rotationDaysCount - 1);
    // $('#instant_plot_img').data('index', rotationDaysCount - 1);
  }

  render_whats_new(stats_table)
}


function set_update_button_text() {
  $('#' + update_button_id).text('[Обновить сейчас]')
}


function update_stat_storage(post_process_handler, force_update=true) {
  function update_button_text_progress(current) {
    $('#' + update_button_id).text(
      'Обновление ' + current + '/' + Object.keys(stat_topics).length + '...'
    );
  }
  var topics_processed = 0;

  if (force_update) update_button_text_progress(0);

  function increment_and_post_process() {
    topics_processed++;
    update_button_text_progress(topics_processed);
    if (topics_processed == Object.keys(stat_topics).length) {
      set_update_button_text();
      write_update_status_label();
      post_process_handler();
    }
  }

  for (const topic_name in stat_topics) {
    let topic = stat_topics[topic_name];
    if (!force_update && Date.now() - get_stat_date_raw(topic_name) < topic['timeout']) {
      increment_and_post_process();
      continue;
    }

    let handler = function() {
      console.log('Got ' + topic_name);
      let users = $('span:empty', this.responseText).filter(
        function(){return this.id.search(/^\d+$/) != -1;}
      )
      if (!users.length && topic_name != 'quarter_stat' && topic_name != 'seed_stat') {
        // TODO: print about failure
        console.log('Users are empty for ' + topic_name);
        return;
      }

      set_stat(topic_name, this.responseText);

      increment_and_post_process();
    }

    do_ajax('viewtopic.php?t=' + topic['id'], handler);
  }
}


function stat_storage_oldest_date() {
  min_date = 0;
  for (const topic_name in stat_topics) {
    current_date = get_stat_date_raw(topic_name);
    if (!min_date || current_date < min_date)
      min_date = current_date;
  }
  return min_date;
}


function write_update_status_label() {
  oldest_date = stat_storage_oldest_date();
  current_time = Date.now();
  var result_text = String();
  if (!oldest_date) {
    result_text = 'Хранилище пусто.'
  } else {
    days_delta = Math.floor((current_time - oldest_date) / 1000 / 60 / 60 / 24);
    hours_delta = Math.floor((current_time - oldest_date) / 1000 / 60 / 60);
    if (!days_delta) {
      result_text = 'Обновлено ' + hours_delta + 'ч назад.';
    } else {
      result_text = 'Обновлено ' + days_delta + 'д назад.';
    }
  }
  $('#' + update_status_label_id).text(result_text);
}


function init_keeper_features() {
  var update_stat_and_render = function(force_update=true) {
    update_stat_storage(function(){render_stats(user_id);}, force_update);
  }
  var user_id = get_user_id()
  $('#keeper-links').html(
    '<a class="txtb" href="search.php?uid=' + user_id + '&myt=1&f=1584">[Созданные темы в раб. подфоруме]</a></br> \
    <a class="txtb" href="search.php?uid=' + user_id + '&f=1584&dm=1">[Посты в рабочем подфоруме]</a></br> \
    <a class="txtb" href="search.php?uid=' + user_id + '&t=4275633&dm=1">[Поиск сводного отчета]</a></br> \
    <a class="txtb" href="search.php?uid=' + user_id + '&t=3130748&dm=1">[Поиск в теме неактивных]</a></br> \
    <a class="txtb" href="viewtopic.php?t=5413252&start=30#' + user_id +'">[Статистика по отдаче]</a></br> \
    <a class="txtb" href="viewtopic.php?t=5471390#' + user_id +'">[Статистика по спискам]</a></br> \
    <span style="font-size: 11px;" id="' + plot_links_id + '"></span></br> \
    <a class="txtb" id="' + whats_new_button_id + '" href="#"></a></br> \
    <a class="txtb" id="' + update_button_id + '" href="#"></a></br> \
    <span id="' + update_status_label_id + '" style="font-size: 11px;">Время последнего обновления</span></br> \
  ');
  $('#' + update_button_id).click(update_stat_and_render);
  set_update_button_text();
  write_update_status_label();
  if (stat_storage_oldest_date()) {
    render_stats(user_id);
  }
  render_plot_links(user_id);
  update_stat_and_render(false);
  set_value(get_user_id() + '-is-keeper', true);
}


function init_profile_elements() {
  $('#user-contacts tbody').append('<tr><th></th><td id="keeper-links"></td></tr>');
  if (
      $('div:not(#avatar-img).med.mrg_4').text().match('Хранитель')
      || get_value(get_user_id() + '-is-keeper')
  ) {
    init_keeper_features();
  } else {
    $('#keeper-links').html('<a href="#">Включить хранительские фичи</a>').click(init_keeper_features);
  }
}


(function() {
  'use strict';
  if (window.location.href.includes('/forum/profile.php?mode=viewprofile')) {
    init_profile_elements();
  } else if (window.location.href.includes('forum/viewtopic.php?t=5471390')){
    draw_initialization_button();
  } else {
    init_subforum_page_elements();
  }
})();
