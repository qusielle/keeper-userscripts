var stat_top_columns = [
  {
    key: '',
    title: '№',
    attrs: {'data-sorter': "false", 'data-filter': "false"},
    data_key: 'categories',
  },
  {
    key: 'nick_link',
    title: 'Ник',
    data_key: 'nick',
  },
  {
    key: 'size_text',
    title: 'Объем',
    data_key: 'size',
    sort: 1,
    align: "right",
    default: 0,
  },
  {
    key: 'size_diff_text',
    title: 'Объем +/-',
    data_key: 'size_diff',
    align: "right",
    default: 0,
  },
  {
    key: 'count',
    title: 'Кол-во',
    align: "right",
    default: 0,
  },
  {
    key: 'count_diff_text',
    title: 'Кол-во +/-',
    data_key: 'count_diff',
    align: "right",
    default: 0,
  },
  {
    key: 'dupl_line',
    title: 'Дубли',
    data_key: 'dupl_percent',
    default: 0,
  },
  {
    key: 'zero_line',
    title: 'Нули',
    data_key: 'zero_total',
    default: 0,
  },
  {
    key: 'unreg',
    title: 'Разрег',
    default: 0,
  },
  {
    key: 'updated',
    title: 'Обнов',
    default: 0,
  },
  {
    key: 'entry_date',
    title: 'Вступл.',
  },
  {
    key: 'login_date',
    title: 'Логин',
  },
  {
    key: 'upload_month_text',
    title: 'Отдача 30дн',
    data_key: 'upload_month',
    default: 0,
  },
  {
    key: 'upload_month_rare_text',
    title: 'Редкие 30дн',
    data_key: 'upload_month_rare',
    default: 0,
  },
]
var ignore_ids = ['40621895', '46790908']
var log_element = $('<div>', {id: 'keepers-reports-stat-log'})
var pager_html = `
<div class="pager forumline">
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/first.png" class="first"/>
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/prev.png" class="prev"/>
  <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/next.png" class="next"/>
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/last.png" class="last"/>
  <select class="pagesize" title="Количество строк на страницы">
    <option selected="selected" value="20">20</option>
    <option value="50">50</option>
    <option value="100">100</option>
    <option value="300">300</option>
    <option value="all">Все</option>
  </select>
  <select class="gotoPage" title="Номер страницы"></select>
</div>`

function pager_options() {
  return {
    // target the pager markup - see the HTML block below
    container: $(".pager"),

    // use this url format "http:/mydatabase.com?page={page}&size={size}"
    ajaxUrl: null,

    // process ajax so that the data object is returned along with the
    // total number of rows; example:
    // {
    //   "data" : [{ "ID": 1, "Name": "Foo", "Last": "Bar" }],
    //   "total_rows" : 100
    // }
    ajaxProcessing: function(ajax) {
        if (ajax && ajax.hasOwnProperty('data')) {
            // return [ "data", "total_rows" ];
            return [ajax.data, ajax.total_rows];
        }
    },

    // output string - default is '{page}/{totalPages}';
    // possible variables:
    // {page}, {totalPages}, {startRow}, {endRow} and {totalRows}
    output: '{startRow} по {endRow} ({totalRows})',

    // apply disabled classname to the pager arrows when the rows at
    // either extreme is visible - default is true
    updateArrows: true,

    // starting page of the pager (zero based index)
    page: 0,

    // Number of visible rows - default is 10
    size: 20,

    // if true, the table will remain the same height no matter how many
    // records are displayed. The space is made up by an empty
    // table row set to a height to compensate; default is false
    fixedHeight: true,

    // // remove rows from the table to speed up the sort of large tables.
    // // setting this to false, only hides the non-visible rows; needed
    // // if you plan to add/remove rows with the pager enabled.
    // removeRows: false,

    // css class names of pager arrows
    // next page arrow
    cssNext: '.next',
    // previous page arrow
    cssPrev: '.prev',
    // go to first page arrow
    cssFirst: '.first',
    // go to last page arrow
    cssLast: '.last',
    // select dropdown to allow choosing a page
    cssGoto: '.gotoPage',
    // location of where the "output" is displayed
    cssPageDisplay: '.pagedisplay',
    // dropdown that sets the "size" option
    cssPageSize: '.pagesize',
    // class added to arrows when at the extremes
    // (i.e. prev/first arrows are "disabled" when on the first page)
    // Note there is no period "." in front of this class name
    cssDisabled: 'disabled'
  }
}


function parse_spoiler_header(spoiler_header) {
  let header_regex = /^(?<nick>.+?) - (?<count>\d+)шт, (?<size>[\d \.]+)GB(?: \((?<additional>.+?)\))? (?<categories>[\d\|]+)/
  let header_match = header_regex.exec(spoiler_header)
  let safe_get = ((array, index, default_val) => array ? array[index]: default_val)

  if (!header_match) {
    let nick = /(.+?) - нет данных/.exec(spoiler_header)
    return {'nick': safe_get(nick, 1, '')}
  }

  let result = {
    nick: header_match.groups.nick,
    count: Number(header_match.groups.count),
    size: Number(header_match.groups.size.replace(' ', '')),
    size_text: header_match.groups.size + ' GB',
  }
  if (header_match.groups.additional) {
    result['dupl_line'] = safe_get(/⧉ \d+%(:[\d\-]+)?/.exec(header_match.groups.additional), 0, '')
    if (result['dupl_line'])
      result['dupl_percent'] = Number(safe_get(/⧉ (\d+)%:/.exec(result['dupl_line']), 1, 0))

    result['zero_line'] = safe_get(/(0>(\d+)д:(\d+)шт(?:, )?)+/.exec(header_match.groups.additional), 0, '')
    if (result['zero_line']) {
      result['zero_line'] = result['zero_line'].replace(/, $/, '');
      result['zero_total'] = Array.from(result['zero_line'].matchAll(/(\d+)шт/g), (m) => Number(m[1])).reduce((partialSum, a) => partialSum + a, 0);
    }

    result['unreg'] = Number(safe_get(/р:(\d+)шт/.exec(header_match.groups.additional), 1, 0))
    result['updated'] = Number(safe_get(/о:(\d+)шт/.exec(header_match.groups.additional), 1, 0))

    result['categories'] = header_match.groups.categories
  }

  return result
}


function parse_text_upload(upload_text) {
  var last_letter = upload_text.slice(-1)
  if (last_letter == '0') return 0

  var size = Number(upload_text.slice(0, -1))
  if (last_letter == 'K') {
    size /= 1024 * 1024
  } else if (last_letter == 'M') {
    size /= 1024
  } else if (last_letter == 'T') {
    size *= 1024
  } else if (last_letter == 'P') {
    size *= 1024 * 1024
  }
  return size
}


function parse_seed_stat(user_id) {
  if (typeof parse_seed_stat.topic_text == 'undefined') {
    log_element.html('Парсинг статистики отдачи...')
    parse_seed_stat.topic_text = $(get_stat('seed_stat'))
  }
  var user_line = $('pre a[href$="#' + user_id + '"]:first', parse_seed_stat.topic_text)
                  .prev().nextUntilWithTextNodes('br').text().trim()
  if (!user_line) return {}
  var result = {}

  var dates = Array.from(user_line.matchAll(/\d\d\.\d\d\.\d\d/g), (m) => m[0])
  result['entry_date'] = dates[0].replace(/(\d\d\.\d\d\.)(\d\d)/, '$120$2')
  if (dates[1]) result['login_date'] = dates[1].replace(/(\d\d\.\d\d\.)(\d\d)/, '$120$2')
  var uploads = Array.from(user_line.matchAll(/( +-?\d+[BKMGTP]| +0){4}/g), (m) => m[0])
  result['upload_month_text'] = Array.from(uploads[0].matchAll(/-?\d+[BKMGTP]|0/g), (m) => m[0])[0]
  result['upload_month_text'] = result['upload_month_text'] == 0 ? '' : result['upload_month_text']

  result['upload_month_rare_text'] = Array.from(uploads[0].matchAll(/-?\d+[BKMGTP]|0/g), (m) => m[0])[1]
  result['upload_month_rare_text'] = result['upload_month_rare_text'] == 0 ? '' : result['upload_month_rare_text']

  result['upload_month'] = parse_text_upload(result['upload_month_text'])
  result['upload_month_rare'] = parse_text_upload(result['upload_month_rare_text'])

  return result
}


function parse_quarter_stat(user_nick) {
  if (typeof parse_quarter_stat.message_text == 'undefined') {
    log_element.html('Парсинг квартальной статистики...')
    parse_quarter_stat.message_text = $(get_stat('quarter_stat'))
      .find('#top_keepers').nextUntilWithTextNodes('tbody').text().trim()
  }

  var user_nick_escaped = user_nick.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
  // [ "^17 * +36564ш", "^36    +33 843.2GB" ]
  var quarter_dynamic = Array.from(parse_quarter_stat.message_text.matchAll(
    new RegExp('\\d+\\. ?' + user_nick_escaped + ' +[\\d \.GBшт]+ \\(([^)]+)\\)' , 'g')),
    (m) => m[1]
  )
  if (!quarter_dynamic.length) return {}

  var result = {}

  var size_diff_full_line = quarter_dynamic.filter(m => m.endsWith('GB'))[0]
  var count_diff_full_line = quarter_dynamic.filter(m => !m.endsWith('GB'))[0]

  if (!size_diff_full_line) return {}

  var size_diff_line = size_diff_full_line.match(/([+\-][\d \.]+)GB/)
  if (size_diff_line) {
    result['size_diff'] = Number(size_diff_line[1].replace(' ', ''))
    if (result['size_diff']) result['size_diff_text'] = size_diff_line[1] + ' GB'
  }

  var count_diff_line = count_diff_full_line.match(/[+\-][\d]+/)
  if (count_diff_line) {
    result['count_diff'] = Number(count_diff_line[0])
    if (result['count_diff']) result['count_diff_text'] = count_diff_line[0]
  }

  return result
}


function parse_spoiler(spoiler) {
  var user_id = $(spoiler).prev().attr('id')
  if (ignore_ids.includes(user_id)) return undefined

  var header_dict = parse_spoiler_header($(spoiler).find('div.sp-head').text())
  var row = $('<tr>')
  var seed_stat_dict = parse_seed_stat(user_id)
  var quarter_stat_dict = parse_quarter_stat(header_dict['nick'])
  var stat_dict = Object.assign({}, header_dict, seed_stat_dict, quarter_stat_dict)

  stat_dict['user_id'] = user_id
  stat_dict['nick_link'] = $('<a>', {href: 'profile.php?mode=viewprofile&u=' + user_id})
    .text(stat_dict['nick'])

  for (var column in stat_top_columns) {
    let key = stat_top_columns[column]['key']
    let sort_value_key = stat_top_columns[column]['data_key']
    let default_value = stat_top_columns[column]['default']

    let value = stat_dict[key]
    let sort_value = stat_dict[sort_value_key]
    let cell = $('<td>')

    if (value) cell.html(value);
    if (sort_value) cell.attr('data-ts_text', sort_value);
    if (!value && typeof default_value !== undefined) {
      cell.attr('data-ts_text', default_value);
    }

    row.append(cell)
  }

  return row
}

function draw_update_status() {
  var update_stat_and_render = function(force_update=true) {
    update_stat_storage(function(){init_stat_page_table();}, force_update);
  }
  var target_post_element = $('#keepers-reports-stat-div')
  target_post_element.prepend($('<span>',
      {
        id: update_status_label_id,
        style: 'font-size: 11px; float: right;',
      }
    ).html('Время последнего обновления')
  )
  target_post_element
    .prepend($('<a>', {
      id: update_button_id,
      href: "javascript:void(0)",
      style: 'font-size: 11px; float: right;'
    }).click(update_stat_and_render))

  set_update_button_text();
  write_update_status_label();
}


function init_stat_page_table() {
  $('#keepers-reports-init-button').remove()
  $('#keepers-reports-stat-div').remove()
  var target_post_element = $('span#top')
  var table = $('<table>', {id: 'keepers-reports-stat', class: 'forumline'})
  target_post_element.after($('<div>', {id: 'keepers-reports-stat-div'}).append(table))
  table.before(
    $('<div id="keepers-reports-stat-category-filter">Категория (ID через пробел): <input class="search" type="search" data-column="0"></div>'))
  table.before($('<div>', {id: 'keepers-reports-stat-log'}).append(log_element))
  draw_update_status()

  log_element.html('Инициализация...')
  var all_spoilers = $($('#per_user').parents()[4]).prev().nextAll().find('span[id]').next('div[class="sp-wrap"]')
  var table_head = stat_top_columns.map(i => $('<th>', i['attrs'] || {}).html(i['title']))
  table.append($('<thead>').append($('<tr>').append(table_head)))
  var table_body = $('<tbody>')
  table.append(table_body)

  for (var i in all_spoilers.toArray()) {
    var row = parse_spoiler(all_spoilers[i])
    if (!row) continue
    row.attr('class', 'hl-tr')
    table_body.append(row)
    log_element.html(i + '/' + all_spoilers.length)
  }
  table.after($(pager_html))

  var sorting_columns = []
  for (var i in stat_top_columns) {
    var sorting_order = stat_top_columns[i]['sort']
    if (sorting_order == 1 || sorting_order == 0)
      sorting_columns.push([i, sorting_order])
  }

  var align_css = ''
  for (var i in stat_top_columns) {
    var align = stat_top_columns[i]['align']
    if (align) {
      var index = Number(i) + 1
      align_css += '#keepers-reports-stat tbody tr td:nth-child(' + index + ') { text-align: ' + align + '; } \n'
    }
  }
  if (align_css) {
    $('html > head').append($('<style>' + align_css + '</style>'))
  }

  table.tablesorter({
    widgets: ['filter', 'pager', 'alignChar'],
    sortList: sorting_columns,
    widthFixed: true,
    dateFormat: 'ddmmyyyy',
    textAttribute: 'data-ts_text',
    widgetOptions: {
      filter_external : '.search',
      filter_functions : {
        0 : function(e, n, f, i, $r, c, data) {
          let splitted = f.split(' ')
          for (let cat_id in splitted) {
            if (!e.includes('|' + splitted[cat_id] + '|')) return false
          }
          return true
        }
      },
    },
  })
  .tablesorterPager(pager_options())
  .bind("sortEnd filterEnd",function() { 
    var i = 1;
    table.find('tr:gt(1):not(.filtered)').each(function(){
        $(this).find("td:eq(0)").text(i);
        i++;
    });
  })
  .trigger("sortEnd")
  log_element.html('')
}

function draw_initialization_button() {
  var target_post_element = $('span#top')
  var init_button = $('<input>', {id: 'keepers-reports-init-button', value: 'Показать интерактивную таблицу', type: 'button'})
    .click(init_stat_page_table)
  target_post_element.after(init_button)
}

console.log('stat_page_table.js is included. 0')
