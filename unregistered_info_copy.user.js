// ==UserScript==
// @name         RuTracker.org Unregistered release info copy
// @version      0.0.2
// @description  Button for copying an unregistered release info to the clipboard
// @description:ru Кнопка для копирования информации о разрегистрированном релизе
// @author       Sunni2
// @namespace    https://gitlab.com/qusielle/keeper-userscripts
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/master/unregistered_info_copy.user.js
// @match        https://rutracker.org/forum/viewtopic.php*
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js
// @grant        GM_setClipboard
// ==/UserScript==

// Shortcut key to press together with Alt+Shift that triggers an info copying to clipboard.
ACCESS_KEY = 'z'

function set_elements() {
  last_status = $('i:contains("Последний статус") b').text()

  if (!last_status) return

  $('td.w100.vBottom.pad_2,.title-block').after('\
      <td style="padding: 4px; vertical-align:top"><input type="button" title="Alt+Shift+' + ACCESS_KEY + '" accesskey="' + ACCESS_KEY + '" id="unreg-copy-info" class="bold" style="width: 100%; font-family: Verdana,sans-serif; font-size: 11px;" value="Copy info"></input></td>\
      <td style="padding: 4px; vertical-align:top"><div><textarea id="unreg-copy-text" rows="7"  onfocus="storeCaret(this);" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" style="min-width: 300px;"></textarea></div></td>\
  ');

  source_forum = $('div.post_body:contains("Тема была перенесена из форума")').last().find('a').first()
  source_forum_link = source_forum.attr('href')
  source_forum_name = source_forum.text()

  archive_forum_name = $('.t-breadcrumb-top > a:last').text()

  res_text = archive_forum_name
      + ' [url=' + window.location.href + ']' + window.location.href.split('t=')[1] + '[/url] - '
      + last_status

      if (source_forum_link) {
      res_text += ' - из ' + '[url=' + source_forum_link + ']' + source_forum_name + '[/url]'
  }

  res_text += '\n'

  $('#unreg-copy-text').val(res_text)
  $('#unreg-copy-info').click(function(){
    GM_setClipboard($('#unreg-copy-text').val());
    $('#unreg-copy-info').val('Copied!');
  })
}

set_elements();
