
// ==UserScript==
// @name         Curator cur_active_seed in profile
// @namespace    http://tampermonkey.net/
// @version      0.3.3
// @description  try to take over the world!
// @author       Sunni2
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/master/profile_cur_active_seed.user.js
// @include      https://rutracker.*/forum/profile.php?mode=viewprofile*
// @resource     bootstrap_css https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css
// @require      https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js
// @resource     cat_forum_tree http://api.rutracker.cc/v1/static/cat_forum_tree
// @grant       GM_addStyle
// @grant       GM_getResourceText
// ==/UserScript==

var bsModal = $.fn.modal.noConflict();

GM_addStyle (GM_getResourceText ("bootstrap_css"));
GM_addStyle (GM_getResourceText ("bootstrap_table_css"));
var cat_forum_tree = JSON.parse(GM_getResourceText ("cat_forum_tree"));

function do_ajax(url, event_handler) {
  var handler_wrapper = function() {
    if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
      event_handler.call(this);
    }
  }
  var req = new XMLHttpRequest();
  req.onreadystatechange = handler_wrapper;
  req.open('GET', url, true);
  req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
  req.send();
}


function forum_ids_from_stat(user_id) {
  var stats_request_handler = function() {
    var all_forums = $('#' + user_id, this.responseText).next().text().match(/(.+) ?\(\d\d\.\d\d\.\d\d\): /g);
    for (const index in all_forums) {
      all_forums[index] = all_forums[index].replace(/ ?\(\d\d\.\d\d\.\d\d\): $/, '');
    }

    var catname_catid = {};
    for (var cat_id in cat_forum_tree['result']['f']){
      if (!catname_catid[cat_forum_tree['result']['f'][cat_id]]) {
        catname_catid[cat_forum_tree['result']['f'][cat_id]] = cat_id;
      } else {
        catname_catid[cat_forum_tree['result']['f'][cat_id]] += '/' + cat_id;
      }
    }

    var result_string = '';
    for (const key in all_forums) {
      if (all_forums.hasOwnProperty(key)) {
        result_string += ' ' + catname_catid[all_forums[key]];
      }
    }
    $('#curator-table-manual-ids').val(result_string);
    localStorage.setItem('curator-table-manual-ids-' + user_id, result_string);
    $('#curator-table-status').text('curator-table-manual-ids-' + user_id + ' - кука сохранена.');
  }
  do_ajax('viewtopic.php?t=5471390', stats_request_handler);
  $('#curator-table-status').text('Скачиваем стату...');
}

var g_stat_results = {};
var max_count = 0;
var current_date = 0;

function fill_table(user_id) {
  // $('#curator-table-status').text(JSON.stringify(g_stat_results));
  var all_ids = [];
  for (const date in g_stat_results) {
    for (const forum_id in g_stat_results[date]) {
      if (all_ids.indexOf(forum_id) == -1) all_ids.push(forum_id);
    }
  }

  var table_columns = [{field: 'date', title: 'Дата', width: '15%'}, ];
  // var table_data = [{date: 'Названия разделов'}];
  var table_data = [{date: ' '}];
  for (const index in all_ids) {
    table_columns.push({field: all_ids[index], title: all_ids[index]});
    table_data[0][all_ids[index]] = '<a href="profile.php?mode=keeper_stats&stats=cur_active_seed&u=' + user_id + '&f=' + all_ids[index] + '&s=10">' + cat_forum_tree['result']['f'][all_ids[index]] + '</a>';
  }
  for (const date in g_stat_results) {
    var sum_count = 0;
    var sum_size = 0;
    var cur_element = {};
    for (const forum_id in g_stat_results[date]) {
      cur_element[forum_id] = g_stat_results[date][forum_id]['count'] + 'шт, ' + g_stat_results[date][forum_id]['size'] + 'GB';
      sum_count += Number(g_stat_results[date][forum_id]['count']);
      sum_size += Number(g_stat_results[date][forum_id]['size']);
    }
    cur_element['date'] = date + '\n(' + sum_count + 'шт, ' + sum_size + 'GB)';
    table_data.push(cur_element);
  }

  $('#curator-table-main-table').bootstrapTable('refreshOptions', {
    columns: table_columns,
    data: table_data,
    striped: false,
  });
  $('.bootstrap-table').prop('style', 'width: 97vw;');

}

function request_cur_active_seed(user_id, forum_id, seeds=10) {
  url = 'profile.php?mode=keeper_stats&stats=cur_active_seed&u=' + user_id + '&f=' + forum_id + '&s=' + seeds;
  var handler = function(){
    var textarea = $('div textarea', this.responseText);
    var count = 0;
    var size = 0;
    if (textarea.length) {
      count = textarea.text().match(/Количество сидируемых раздач: \[b\](\d+)/);
      count = count && Number(count[1]) || 0;
      size = textarea.text().match(/Общий объём: \[b\](\d+)\s+GB\[\/b\]/);
      size = size && Number(size[1]) || 0;
    }
    g_stat_results[current_date][forum_id] = {count:count, size:size};
    $('#curator-table-status').text('Запрос ' + Object.keys(g_stat_results[current_date]).length + '/' + max_count + ': ' + 'подраздел ' + forum_id + ' (' + count + 'шт, ' + size + 'GB)');
    if (Object.keys(g_stat_results[current_date]).length == max_count) {
      localStorage.setItem('curator-g_stat_results-' + user_id, JSON.stringify(g_stat_results));
      $('#curator-table-status').text('curator-g_stat_results-' + user_id + ' - кука сохранена.');
      fill_table(user_id);
    }
  }
  do_ajax(url, handler);
}

function check_now(user_id) {
  var all_ids = $('#curator-table-manual-ids').val().match(/\d+/g);
  // g_stat_results = {};
  max_count = all_ids.length;
  // current_date = (new Date()).toISOString().replace('T', ' ').replace(/\..*$/, '');
  var d = new Date();
  current_date = d.getDate() + "/" + (d.getMonth()+1) + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
  g_stat_results[current_date] = {};

  all_ids.forEach(forum_id => {
    request_cur_active_seed(user_id, forum_id);
  });

}

function process_timer_change() {
  if ($('#curator-table-timer-checkbox:checked').length) {
    $("#curator-table-timer-textandinput").show();
  } else {
    $("#curator-table-timer-textandinput").hide();
  }
}

function get_personal_topic(user_id) {
  function render_topic_handler() {
    $('#personal-topic').append(
      $('.maintitle', this.responseText),
      $('#topic_main', this.responseText),
      $('#post-msg-form', this.responseText),
    )
    BB.initSpoilers($('#personal-topic'))
    if (BB.form_token) {
      $('form.tokenized').append('<input type="hidden" name="form_token" value="' + BB.form_token + '">');
     }
  }

  function search_result_handler() {
    user_topic_link = localStorage.getItem('curator-table-personal-table-' + user_id)
    if (!user_topic_link) {
      user_topic_link = $('a', this.responseText).filter(function(index) { return $(this).text().includes('[id: ' + user_id + ']'); }).attr('href')
      if (user_topic_link) {
        localStorage.setItem('curator-table-personal-table-' + user_id, user_topic_link)
      }
    }
    if (user_topic_link) {
      do_ajax(user_topic_link, render_topic_handler)
    } else {
      console.log('No user_topic_link')
      $('#personal-topic')
      .append(
        $('<h2>', {class: 'pagetitle'})
        .html(
          'Хранительское досье отсутствует. '
          + $('<a>', {
              href: 'posting.php?f=761&mode=newtopic',
              onclick: 'return confirm("Назовите тему так: \\n' + $('#profile-uname').text() + ' [id: ' + user_id + ']")',
            }).text('Создать?').prop('outerHTML')
        ),
        $('<span>').text(`Назовите тему так:   ${$('#profile-uname').text()} [id: ${user_id}]`),
      )
    }
  }

  put_after_anchor('<div id="personal-topic"></div>')
  do_ajax('viewforum.php?f=761&nm=id: ' + user_id, search_result_handler)
}

function put_after_anchor(element) {
  var anchor = $('#stats-table')
  if (!anchor.length) anchor = $('.user_profile')
  anchor.after(element)
}

function init_curator_features(user_id) {
  $('#curator-links').html('Тут может быть что-то кураторское');

  put_after_anchor('<table class="bordered w100 row1" id="curator-table"></table>');
  $('#curator-table').append(' \
  <div id="curator-table-controls"> \
  <input id="curator-table-manual-ids" type="text" placeholder="Перечень айди подфорумов" style="width:100%;"></input> \
  <div id="curator-table-buttons"><a id="curator-table-check-now" class="bold" style="white-space:nowrap; margin: 5px; width: 165px; font-family: Verdana,sans-serif;">Проверить сейчас</a><a id="curator-table-get-all-ids" class="bold" style="white-space:nowrap; margin: 5px; width: 165px; font-family: Verdana,sans-serif;">Получить подразделы из статы</a><a id="curator-table-clear-all" class="bold" style="white-space:nowrap; margin: 5px; width: 165px; font-family: Verdana,sans-serif;">Очистить и забыть таблицу</a> \
  <div id="curator-table-timer-controls" style="float: right;"><label style="white-space:nowrap; margin-right: 15px;"><input id="curator-table-timer-checkbox" value="0" type="checkbox">Авто обновление</label><div id="curator-table-timer-textandinput" style="float: right;">Один раз в <input id="curator-table-timer-textbox" type="text" size="2"> минут</div></div></div> \
  <div id="curator-table-status">&nbsp;</div> \
  <table id="curator-table-main-table"></table> \
  </div>');

  $('#curator-table-check-now').click(function(e) {e.preventDefault(); check_now(user_id);});
  $('#curator-table-get-all-ids').click(function(e) {e.preventDefault(); forum_ids_from_stat(user_id);});
  $('#curator-table-clear-all').click(function(e) {e.preventDefault(); g_stat_results = ''; localStorage.setItem('curator-g_stat_results-' + user_id, ''); $('#curator-table-main-table').bootstrapTable(); fill_table(user_id); });

  $('#curator-table-manual-ids').val(localStorage.getItem('curator-table-manual-ids-' + user_id)).focusout(function(){localStorage.setItem('curator-table-manual-ids-' + user_id, $(this).val()); $('#curator-table-status').text('curator-table-manual-ids-' + user_id + ' - кука сохранена.');});
  g_stat_results = JSON.parse(localStorage.getItem('curator-g_stat_results-' + user_id)) || {};

  $('#curator-table-timer-checkbox').attr('checked', localStorage.getItem('curator-table-timer-checkbox-' + user_id)).change(function() {
    localStorage.setItem('curator-table-timer-checkbox-' + user_id, $(this).attr('checked'));
    process_timer_change();
  });
  process_timer_change();


  $('#curator-table-main-table').bootstrapTable();
  $('.bootstrap-table').prop('style', 'max-width: 1200px;');
  fill_table(user_id);

}


(function() {
  'use strict';
  var user_id = $('#profile-uname').attr('data-uid');
  get_personal_topic(user_id);
  $('#user-contacts tbody').append('<tr><th></th><td id="curator-links"></td></tr>');
  if ($('div:not(#avatar-img).med.mrg_4').text().match('Хранитель')) {
    init_curator_features(user_id);
  } else {
    $('#curator-links').html('<a href="#">Включить кураторские фичи</a>').click(function() {init_curator_features(user_id)});
  }

})();
