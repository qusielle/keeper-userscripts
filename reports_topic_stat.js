var subforum_stat_body_id = 'reports-stats-subforum'
var subforum_stat_instant_plot_id = subforum_stat_body_id + '-plot'
var all_months = new Array()


function load_picture(all_months_index) {
  let year = all_months[all_months_index][0]
  let month = all_months[all_months_index][1]
  let target_img = $(
    'img[src*="/subforum/instant/plots/"]:first'
    + ',img[title*="/subforum/instant/plots/"].post-img-broken:first'
  )
  let url_containing_attr = 'src'
  if (target_img.hasClass('post-img-broken')) {
    url_containing_attr = 'title'
  }
  let base_url = target_img.attr(url_containing_attr).match(/^http.+\/(?=.+?\.png)/)
  if (!base_url.length) return;
  base_url = base_url[0]

  let month_str = (month < 10) ? '0' + month : String(month)
  let target_url = base_url + year + '-' + month_str + '.png'
  target_img.attr('src', target_url)
  $('.selected_month_plot').removeClass('selected_month_plot')
  target_img.removeClass('post-img-broken')
  $('#' + year + '-' + month).attr('class', 'selected_month_plot')
  $('#plot_slider').val(all_months_index)
}


function draw_year(year, min_month, max_month) {
  let months_element = $('<span>')
  months_element.append($(document.createTextNode(year +':  ')))
  min_month = min_month || 1
  max_month = max_month || 12
  let month_names = ['', 'янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']
  for (let month = 1; month <= max_month; month++) {
    let text = month_names[month]
    let element
    if (month >= min_month && month <= max_month) {
      let months_length = all_months.push([year, month])
      element = $('<a>', {href: 'javascript:void(0);', id: year + '-' + month})
        .text(text)
        .click(function() {load_picture(months_length - 1)})
    } else {
      // element = $(document.createTextNode(text))
    }
    months_element.append(element)
    months_element.append($(document.createTextNode(' ')))
  }
  return months_element
}


function init_subforum_page_elements() {
  if (!$('#topic-title').text().startsWith('[Список]')) return;
  if ($('a.pg:contains("Пред")').length) return;

  var first_post = $('.row1')[0];
  if (!first_post) return;

  if (
    !$('var[title*="/subforum/instant/plots/"]:first,img[src*="/subforum/instant/plots/"]:first')
    .before($('<div>', {id: 'plot-calendar'})).length
  ) return

  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth();
  for (let year = 2018; year <= currentYear; year++) {
    let min_month = (year == 2018) ? 3 : 1
    let max_month = (year == currentYear) ? currentMonth + 1 : 12
    $('#plot-calendar').append(draw_year(year, min_month, max_month))
  }
  $('html > head').append('<style>.selected_month_plot { color: limegreen !important; }</style>');

  let months_slider = $(
    '<input>',
    {id: 'plot_slider', type: 'range', min: '0', max: all_months.length - 1, step: 1, style: 'width: 95%;'}
  )
  .change(function() {
    let n = $(this).val();
    load_picture(n);
  })
  .val(all_months.length - 1)

  $('#plot-calendar').append(
    $('<div>').append(
      $('<a>', {href: 'javascript:void(0);', style: 'font-size: 150%;'}).text('< ').click(function() {
        load_picture(Math.max(0, months_slider.val() - 1))
      }),
      months_slider,
      $('<a>', {href: 'javascript:void(0);', style: 'font-size: 150%;'}).text(' >').click(function() {
        load_picture(Math.min(all_months.length - 1, months_slider.val() + 1))
      }),
    )
  )
}

console.log('reports_topic_stat.js is included. 0')
