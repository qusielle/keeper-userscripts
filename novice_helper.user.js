// ==UserScript==
// @name         Keepers Novice Helper
// @namespace    http://tampermonkey.net/
// @version      0.4.1
// @description  Help with keepers applications!
// @author       Horo
// @namespace    https://gitlab.com/qusielle/keeper-userscripts
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/master/novice_helper.user.js
// @include      https://rutracker.*/forum/viewtopic.php?t=3118460*
// @include      https://rutracker.*/forum/viewtopic.php?p=*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=rutracker.org
// @grant        none
// ==/UserScript==

(function() {
    var posts = new Array();
    var users = new Array();
    var reactions = new Array();
    var reactions_author = new Array();
    var curators = new Array();
    var counter = 0;

    if($('#topic-title')[0].innerText != "Правила и набор в группу «Хранители»") return;
    console.log("Keepers Novice Helper!");

    function do_ajax(url, event_handler) {
        var handler_wrapper = function() {
            if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                event_handler.call(this);
            }
        }
        var req = new XMLHttpRequest();
        req.onreadystatechange = handler_wrapper;
        req.open('GET', url, true);
        req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
        req.send();
    }

    function page_handler() {
        if (this.status >= 400) {
            console.log('Returned ' + this.status + ': ' + this.responseText);
            return
        }
        parce_page($(this.responseText));
    }

    function parce_page(responseText){
        var reply_nicks = responseText.find('#topic_main > tbody > tr > td > .nick > a')
        var reply_texts = responseText.find('#topic_main > tbody > tr > .message > .post_wrap > .post_body');
        for(let i = 0; i < reply_texts.length; i++){
            posts.push(reply_texts[i]);
            users.push(reply_nicks[i]);
        }
        counter++;
        if (counter == 4) analyze();
    }

    function analyze() {
        for(let i = 0; i < page_nicks.length; i++){
            var nick = page_nicks[i].innerText;

            if (!curators.includes(nick)) {
                var found = false;

                for(let j = 0; j < posts.length; j++){
                    if(posts[j].innerText.includes(nick)){
                        if(curators.includes(users[j].innerText)){
                            reactions.push(posts[j]);
                            reactions_author.push(users[j]);
                            found = true;
                            break;
                        }
                    }
                }
                if(!found){
                    reactions.push("NaN");
                    reactions_author.push("NaN");
                }
            }else{
                reactions.push("-");
                reactions_author.push("-");
            }
        }

        for(let i = 0; i < reactions.length; i++){
            if (reactions[i] != "-") {
                var text = reactions[i] != "NaN" ? reactions[i].innerHTML : "<span class='post-b'>Ответ куратора не найден!</span>";
                var style = reactions[i] != "NaN" ?
                    'border-color: #B8D199; background: #E4F2D4;' :
                    'border-color: #F6A992; background: #F2DBD4;';

                var id = '#'+page_texts[i].id;
                $(id).append($('<hr>', {class: 'post-hr'}));
                $(id).append($('<div>', {class: 'post-box-default curator'}));
                $(id + ' > .curator').append($('<div>', {class: 'post-box', style: 'border-color: transparent; background: transparent;'}));
                $(id + ' > .curator > div').append($('<span>', {class: 'post-b curator-name'}));
                $(id + ' > .curator > div > .curator-name').append(reactions_author[i].innerText);
                $(id).append($('<br>'));
                $(id).append($('<div>', {class: 'post-box-default annotation'}));
                $(id + ' > .annotation').append($('<div>', {class: 'post-box', style: style}));
                $(id + ' > .annotation > div').append(text);
            }
        }
    }

    function parse_curators(){
        if (this.status >= 400) {
            console.log('Returned ' + this.status + ': ' + this.responseText);
            return
        }
        var curlist = $(this.responseText).find('#p-84160866 a');
        for(let i = 0; i < curlist.length; i++){
            if(curlist[i].href.includes("mode=viewprofile&u=") &&
               curlist[i].innerText != "вакантно" &&
               !curators.includes(curlist[i].innerText)){
                curators.push(curlist[i].innerText);
            }
        }
        counter++;
        if (counter == 4) analyze();
    }

    var searchableStr = document.URL + '&';
    var domain = searchableStr.match (/\/\/([w\.]*[^\/]+)/i) [1];
    var start = searchableStr.match (/[\?\&]start=([^\&\#]+)[\&\#]/i);

    if (start?.length > 0){
        start = start[1];
    }else{
        var jumps = $('.vBottom > .small > b > .pg');
        var aobj = jumps[jumps.length-1];
        var link = aobj+"&";
        var next = link.match (/[\?\&]start=([^\&\#]+)[\&\#]/i)[1];
        start = Number(next) - 30;
    }
    console.log("page: " + start);

    var page_nicks = $('#topic_main > tbody > tr > td > .nick > a');
    var page_texts = $('#topic_main > tbody > tr > .message > .post_wrap > .post_body');
    for(let i = 0; i < page_texts.length; i++){
        posts.push(page_texts[i]);
        users.push(page_nicks[i]);
    }
    counter++;

    do_ajax("https://"+domain+"/forum/viewtopic.php?t=3352749", parse_curators);
    for(let i = Number(start) + 30; i < Number(start) + 70; i += 30){
        let url = "https://"+domain+"/forum/viewtopic.php?t=3118460&start="+i;
        do_ajax(url, page_handler);
    }

})();