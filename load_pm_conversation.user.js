// ==UserScript==
// @name         RuTracker.org Private message conversation loader
// @version      0.0.12
// @description  Load previous messages in the conversation
// @description:ru Подгрузка предыдущих сообщений в личной переписке
// @author       Sunni2 & Horо
// @namespace    https://gitlab.com/qusielle/keeper-userscripts
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/master/load_pm_conversation.user.js
// @supportURL   https://rutracker.org/forum/viewtopic.php?t=5373769
// @include      https://rutracker.*/forum/privmsg.php*
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==


$.fn.nextUntilWithTextNodes = function (until) {
  var matched = $.map(this, function (elem, i, until) {
      var matched = [];
      while ((elem = elem.nextSibling) && elem.nodeType !== 9) {
          if (elem.nodeType === 1 || elem.nodeType === 3) {
              if (until && jQuery(elem).is(until)) {
                  break;
              }
              matched.push(elem);
          }
      }
      return matched;
  }, until);

  return this.pushStack(matched);
};


function do_ajax(url, event_handler) {
  var handler_wrapper = function() {
    if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
      event_handler.call(this);
    }
  }
  var req = new XMLHttpRequest();
  req.onreadystatechange = handler_wrapper;
  req.open('GET', url, true);
  req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
  req.send();
}


function generate_hsl_color(string) {
  function hashCode(line) {
    var hash = 0,
      i, chr;
    if (line.length === 0) return hash;
    for (i = 0; i < line.length; i++) {
      chr = line.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  var lightness = 92.5
  var saturation = 100
  var hue = hashCode(string) % 360

  return 'hsl(' + hue + ',' + saturation + '%,' + lightness + '%)'
}


function set_topic_color(thead_element) {
  var message_topic = thead_element.find('td:contains("Тема:")')
  var topic_color = generate_hsl_color(message_topic.next().text().replace('Re: ', ''))
  message_topic.css('background', topic_color)
  message_topic.next().css('background', topic_color)
}

function add_message_link(thead_element, folder, id) {
  var time_line = thead_element.find('td:contains("Добавлено:")');
  time_line.next().wrapInner("<a href='./privmsg.php?folder="+folder+"&mode=read&p="+id+"'>");
}

function get_message_type(header) {
  var type = header.find("th").text().split(" :: ")[0];
  switch(type){
    case "Входящие":
      return "inbox";
    case "Отправлено":
      return "sentbox";
    case "Сохранено":
      return "savebox";
    case "Исходящие":
      return "outbox";
  }
}

function set_header_color(header, type) {
  var color = "";
  switch(type){
    case "inbox":
      color = "rgb(204, 230, 237)";
      break;
    case "outbox":
      color = "rgb(253, 221, 221)";
      break;
    case "savebox":
      color = "rgb(251, 251, 220)";
      break;
    case "sentbox":
      color = "rgb(186, 243, 185)";
      break;
  }
  header.find("th").css("background", color);
}

function render_messages(id_message, base_message_id) {
  console.log('render_messages')
  var sorted_keys = Object.keys(id_message)
  sorted_keys.reverse()
  for (var i in sorted_keys) {
    var reply_id = sorted_keys[i]
    var message_form = id_message[reply_id]
    message_form.find('.catBottom.pad_4').hide()

    var thead = message_form.children('thead')
    var header = thead.find("tr").eq(0)
    thead.find("a").eq(2).remove()
    thead.find("a").eq(1).remove()
    set_topic_color(thead);
    var type = get_message_type(header);
    set_header_color(header, type)
    add_message_link(thead, type, reply_id)

    var tbody = message_form.children('tbody')
    tbody.find("td").eq(0).css('vertical-align', 'top');
    var message_content = $('<table>', {id: 'body'+reply_id, style: 'width: 100%; vertical-align:top;'}).append(tbody)
    BB.initSpoilers(message_content);
    BB.initPostImages(message_content);

    var row = $('<tr>', {id: 'reply'+reply_id})
    var cell1 = $('<td>', {class: 'row4', style: 'padding: 0px; vertical-align:top; width: 230px;'}).append(thead)
    var cell2 = $('<td>', {class: 'row4', style: 'padding: 0px; vertical-align:top;'}).append(message_content)
    row.append(cell1, cell2)

    if (reply_id < base_message_id) {
      $('#previous-replies-anchor').append(header)
      $('#previous-replies-anchor').append(row)
    } else {
      $('#future-replies-anchor').append(header)
      $('#future-replies-anchor').append(row)
    }

    var heightExp = $('#reply'+reply_id)[0]?.clientHeight - 1;
    $('#body'+reply_id).css('min-height', (heightExp+'px'));
  }
  $('#status-log').text('Сообщения подгружены')
}


function init_read_message_page_elements() {
  var self_profile_url = $('#logged-in-username').attr('href')
  var partner_profile_url = $('thead a.med[href*="forum/profile.php?mode=viewprofile"]:not([href="' + self_profile_url + '"]):first').attr('href')
  var base_message_url = window.location.href

  var partner_id = partner_profile_url.split('u=')[1]
  var base_message_id = $('form input[type="hidden"][name="mark[]"]').attr('value')
  $('.nav.w100').append($('<span>', {id: 'status-log', style: 'padding-left: 3px; color: grey; font-weight: normal;'}))
  var mainBody = $('.forumline');
  mainBody.before($('<table>', {id: 'future-replies-anchor', class: 'forumline'}))
  if($('#post-msg-form').length){
    $('#post-msg-form').after($('<table>', {id: 'previous-replies-anchor', class: 'forumline'}))
  }else{
    mainBody.after($('<table>', {id: 'previous-replies-anchor', class: 'forumline'}))
  }
  set_topic_color(mainBody.find('thead'))
  var header = mainBody.find('thead').find("tr");
  var type = get_message_type(header);
  set_header_color(header, type)

  var id_message = {}
  var pm_left = 0

  function pm_page_handler() {
    if (this.status >= 400) {
      $('#status-log').text('Невозможно получить страницу (' + this.status + '): ' + this.responseURL)
      console.log('Returned ' + this.status + ': ' + this.responseText);
      return
    }

    $('#status-log').text('Парсим: ' + this.responseURL + ' (осталось ' + pm_left + 'шт)')
    var reply_id = $(this.responseText).find('form input[type="hidden"][name="mark[]"]').attr('value')
    var reply_form = $(this.responseText).find('.forumline')
    console.log(reply_form)
    id_message[reply_id] = reply_form
    pm_left--
    if (!pm_left) {
      render_messages(id_message, base_message_id)
    }
  }

  function pm_list_handler() {
    if (this.status >= 400) {
      $('#status-log').text('Невозможно получить страницу (' + this.status + '): ' + this.responseURL)
      console.log('Returned ' + this.status + ': ' + this.responseText);
      return
    }

    $('#status-log').text('Парсим: ' + this.responseURL + ' (осталось ' + pm_left + 'шт)')
    console.log($(this.responseText).find('div.pm-nick-wrap a[href$="u=' + partner_id + '"]'))
    $(this.responseText).find('div.pm-nick-wrap a[href$="u=' + partner_id + '"]').parents('.pm-nick-td').prev().find('a')
      .each(function() {
        reply_url = $(this).attr('href')
        console.log(reply_url, base_message_url)
        if (base_message_url.endsWith(reply_url)) {
          return
        }
        do_ajax(reply_url, pm_page_handler)
        pm_left++
        $('#status-log').text(' (осталось ' + pm_left + 'шт)')
      })
    pm_left--
    $('#status-log').text(' (осталось ' + pm_left + 'шт)')
  }

  pm_folders = ['inbox', 'sentbox', 'savebox', 'outbox']
  for (i in pm_folders) {
    folder = pm_folders[i]
    do_ajax('privmsg.php?folder=' + folder + '&filter_uid=' + partner_id, pm_list_handler)
    pm_left++
  }
}


(function() {
  'use strict';
  init_read_message_page_elements()
})();