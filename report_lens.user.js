// ==UserScript==
// @name         RuTracker.org Report Lens
// @version      1.2.1
// @description  Interact with reports like never before
// @description:ru Улучшенное взаимодействие с отчетами
// @author       Sunni2
// @namespace    https://gitlab.com/qusielle/keeper-userscripts
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/use-keeper-report-service/report_lens.user.js
// @supportURL   https://rutracker.org/forum/viewtopic.php?t=5373769
// @include      https://rutracker.*/forum/viewtopic.php?t=*
// @include      https://rutracker.*/forum/viewtopic.php?p=*
// @include      https://rutracker.*/forum/viewforum.php?f=*
// @include      https://rutracker.*/forum/profile.php?mode=viewprofile*
// @connect      api.rutracker.cc
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.widgets.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/extras/jquery.tablesorter.pager.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/widgets/widget-alignChar.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js
// @require      https://gitlab.com/qusielle/keeper-userscripts/-/raw/use-keeper-report-service/resources/widget-build-table.js
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// ==/UserScript==

try {var bsModal = $.fn.modal.noConflict();} catch (e) {}

$.fn.nextUntilWithTextNodes = function (until) {
  var matched = $.map(this, function (elem, i, until) {
      var matched = [];
      while ((elem = elem.nextSibling) && elem.nodeType !== 9) {
          if (elem.nodeType === 1 || elem.nodeType === 3) {
              if (until && jQuery(elem).is(until)) {
                  break;
              }
              matched.push(elem);
          }
      }
      return matched;
  }, until);

  return this.pushStack(matched);
};


function do_ajax(url, event_handler, responseType, request_method='GET') {
  var handler_wrapper = function() {
    if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
      event_handler.call(this);
    }
  }
  var req = new XMLHttpRequest();
  req.onreadystatechange = handler_wrapper;
  req.open(request_method, url, true);
  req.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
  if (responseType) {
    req.responseType = responseType;
  }
  req.send();
}


// Needed for CORS requests.
function do_ajax_gm(url, event_handler, responseType, request_method) {
  var handler_wrapper = function(response) {
    if (response.readyState == XMLHttpRequest.DONE) {
      event_handler.call(response);
    }
  }
  GM_xmlhttpRequest({
    method: request_method,
    url: url,
    onload: handler_wrapper,
    responseType: responseType,
  });
}


function set_value(name, value) {
  GM_setValue(name, value);
}


function get_value(name, default_value=null) {
  return GM_getValue(name, default_value);
}


function parse_date(date_str) {
  if (!date_str) {
    return
  }
  months = {
    'Янв': '01', 'Фев': '02', 'Мар': '03', 'Апр': '04', 'Май': '05', 'Июн': '06',
    'Июл': '07', 'Авг': '08', 'Сен': '09', 'Окт': '10', 'Ноя': '11', 'Дек': '12'
  }
  return Date.parse(
    date_str
    .replace(/[^\d-: ]{3}/, m => months[m])
    .replace(/.*(\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d).*/, '20$3-$2-$1 $4:$5')
  )
}


function format_date(date_obj, sep='-') {
  yyyy = date_obj.getFullYear().toString()
  mm = (date_obj.getMonth() + 1).toString()
  dd = date_obj.getDate().toString()

  return (dd[1]?dd:"0"+dd[0]) + sep + (mm[1]?mm:"0"+mm[0]) + sep + yyyy
}


function human_file_size(bytes, si=false, dp=1, show_sign=false) {
  const thresh = si ? 1000 : 1024;
  var gb = bytes / (thresh ** 3);
  return ((show_sign && bytes > 0) ? '+' : '') + gb.toFixed(dp) + 'GB';
}


// -----------------------------------------------------------------------------
//     Batch download functions
// -----------------------------------------------------------------------------
var zip_file, download_count, torrent_files;
var zip = null;

function add_torrent(url, number, parent_element){
  event_handler = function() {
    download_count++;
    parent_element.find('.torrent-download-button').attr('value', `Загрузка… ${number - download_count} / ${number}`);
    var tor_id = url.split('=')[1];

    resp_text = (new TextDecoder('windows-1251')).decode(this.response);

    if (this.status == 200 && !resp_text.startsWith('<center>')) {
        console.log('Success: ' + tor_id);
        torrent_files++;
        if (!zip) {
          zip = new JSZip();
        }
        zip.file(tor_id + '.torrent', this.response, {binary: true});
    } else {
      console.log('Fail: ' + tor_id + ' ' + this.status + ' ' + resp_text);
      $('.table-columns-checkboxes')
      .append(document.createTextNode(
          tor_id + ': Error - ' + this.status + ' ' + resp_text.replace(/(<.+?>)+/g, ' ') + '\n'
      ));
    }

    if (download_count == number) save_zip(parent_element);
  }
  do_ajax(url, event_handler, 'arraybuffer', 'POST');
}

function save_zip(parent_element){
  var user_id = parent_element.find('div[user_id]').attr('user_id')
  var subforum_id = parent_element.find('div[subforum_id]').attr('subforum_id')
  var file_name = `torrents_${user_id}_${subforum_id}_[${torrent_files}].zip`;

  zip.generateAsync({type:'blob'}).then(function(zip_file) {
    saveAs(zip_file, file_name);
    parent_element.find('.torrent-download-button')
      .attr('disabled', false)
      .attr('value', 'Скачать показанные .torrent файлы');
  });
  parent_element.find('.torrent-download-button').attr('value', 'Генерация архива...');
}
// -----------------------------------------------------------------------------



function process_report_entry(kept_entry, keepers_user_data_item) {
  is_excluded = (keepers_user_data_item) ? false : true
  is_candidate = !is_excluded && keepers_user_data_item[1]
  is_in_main_group = !is_excluded && !keepers_user_data_item[1]

  nick = (is_excluded) ? kept_entry.keeper_id : $("<textarea/>").html(keepers_user_data_item[0]).text()
  if (is_excluded) nick += ' (не в группе)'
  else if (is_candidate) nick += ' (кандидат)'

  var last_update_time = new Date(kept_entry.last_update_time)
  var last_seeded_time = new Date(kept_entry.last_seeded_time)
  var actual_date = new Date(Math.max(last_update_time, last_seeded_time))

  var first_update_time = new Date(kept_entry.first_update_time)
  // var first_seeded_time = new Date(kept_entry.seeding_history[0] * 1000)
  // var first_appeared = new Date(Math.min(first_seeded_time, first_update_time))

  var reg_time = new Date(kept_entry.reg_time)

  is_downloading = kept_entry.status & downloading
  is_imported_from_forum = kept_entry.status & imported_from_forum
  is_reported_by_tlo = kept_entry.status & reported_by_api && ! (is_imported_from_forum)

  row = $('<tr>', {class: 'row5'})
  cellStyle = 'border-bottom-width: 0px; border-top-width: 0px; background: #e7e7e7; white-space: nowrap;'
  nick_link = $('<a>', {href: `profile.php?mode=viewprofile&u=${kept_entry.keeper_id}`}).text(nick)
  nick_cell = $('<td>', {style: cellStyle}).append(nick_link)
  report_date_cell = $('<td>', {style: cellStyle})
  status_cell = $('<td>', {style: cellStyle})
  seeding_history_cell = $('<td>', {class: 'additional-column'})

  row.append(nick_cell, report_date_cell, status_cell, seeding_history_cell)
  statuses = []

  if (actual_date < reg_time) {
    row.css('color', 'gray')
    statuses.push('Прошлая версия')
  }

  if (is_downloading) {
    row.css('color', 'brown')
    statuses.push('Качает')
  } else {
    statuses.push('Хранит')
  }

  if (is_reported_by_tlo) {
    statuses.push('TLO')
  } else {
    statuses.push('по сидированию')
  }
  report_date_cell.append(
    $('<span>')
    .text(format_date(actual_date))
    .attr('title', `Первый отчет: ${format_date(first_update_time)}`)
  )
  status_cell.html(statuses.join(' | '))
  seeding_history_cell
    .html(prepare_seeding_history(kept_entry.seeding_history))
    .attr('style', 'background-color: black !important; color: DarkTurquoise; text-align: left; white-space: nowrap;')

  return row
}


function draw_average_seeds(kept_entries) {
  var average_seeds_per_day = merge_average_seeds(kept_entries[0].average_seeds_count, kept_entries[0].average_seeds_sum)
  var [total_average, total_average_str, detail_line] = prepare_average_seeds_details(average_seeds_per_day)
  var result_lines = []
  for (const day_period of [7, 14, 30]) {
    var average = period_average_seeds(
      kept_entries[0].average_seeds_count, kept_entries[0].average_seeds_sum, day_period)
    average = (average == null) ? 'N/A' : average.toFixed(2)
    var element = $('<li>').append($('<span>').html(`${day_period} дней: <b>${color_average_seeds(average)}</b>`))
    result_lines.push(element)
  }

  var target_element = $('#seeder-last-seen').parent()
  if (!target_element.length)
    target_element = $('.seed').first().parent().parent()

  target_element.append(
    $('<div>', {class: 'mrg_4 pad_4'}).append(
      'Средние сиды:  ',
      $('<ul>', {class: 'inlined middot-separated'}).append(
        result_lines
      ).attr('title', detail_line)
    )
  )
}


function draw_endangered_release_status(kept_entries) {
  var average_seeds_per_day = merge_average_seeds(kept_entries[0].average_seeds_count, kept_entries[0].average_seeds_sum)
  var average_keepers_per_day = new Array(average_seeds_per_day.length).fill(0);

  for (entry of kept_entries) {
    var seeding_history = entry.seeding_history
    for (let day = 0; day < average_seeds_per_day.length; day++) {
      average_keepers_per_day[day] += get_seeded_hours_of_day(seeding_history[day]) / 24
    }
  }

  var result_lines = []
  for (const day_period of [7, 14, 30]) {
    var sum_avg = 0
    var sum_avg_keepers = 0
    for (let day = 0; day < day_period; day++) {
      if (average_seeds_per_day[day] == null) continue
      sum_avg += average_seeds_per_day[day]
      sum_avg_keepers += average_keepers_per_day[day]
    }
    var keeper_avg_part = Math.floor(Math.min(sum_avg_keepers / sum_avg, 1.0) * 100)

    var element = $('<li>').append($('<span>').html(`${day_period} дней: <b>${keeper_avg_part}%</b>`))
    result_lines.push(element)
  }

  var target_element = $('#seeder-last-seen').parent()
  if (!target_element.length)
    target_element = $('.seed').first().parent().parent()

  target_element.append(
    $('<div>', {class: 'mrg_4 pad_4'}).append(
      'Вклад хранителей в сс:  ',
      $('<ul>', {class: 'inlined middot-separated'}).append(
        result_lines
      )
    ).attr('title',
      'Примерный показатель, какой процент от средних сидов составляют хранители.'
      + '\nМожет использоваться для определения, как часто на раздаче есть кто-то кроме хранителей.'
      + '\nВажно: в случае ухода хранителя, процент станет ниже на некоторый срок.'
    )
  )
}


function draw_keeping_info_table(api_response, keepers_user_data) {
  height = $('#t-tor-stats')[0].clientHeight - 25;

  // release_date = parse_date($('.attach td ul li:not(:has(*)):first').text())
  result_table = $('<table>', {
    id: 'keepers-table', class: 'forumline',
    style: 'border-bottom:0px; min-height: unset !important;'
  })
  if (!api_response['result'] || !api_response['result'].length) {
    $('#status-log').text('Раздача отсутствует в отчетах.')
    api_request(
      `/releases/pvc`,
      {
        topic_ids: `${target_release_id}`,
        columns: 'average_seeds_count,average_seeds_sum',
      },
      function(response) {
        var kept_entries = column_response_to_objects(response, 'releases')
        if (!kept_entries[0]) return
        draw_average_seeds(kept_entries)
      }
    )
    return
  }

  result_table.append($('<tr>', {style: 'height: 21px;'}).append(
    $('<td>', {class: 'catTitle'}).text('Ник'),
    $('<td>', {class: 'catTitle'}).text('Дата'),
    $('<td>', {class: 'catTitle'}).text('Статус')
      .append($('<span>', {class: 'vf-t-float-icon', style: 'cursor: pointer;'})
        .text('>>').click(function(){$('.additional-column').toggle()})),
    $('<td>', {class: 'catTitle additional-column'}).text('История сидирования'),
  ))

  var kept_entries = column_response_to_objects(api_response)

  for (const entry of kept_entries) {
    row = process_report_entry(entry, keepers_user_data[entry['keeper_id']])
    result_table.append(row)
  }
  $('#keeping-info').append(result_table)
  $('.additional-column').toggle()

  draw_average_seeds(kept_entries)
  draw_endangered_release_status(kept_entries)

  if($('#keepers-table')[0].clientHeight + 25 > $('#t-tor-stats')[0].clientHeight){
    var diff = $('#keepers-table')[0].clientHeight + 25 - $('#t-tor-stats')[0].clientHeight;
    var newHeight = 15 + diff;
    $('.seed')[0].style.lineHeight = newHeight+'px'
  }
}


function draw_kept_status_switch() {
  var self_user_id = GM_getValue('user_id')
  var target_release_id = parseInt($('a#topic-title').attr('href').split('t=')[1])

  api_request(
    `/release/${target_release_id}/status`, {},
    function(response) {
      var subforum_kept_status = response['subforum_status']
      var release_kept_status = response['release_status']
      var subforum_id = BB.FORUM_ID

      var options
      if (subforum_kept_status & ignore_non_reported_in_subforum) {
        options = [
          $('<option>', {value: exclude_from_report}).text('игнорировать раздачу в отчете'),
          $('<option>', {value: 0}).text('не хранится'),
          $('<option>', {value: reported_by_api}).text('хранится'),
        ]
      } else if (subforum_kept_status & reported_by_api) {
        options = [
          $('<option>', {value: exclude_from_report}).text('игнорировать раздачу в отчете'),
          $('<option>', {value: 0}).text('учет хранимым по факту сидирования'),
          $('<option>', {value: reported_by_api}).text('хранится'),
        ]
      }

      result_element = $('<div>', {style: 'position: absolute; margin: -6px;'})

      if (options) {
        result_element.append(
          $('<label id="release_kept_status-label">')
            .text('Статус хранения раздачи: ')
            .append($('<select>', {name: 'release_kept_status'}).append(options))
        )

        if (release_kept_status == null) {
          release_kept_status = 0
        } else if (release_kept_status & exclude_from_report) {
          release_kept_status = exclude_from_report
        } else if (release_kept_status & reported_by_api) {
          release_kept_status = reported_by_api
        }

        result_element.find(`select[name='release_kept_status'] option[value='${release_kept_status}']`)
          .prop('selected', 'selected')
        result_element.find(`select[name='release_kept_status']`).on("change", function() {
          var selected_status = parseInt($("select[name='release_kept_status'] option:selected").val())

          api_request(
            '/releases/set_status',
            JSON.stringify({
              'keeper_id': self_user_id,
              'topic_ids': [target_release_id],
              'status': selected_status,
              'reported_subforum_id': subforum_id,
              'unreport_other_releases_in_subforum': false,
              'copy_status_to_subforum': false,
            }),
            function(set_status_response) {},
            'post',
          )
          if (subforum_kept_status) {
            api_request(
              '/subforum/set_status?'
              + $.param({
                'keeper_id': self_user_id,
                'subforum_id': subforum_id,
                'status': subforum_kept_status,
                'comment': `С веб-страницы (раздача ${target_release_id})`,
              }),
              {},
              function(set_status_response) {},
              'post',
            )
          }
        })
      } else {
        result_element
          .text('Обозначьте подраздел как хранимый для управления статусом хранения.')
          .css('color', 'white')
          .css('margin', '0px')
      }

      result_element.prependTo($('#soc-container:parent'))
    }
  )

}


function draw_old_release_versions(topic_id, target_div) {
  const column_names = {
    'reg_time': 'Регистрация',
    'tor_size_bytes': 'Размер',
    'topic_title': 'Название',
    'tor_status': 'Статус',
    'keeping_priority': 'Приоритет',
    'seeders': 'Посл. сиды',
    'subforum_id': 'Подраздел',
    'topic_poster': 'Автор',
    'info_hash': 'Хэш',
  }
  const release_magnet = $('a.magnet-link').attr('title')

  api_request(
    `/old_release_versions/${topic_id}`, {},
    function(releases_reports_response) {
      target_div.empty()
      target_div.append('<div class="clear" style="height: 8px;"></div>')
      target_div.append(`<style>
        .old-versions-table { width: 100%; }
        .old-versions-table td { width: 1%; white-space: nowrap; }
        .old-versions-table .title-column {
          max-width: 0;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
          width: 30%;
        }
      </style>`)
      var table = $('<table>').addClass('attach bordered med old-versions-table')
      var columns = []
      for (var key of Object.keys(column_names)) {
        if (releases_reports_response.columns.indexOf(key) != -1)
          columns.push(key)
      }
      const versions = column_response_to_objects(releases_reports_response, 'versions')

      var thead = $('<thead>').addClass('row3 tCenter')
      var header_row = $('<tr>')
      columns.forEach(column => {
          var th = $('<th>').text(column_names[column]);
          header_row.append(th)
      });
      thead.append(header_row)
      table.append(thead)

      var added_versions = 0
      var tbody = $('<tbody>')
      versions.forEach(row => {
        var tr = $('<tr>').addClass('row1')
        var skip_row = false
        columns.forEach(column => {
          var td = $('<td>')
          let value = row[column]

          if (value === null || value === undefined) {
            value = 'N/A'
          } else if (column === 'info_hash') {
            if (value === release_magnet) {
              skip_row = true
              return
            }
            var magnet_link = $('<a>')
              .addClass('med magnet-link')
              .attr('href', `magnet:?xt=urn:btih:${value}`)
              .html(`<img src="https://static.rutracker.cc/templates/v1/images/magnet_1.svg" alt="magnet">${value}`)
            td.append(magnet_link)
            value = null
          } else if (column === 'reg_time') {
            td.attr('title', value)
            value = format_date(new Date(value))
          } else if (column === 'tor_size_bytes') {
            td.attr('title', value)
            value = human_file_size(value)
          } else if (column === 'tor_status') {
            value = release_statuses[value]
          } else if (column === 'keeping_priority') {
            if (value == 1) { value = 'обычный'
            } else if (value == 0) { value = 'низкий'
            } else if (value == 2) { value = 'высокий'
            }
          } else if (column === 'subforum_id') {
            var link = $('<a>')
              .addClass('med')
              .attr('href', `viewforum.php?f=${value}`)
              .text(value)
            td.append(link)
            value = null
          } else if (column === 'topic_poster') {
            var link = $('<a>')
              .addClass('med')
              .attr('href', `profile.php?mode=viewprofile&u=${value}`)
              .text(value)
            td.append(link)
            value = null
          } else if (column === 'topic_title') {
            td.html(value).addClass('title-column')
            value = null
          }

          if (value !== null) {
              td.text(value)
          }
          tr.append(td)
        })
        if (skip_row) return
        tbody.append(tr)
        added_versions++
      })
      table.append(tbody)

      if (!added_versions) {
        target_div.append($('<div>', {class: 'tCenter'}).text('Нет прошлых версий релиза'))
        return
      }

      if (added_versions <= 5) {
        target_div.append(
          $('<div>', {class: 'tCenter'}).text('Прошлые версии релиза:'),
          '<div class="clear" style="height: 4px;"></div>',
          table,
        )
      } else {
        target_div.attr('style', 'width:95%; margin: 12px auto 0;')
        table = wrap_in_spoiler(table, `Прошлые версии релиза (${added_versions}шт)`)
        table.attr('style', 'width: 100%;')
        target_div.append(
          '<div class="clear" style="height: 4px;"></div>',
          table,
        )
        BB.initSpoilers(table)
      }
    }
  )
}


function init_usual_topic_page_elements() {
  var target_element = $('fieldset.attach:first')
  if (!target_element.length) {
    target_element = $('table.message.forumline tbody tr td div:first')
  }
  if (!target_element.length) {
    console.log('No target element found to attach old release versions!')
    return
  }

  var topic_title = $('a#topic-title')
  var target_release_id
  if (topic_title.length) {
    target_release_id = topic_title.attr('href').split('t=')[1]
  } else {
    var topic_id_match = window.location.href.match(/viewtopic\.php\?t=(\d+)/)
    if (topic_id_match) {
      target_release_id = topic_id_match[1]
    } else {
      console.log('Cannot get topic_id to request old release versions!')
      return
    }
  }

  var old_release_versions_div = $('<div>', {id: 'old-release-versions'})
  target_element.after(old_release_versions_div)
  draw_old_release_versions(target_release_id, old_release_versions_div)
}


function init_release_page_elements() {
  target_table = $('<table>', {class: 'forumline', style: 'border-width:0px;'})
  inner_div = $('<div>', {style: 'position: absolute; right:0px; top:0px; '}).append(target_table)
  target_div = $('<div>', {style: 'position: relative; width: 0px; height: 21px;'}).append(inner_div)

  new_tor_stats = $('<table>', {class: 'w100'}).append(
    $('<td>', {class: 'w100', id: 't-tor-stats-td'}),
    $('<td>', {class: 'w0'}).append(target_div),
  )
  $('#t-tor-stats').after(new_tor_stats).detach().appendTo($('#t-tor-stats-td'))

  target_table.append(
    $('<tr>').append($('<span>', {id: 'keeping-info'})),
    $('<tr>').append($('<span>', {id: 'status-log'})),
  )

  target_release_id = $('a#topic-title').attr('href').split('t=')[1]

  subforum_id = $('td.nav:first a[href*="viewforum.php?f="]:last').attr('href').split('f=')[1]
  draw_kept_status_switch()
  api_request(
    `/releases/reports`,
    {
      topic_ids: `${target_release_id}`,
      columns:
        'status,last_update_time,last_seeded_time,first_update_time'
        + ',seeding_history,average_seeds_count,average_seeds_sum,reg_time',
      include_unreg_and_updated: true,
    },
    function(releases_reports_response) {
      do_ajax_gm(
        'https://api.rutracker.cc/v1/static/keepers_user_data',
        function() {
          var keepers_user_data = JSON.parse(this.responseText)['result']
          draw_keeping_info_table(releases_reports_response, keepers_user_data)
        },
        'application/json', 'GET',
      )
    }
  )

  var old_release_versions_div = $('<div>', {id: 'old-release-versions'})
  $('.attach.bordered.med').after(old_release_versions_div)
  draw_old_release_versions(target_release_id, old_release_versions_div)
}


function init_subforum_page_elements() {
  subforum_id = $('td.nav:first a[href*="viewforum.php?f="]:last').attr('href').split('f=')[1]

  $('td ul.vf-opt-links:first').prepend(
    $('<li>', {class: 'a-like'})
      .text('Показать шапку отчетов')
      .attr('style', 'color: green !important')
      .click(function() {
        $(this).attr('id', 'status-log').removeClass('a-like').unbind()
        api_request(
          `/subforum/report_topics`, {},
          function(response) {
            report_topic_id = response[subforum_id]
            if (!report_topic_id) {
              $('#status-log').text(`Не найдены списки подраздела ${subforum_id}`)
              return
            }
            $.get(`viewtopic.php?t=${report_topic_id}`).then(function(response) {
              header_post = $('table#topic_main tbody[id^="post_"].row1:first()', response)
              report_header = $('<div>', {class: 'forumline', id: 'report-header'}).append(header_post)
              $('.forum').before(report_header)
              BB.initPostImages(report_header)
              BB.initSpoilers(report_header)
              $('#status-log').remove()
            })
          }
        )
      }),
  )
}

// -----------------------------------------------------------------------------
//     Report topic functions
// -----------------------------------------------------------------------------

var report_table_columns = [
  {
    key: 'release_id_text',
    title: 'ID',
    data_key: 'release_id',
  },
  {
    key: 'release_name_text',
    title: 'Название',
    data_key: 'release_name',
    attrs: {'data-value': "!не-хранится"},
  },
  {
    key: 'release_status_text',
    title: 'Статус',
    data_key: 'release_status',
  },
  {
    key: 'release_size_text',
    title: 'Размер',
    data_key: 'release_size',
    align: "right",
  },
  {
    key: 'release_reg_time_text',
    title: 'Регистрация',
    data_key: 'release_reg_time',
    sort: 1,
  },
  {
    key: 'seeds_text',
    title: 'Сиды',
    data_key: 'seeds',
  },
  {
    key: 'average_seeds_html',
    title: 'Ср.сиды',
    data_key: 'average_seeds',
  },
  {
    key: 'keepers_html',
    title: 'Хранители',
    data_key: 'keepers',
  },
]
var release_statuses = [
  '* не проверено',
  'x закрыто',
  '√ проверено',
  '? недооформлено',
  '! не оформлено',
  'D повтор',
  'N/A',
  '∑ поглощено',
  '# сомнительно',
  '% проверяется',
  'T временная',
  '∏ премодерация'
]
var report_table_help_text = `

Подсказки по фильтру названия:
- не-хранится - для поиска раздач не в списках.

Подсказки по фильтру статуса:
- высокий/низкий - для поиска по соответствующему приоритету.

Подсказки по фильтру хранителей:
- !ник - для исключения из фильтра.
- ник1 && ник2 - для поиска раздач с двумя хранителями.
- Буква v после ника фильтрует по хранителю, который загружает раздачу.
- Буква u после ника фильтрует по хранителю, у которого хранится раздача до обновления.
`
var pager_html = `
<div class="pager forumline">
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/first.png" class="first"/>
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/prev.png" class="prev"/>
  <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/next.png" class="next"/>
  <img src="https://mottie.github.io/tablesorter/addons/pager/icons/last.png" class="last"/>
  <select class="pagesize" title="Количество строк на страницы">
    <option selected="selected" value="20">20</option>
    <option value="50">50</option>
    <option value="100">100</option>
    <option value="300">300</option>
    <option value="all">Все</option>
  </select>
  <select class="gotoPage" title="Номер страницы"></select>
</div>`

function pager_options() {
  return {
    container: $(".pager"),
    ajaxUrl: null,
    ajaxProcessing: function(ajax) {
      if (ajax && ajax.hasOwnProperty('data')) {
        return [ajax.data, ajax.total_rows];
      }
    },
    output: '{startRow} по {endRow} ({totalRows})',
    updateArrows: true,
    page: 0,
    size: 20,
    fixedHeight: true,
    cssNext: '.next',
    cssPrev: '.prev',
    cssFirst: '.first',
    cssLast: '.last',
    cssGoto: '.gotoPage',
    cssPageDisplay: '.pagedisplay',
    cssPageSize: '.pagesize',
    cssDisabled: 'disabled'
  }
}


function prepare_report_table_release_data(release_id, pvc_release, keepers_user_data, report_arrays) {
  release_date = pvc_release && new Date(pvc_release.reg_time * 1000)
  columns_data = {}

  columns_data['release_id'] = release_id
  columns_data['release_id_text'] = $('<a>', {href: 'viewtopic.php?t=' + release_id}).text(release_id)
  columns_data['release_name'] = pvc_release && pvc_release.topic_title || ''
  columns_data['release_name_text'] = pvc_release && pvc_release.topic_title || 'N/A'

  keepers = []
  keeper_nicks = []

  for (const kept_entry of report_arrays) {
    var nick = keepers_user_data[kept_entry.keeper_id.toString()][0]
    var last_update_time = new Date(kept_entry.last_update_time)
    var last_seeded_time = new Date(kept_entry.last_seeded_time)
    var report_date = new Date(Math.max(last_update_time, last_seeded_time))
    var is_downloading = kept_entry.status & downloading
    var nick_link = $('<a>', {href: '#report-' + kept_entry.keeper_id}).text(nick)
    if (report_date < release_date) {
      nick_link
        .css({'text-decoration': 'line-through', 'color': 'grey'})
        .attr('title', 'Прошлая версия релиза. Сведения о хранении от ' + format_date(report_date))
      keeper_nicks.push(nick + 'u')
    }
    nick_line = ''
    if (is_downloading) {
      nick_line += (is_downloading) ? '▼' : ''
      keeper_nicks.push(nick + 'v')
    }
    nick_line += nick_link.get(0).outerHTML

    keepers.push(nick_line)
    keeper_nicks.push(nick)
  }

  keepers.sort()
  columns_data['keepers_html'] = keepers.join(', ')
  columns_data['keepers'] = keeper_nicks.join(' ')

  if (!keepers.length) {
    columns_data['release_name'] += ' не-хранится'
  }

  if (pvc_release) {
    columns_data['release_size'] = pvc_release.tor_size_bytes
    columns_data['release_size_text'] = human_file_size(pvc_release.tor_size_bytes)
    columns_data['release_status'] = pvc_release.tor_status
    columns_data['release_status_text'] = release_statuses[pvc_release.tor_status]
    columns_data['release_reg_time'] = pvc_release.reg_time
    columns_data['release_reg_time_text'] = format_date(new Date(pvc_release.reg_time))
    columns_data['seeds'] = String(pvc_release.seeders)
    columns_data['seeds_text'] = String(pvc_release.seeders)

    var [total_average, total_average_str, detail_line] = prepare_average_seeds_details(
      merge_average_seeds(pvc_release.average_seeds_count, pvc_release.average_seeds_sum))
    columns_data['average_seeds'] = total_average
    columns_data['average_seeds_html'] = total_average_str

    // Release priority
    if (pvc_release.keeping_priority == 2) {
      columns_data['release_status_text'] += (
        ' <img class="vf-t-float-icon"'
        + ' src="https://static.rutracker.cc/templates/v1/images/priorities/highest.svg"'
        + ' title="высокий приоритет">'
      )
      columns_data['release_status'] += ' высокий'
    } else if (pvc_release.keeping_priority == 0) {
      columns_data['release_status_text'] += (
        ' <img class="vf-t-float-icon"'
        + ' src="https://static.rutracker.cc/templates/v1/images/priorities/lowest.svg"'
        + ' title="низкий приоритет">'
      )
      columns_data['release_status'] += ' низкий'
    }

    // Seeders
    if (pvc_release.seeders == 0) {
      seeder_last_seen = new Date(pvc_release.seeder_last_seen)
      // Naively ignoring timezones here.
      timedelta_days = Math.trunc((Date.now() - seeder_last_seen) / 1000 / 60 / 60 / 24)
      if (timedelta_days > 0) {
        columns_data['seeds_text'] += ' (' + timedelta_days + 'дн)'
        columns_data['seeds'] = -timedelta_days
      }
    }
  }
  return columns_data
}


function prepare_release_row(columns_data) {
  row = $('<tr>')

  for (column in report_table_columns) {
    key = report_table_columns[column]['key']
    sort_value_key = report_table_columns[column]['data_key']
    default_value = report_table_columns[column]['default']

    value = columns_data[key]
    sort_value = columns_data[sort_value_key]
    cell = $('<td>')

    if (value) cell.html(value);
    if (sort_value) cell.attr('data-ts_text', sort_value);
    if (!value && typeof default_value !== undefined) {
      cell.attr('data-ts_text', default_value);
    }

    row.append(cell)
  }

  return row
}


function render_report_table(subforum_reports_response, keepers_user_data, pvc) {
  table = $('<table>', {id: 'keepers-reports-stat', class: 'forumline'})
  $('#report-table-div').append(table)
  $('#status-log').text('Инициализация таблицы...')

  table_head = report_table_columns.map(i => $('<th>', i['attrs'] || {}).html(i['title']))
  table.append($('<thead>').append($('<tr>').append(table_head)))
  table_body = $('<tbody>')
  table.append(table_body)
  table.before($(pager_html))
  table.after($(pager_html))

  var keeping_releases_dict = {}

  for (const keeper_report of subforum_reports_response) {
    if (!keepers_user_data[keeper_report['keeper_id']] || keepers_user_data[keeper_report['keeper_id']][1])
      continue

    for (const topic_info of column_response_to_objects(keeper_report, 'kept_releases')) {
      topic_id = topic_info.topic_id
      if (!keeping_releases_dict[topic_id]) {
        keeping_releases_dict[topic_id] = []
      }
      topic_info.keeper_id = keeper_report['keeper_id']
      keeping_releases_dict[topic_id].push(topic_info)
    }
  }

  var unreg_release_ids = new Set(Object.keys(keeping_releases_dict))

  for (const pvc_release of column_response_to_objects(pvc, 'releases')) {
    release_id = pvc_release.topic_id
    unreg_release_ids.delete(release_id.toString())
    columns_data = prepare_report_table_release_data(
      release_id, pvc_release, keepers_user_data, keeping_releases_dict[release_id] || [])
    row = prepare_release_row(columns_data)
    table_body.append(row)
  }

  for (const unreg_release_id of unreg_release_ids) {
    columns_data = prepare_report_table_release_data(
      unreg_release_id, null, keepers_user_data, keeping_releases_dict[unreg_release_id] || [])
    row = prepare_release_row(columns_data)
    table_body.append(row)
  }

  var align_css = ''
  for (var i in report_table_columns) {
    var align = report_table_columns[i]['align']
    if (align) {
      var index = Number(i) + 1
      align_css += '#keepers-reports-stat tbody tr td:nth-child(' + index + ') { text-align: ' + align + '; } \n'
    }
  }
  if (align_css) {
    $('html > head').append($('<style>' + align_css + '</style>'))
  }

  var sorting_columns = []
  for (var i in report_table_columns) {
    var sorting_order = report_table_columns[i]['sort']
    if (sorting_order == 1 || sorting_order == 0)
      sorting_columns.push([i, sorting_order])
  }

  table.tablesorter({
    widgets: ['filter', 'pager'],
    sortList: sorting_columns,
    widthFixed: true,
    dateFormat: 'ddmmyyyy',
    textAttribute: 'data-ts_text',
    widgetOptions: {
      filter_matchType: { 'input': 'match', 'select': 'match' },
    },
  })
  .tablesorterPager(pager_options())
  .bind("sortEnd filterEnd",function() {
    var total_count = 0;
    var total_size = 0;
    table.find('tr:gt(1):not(.filtered)').each(function(){
      size = parseInt($(this).find("td:eq(3)").attr('data-ts_text'))
      if (size) {
        total_size += size
        total_count++
      }
    });
    $('#status-log').text('Показано: ' + total_count + 'шт, ' + human_file_size(total_size))
  })
  .trigger("sortEnd")
}


function init_report_page_elements() {
  first_post = $('tbody[id^="post_"]:first')
    .after($('<tbody>').append($('<tr>').append(
      $('<td>').css({'vertical-align': 'top'})
        .append($('<div>', {id: 'report-table-controls'}))
        .append($('<span>', {id: 'status-log'}).text('Большие подразделы могут обрабатываться долго!'))
        .append($('<span>', {id: 'report-table-help-text'})
                .text(report_table_help_text).css({'white-space': 'pre-line'})),
      $('<td>', {id: 'report-table-div'}),
    )))
  subforum_id = $('a.postLink[href*="viewforum.php?f="]:first').attr('href').split('f=')[1]

  api_request(
    `/subforum/${subforum_id}/reports`,
    {
      columns: 'status,last_update_time,last_seeded_time,',
      // only_reported_releases: only_reported_releases,
      // last_seeded_limit_days: last_seeded_limit_days,
      // last_update_limit_days: last_update_limit_days,
    },
    function(subforum_reports_response) {
      do_ajax_gm(
        'https://api.rutracker.cc/v1/static/keepers_user_data',
        function() {
          var keepers_user_data = JSON.parse(this.responseText)['result']
          api_request(
            `/subforum/${subforum_id}/pvc`,
            {columns: 'topic_title,tor_status,seeders,reg_time,tor_size_bytes,keeping_priority,seeder_last_seen'
              + ',average_seeds_count,average_seeds_sum'},
            function(pvc) {
              render_report_table(subforum_reports_response, keepers_user_data, pvc)
            }
          )
        },
        'application/json', 'GET',
      )
    }
  )

  $('#report-table-init-button').hide()
}


function draw_initialization_button() {
  var target_post_element = $('.posts:first')
  var init_button = $('<input>', {
    id: 'report-table-init-button',
    value: 'Показать интерактивную таблицу',
    type: 'button'
  }).click(init_report_page_elements).css({'white-space': 'normal', 'width': '100px'})
  target_post_element.after(init_button)
}


// -----------------------------------------------------------------------------
//     Keeper report service functions: Profile page
// -----------------------------------------------------------------------------


var only_subforums_marked_as_kept = true
var only_reported_releases = 0
var last_seeded_limit_days = 30
var last_update_limit_days = 60

reported_by_api                      = 0b00000000_00000000_00000000_00000001
downloading                          = 0b00000000_00000000_00000000_00000010
exclude_from_report                  = 0b00000000_00000000_00000000_00000100
keeping_prio_mask                    = 0b00000000_00000000_11111111_00000000
imported_from_forum                  = 0b00000000_00000001_00000000_00000000
ignore_non_reported_in_subforum      = 0b00000000_00000010_00000000_00000000


function quicksearch_script() {
  function qs_highlight_found() {
    this.style.display = '';
    var a = $('a:first', this)[0];
    var q = $('#q-search').val().toLowerCase();
    if (q != '' && a.innerHTML.toLowerCase().indexOf(q) != -1) {
      a.className = 'hl';
    } else {
      a.className = '';
    }
  }

  $.each($('#f-map a'), function (i, a) {
    var f_id = $(a).attr('href');
    $(a)
      .attr('href', 'viewforum.php?f=' + f_id)
      .text(`(${f_id}) ` +  $(a).text())
      .parents('li:first').attr('subforum_id', f_id)
  });

  $('#q-search').focus().quicksearch('#f-map li', {
    delay: 300,
    noResults: '#f-none',
    show: qs_highlight_found,
  }).on('keydown', function (e) {
    if (e.keyCode == 13) {
        return false;
    }
  });

  $.each($('span.c-title'), function (i, el) {
    $(el).text(this.title);
    this.title = '';
  });
}


function update_self_creds_and_krs_url(callback) {
  var profile_url = $('#logged-in-username').attr('href')
  var krs_url
  var user_id
  var api_key
  var bt_key
  var promises = [
    $.get(profile_url, function(data) {
      keys_text = $('th:contains("Хранительские ключи:")', data).next().text()
      matches = keys_text.match(/bt: (\w+) api: (\w+) id: (\w+)/)
      if (matches.length == 4) {
        GM_setValue('user_id', matches[3])
        GM_setValue('api_key', matches[2])
        GM_setValue('bt_key', matches[1])
        user_id = matches[3]
        api_key = matches[2]
        bt_key = matches[1]
      } else {
        console.log('Error: cannot get self ID and API key! Matches: ', matches)
      }
      return [matches[3], matches[2], matches[1]]
    }),
    $.get('viewtopic.php?t=5373769', function(data) {
      krs_url = $('#krs_url', data).next('span').find('a').attr('href')
      GM_setValue('krs_url', krs_url)
      return krs_url
    }),
  ]
  return $.when.apply($, promises).then(function () {
    console.log('Requested KRS url and credentials.')
    callback(krs_url, user_id, api_key, bt_key)
  })
}


function with_creds(callback) {
  krs_url = GM_getValue('krs_url', '')
  user_id = GM_getValue('user_id')
  api_key = GM_getValue('api_key')
  bt_key = GM_getValue('bt_key')

  // Workaround to force update the API URL.
  if (!krs_url.includes('rutracker')) krs_url = ''

  if (krs_url && api_key && user_id && bt_key) {
    callback(krs_url, user_id, api_key, bt_key)
  } else {
    update_self_creds_and_krs_url(callback)
  }
}


function api_request(address, parameters, callback, method='GET', fail_callback=null) {
  with_creds(function(krs_url, user_id, api_key, bt_key){
    $.ajax({
      url: krs_url + address,
      method: method,
      crossDomain: true,
      data: parameters,
      headers: {'Authorization': 'Basic ' + window.btoa(`${user_id}:${api_key}`)},
      success: callback,
      dataType: 'json',
      processData: method == 'GET',
      contentType: "application/json; charset=UTF-8",
      error: fail_callback,
    })
  })
}

function api_request_promise(address, parameters, method='GET') {
  return new Promise((resolve, reject) => {
    with_creds(function(krs_url, user_id, api_key, bt_key) {
      $.ajax({
        url: krs_url + address,
        method: method,
        crossDomain: true,
        data: parameters,
        headers: {'Authorization': 'Basic ' + window.btoa(`${user_id}:${api_key}`)},
        dataType: 'json',
        processData: method == 'GET',
        contentType: "application/json; charset=UTF-8",
        success: resolve,
        error: reject
      });
    });
  });
}

function getKeptEntryTotalInfo(kept_entry) {
  let total_entry_count = 0
  let total_entry_size = 0

  if (kept_entry.reported_not_seeded_count) {

    total_entry_count += kept_entry.reported_not_seeded_count
    total_entry_size += kept_entry.reported_not_seeded_size
  }

  if (kept_entry.reported_not_seeded_fresh_count) {

    total_entry_count += kept_entry.reported_not_seeded_fresh_count
    total_entry_size += kept_entry.reported_not_seeded_fresh_size
  }

  if (kept_entry.reported_seeded_count) {

    total_entry_count += kept_entry.reported_seeded_count
    total_entry_size += kept_entry.reported_seeded_size
  }

  if (kept_entry.only_seeded_count) {
    total_entry_count += kept_entry.only_seeded_count
    total_entry_size += kept_entry.only_seeded_size
  }

  return {
    total_entry_count,
    total_entry_size,
  }
}


function make_kept_info_line(kept_entry) {
    // reported_not_seeded_count, reported_not_seeded_size,
    // reported_seeded_count, reported_seeded_size,
    // only_seeded_count, only_seeded_size,
    // downloading_count, downloading_size
    result_elements = []

    const {total_entry_size, total_entry_count} = getKeptEntryTotalInfo(kept_entry)

    function push_span(text, color, label, style='') {
        const field = $('<span>', {
                class: 'vf-t-float-icon',
                style: `width: auto !important; font-weight: normal !important; ${style}`
            }).text(text).attr('title', label).css({
                cursor: 'pointer',
                color: color,
            })
        result_elements.push(field)
    }

    if (kept_entry.reported_not_seeded_count) {
        push_span(
            `  💾⤯: ${kept_entry.reported_not_seeded_count}шт, ${human_file_size(kept_entry.reported_not_seeded_size)}`,
            'indigo', 'В отчете, но не сидировалось')
    }

    if (kept_entry.reported_not_seeded_fresh_count) {
        push_span(
            `  💾?: ${kept_entry.reported_not_seeded_fresh_count}шт, ${human_file_size(kept_entry.reported_not_seeded_fresh_size)}`,
            'slategray', 'Недавно скачано, но не сидировалось')
    }

    if (kept_entry.reported_seeded_count) {
        push_span(
            `  💾▲: ${kept_entry.reported_seeded_count}шт, ${human_file_size(kept_entry.reported_seeded_size)}`,
            'green', 'В отчете и сидировалось')
    }

    if (kept_entry.only_seeded_count) {
        push_span(
            `  ▲: ${kept_entry.only_seeded_count}шт, ${human_file_size(kept_entry.only_seeded_size)}`,
            'royalblue', 'Сидировалось вне явно заявленного в отчете')
    }

    if (kept_entry.downloading_count) {
        push_span(
            `  ▼: ${kept_entry.downloading_count}шт, ${human_file_size(kept_entry.downloading_size)}`,
            'DarkGoldenRod', 'В отчете как скачиваемое')
    }

    push_span(
      (total_entry_count) ? `${total_entry_count}шт, ${human_file_size(total_entry_size)}` : '',
        'black', 'Всего', 'min-width: 150px !important; text-align: right;')

    return result_elements
}


function render_report_in_popup(user_id, subforum_id) {
  bb_alert('')
  $('#bb-alert-box').css({top: '5%', left: '5%', 'max-width': '90%', 'max-height': '90vh', 'margin-top': 0, 'margin-left': 0})
  $('#bb-alert-msg').css({'min-width': '99%', 'max-height': '90vh', 'margin': '0%'})
  target_element = $('<div>', {class: 'report-table-container'}).appendTo($('#bb-alert-msg').text(''))
  draw_report(target_element, user_id, subforum_id)
}


function decorate_li_element(li_element, kept_entry, target_user_id) {
  kept_info = make_kept_info_line(kept_entry)

  kept_info_anchor = li_element.find(`span > a[href^="viewforum.php?f="]:first`)
  kept_info_anchor.nextAll().remove()
  kept_info_anchor.after(...kept_info.reverse())
  kept_info_anchor.removeClass()

  li_element.find('span:first .vf-t-float-icon')
    .click(function() {render_report_in_popup(target_user_id, kept_entry.subforum_id)})

  if (kept_entry.reported_not_seeded_count) li_element.addClass('reported-not-seeded api-result')
  if (kept_entry.reported_not_seeded_fresh_count) li_element.addClass('reported-not-seeded-fresh api-result')
  if (kept_entry.reported_seeded_count) li_element.addClass('reported-seeded api-result')
  if (kept_entry.only_seeded_count) li_element.addClass('only-seeded api-result')
  if (kept_entry.downloading_count) li_element.addClass('reported-downloading api-result')
  if (kept_entry.subforum_marked_as_kept && kept_entry.status) {
    li_element.addClass('subforum-marked-as-kept api-result')
    kept_status = kept_entry.status.toString(16).padStart(8, '0')
    title = `Отчеты отправлены: ${kept_entry.last_update_time}, статус хранения: 0x${kept_status}`

    if (kept_entry.status & ignore_non_reported_in_subforum) {
      kept_info_anchor.addClass('subforum-marked-as-kept-only-reported')
      title += ', только заявленные через API раздачи'
    } else {
      kept_info_anchor.addClass('subforum-marked-as-kept-include-seeding')
      title += ', учитываются все сидировавшиеся и заявленные раздачи'
    }
    kept_info_anchor.attr('title', title)
  }
  if (!li_element.hasClass('api-result')) li_element.removeClass('not-kept')
}


function update_fmap_li_style() {
  result_css = `\
  a.subforum-marked-as-kept-only-reported {
    background-color: #3e7e3e; color: white !important;
    padding: 1px 5px !important; border-radius: 3px; }
  a.subforum-marked-as-kept-include-seeding {
    background-color: Aqua; padding: 1px 5px !important; border-radius: 3px; color: #006699; }
  `

  if (!$('#show-all-subforums').is(':checked')) {
    result_css += '.not-kept { display: none; }'
  }
  $('#fmap-li-style').text(result_css)
}


function get_only_reported_releases_fieldset() {
  return $('<fieldset>', {style: 'padding: 6px'}).append(
    $('<legend>').text('Учет раздач в подразделах'),
    $('<label>')
      .text('По настройкам хранителя')
      .prepend(
        $('<input>', {type: 'radio', name: 'only_reported_releases', value: 0, checked: !!!only_reported_releases})
      ),
    $('<label>')
      .text('Принудительно показать все')
      .prepend(
        $('<input>', {type: 'radio', name: 'only_reported_releases', value: 2, checked: !!only_reported_releases})
      ),
  )
}


function get_limit_days_fieldset() {
  var get_period_options = function() {return [
    $('<option>', {value: '3'}).text('3 дня'),
    $('<option>', {value: '7'}).text('7 дней'),
    $('<option>', {value: '14'}).text('14 дней'),
    $('<option>', {value: '21'}).text('21 день'),
    $('<option>', {value: '30'}).text('30 дней'),
    $('<option>', {value: '60'}).text('60 дней'),
    $('<option>', {value: '90'}).text('90 дней'),
    $('<option>', {value: '0'}).text('без ограничения'),
  ]}

  return $('<fieldset>', {style: 'padding: 6px'}).append(
    $('<legend>').text('Период показа'),
    $('<label>')
      .text('Сидировалось за: ')
      .append(
        $('<select>', {name: 'last_seeded_limit_days'})
        .append(get_period_options())
      ),
    $('<label>')
      .text('Отчет за: ')
      .append(
        $('<select>', {name: 'last_update_limit_days'})
        .append(get_period_options())
      ),
  )
}


function draw_all_seeded_switch() {
  function reinit_profile_page_elements() {
    var changed_checkboxes_count = 0
    radio_val = $("input:radio[name='only_subforums_marked_as_kept']:checked").val()
    radio_val = Boolean(parseInt(radio_val))
    if (radio_val == only_subforums_marked_as_kept) changed_checkboxes_count++
    only_subforums_marked_as_kept = radio_val

    radio_val = $("input:radio[name='only_reported_releases']:checked").val()
    radio_val = parseInt(radio_val)
    if (radio_val == only_reported_releases) changed_checkboxes_count++
    only_reported_releases = radio_val

    radio_val = $("select[name='last_seeded_limit_days'] option:selected").val()
    radio_val = parseInt(radio_val)
    if (radio_val == last_seeded_limit_days) changed_checkboxes_count++
    last_seeded_limit_days = radio_val

    radio_val = $("select[name='last_update_limit_days'] option:selected").val()
    radio_val = parseInt(radio_val)
    if (radio_val == last_update_limit_days) changed_checkboxes_count++
    last_update_limit_days = radio_val

    if (!changed_checkboxes_count) return

    $('#kept-subforums-control').remove()
    init_profile_page_elements()
  }

  var limit_days_fieldset = get_limit_days_fieldset()
  limit_days_fieldset.find('select').on("change", reinit_profile_page_elements)

  var only_reported_releases_fieldset = get_only_reported_releases_fieldset()
  only_reported_releases_fieldset.find('input').on("change", reinit_profile_page_elements)

  $('#f-map').before(
    $('</br>'),
    $('<fieldset>', {style: 'padding: 6px'}).append(
      $('<legend>').text('Показ подразделов'),
      $('<label>')
        .text('Только заявленные подразделы')
        .prepend(
          $('<input>', {type: 'radio', name: 'only_subforums_marked_as_kept', value: 1, checked: only_subforums_marked_as_kept})
          .on("change", reinit_profile_page_elements)
        ),
      $('<label>')
        .text('Все подразделы, где что-нибудь сидировалось')
        .prepend(
          $('<input>', {type: 'radio', name: 'only_subforums_marked_as_kept', value: 0, checked: !only_subforums_marked_as_kept})
          .on("change", reinit_profile_page_elements)
        ),
      $('</br>'),
      $('<label>')
        .text('Показать все подразделы трекера')
        .prepend($('<input>', {type: 'checkbox', id: 'show-all-subforums'}))
        .prependTo('#f-map')
        .click(update_fmap_li_style),
    ),
    only_reported_releases_fieldset,
    limit_days_fieldset,
    $('</br>'),
    $('<label>')
      .text('Режим управления хранимыми подразделами')
      .prepend($('<input>', {type: 'checkbox'}).click(function() {$('.kept-subforum-switch').toggle()}))
  )

  $(`select[name='last_seeded_limit_days'] option[value='${last_seeded_limit_days}']`).prop('selected', 'selected')
  $(`select[name='last_update_limit_days'] option[value='${last_update_limit_days}']`).prop('selected', 'selected')

  spoiler_head = $('#kept-subforums-control-spoiler .sp-head span').first()
  if (!spoiler_head.text().includes('и управление статусом')) {
    spoiler_head.text(spoiler_head.text() + ' и управление статусом')
  }
}


function set_subforum_kept_status(target_user_id, subforum_id) {
  var checkbox = $(`input:checkbox[subforum_id="${subforum_id}"]`)
  var status_before_switch = checkbox.attr('current_status')
  var new_status

  if (status_before_switch == 'none') {
    new_status = reported_by_api
    checkbox.prop('checked', 'checked')
    checkbox.attr('current_status', 'include-seeding')
  } else if (status_before_switch == 'include-seeding') {
    new_status = reported_by_api | ignore_non_reported_in_subforum
    checkbox.prop('indeterminate', true)
    checkbox.attr('current_status', 'only-reported')
  } else if (status_before_switch == 'only-reported') {
    new_status = 0
    checkbox.prop('checked', false)
    checkbox.attr('current_status', 'none')
  }

  api_request(
    '/releases/set_status',
    JSON.stringify({
      'keeper_id': target_user_id,
      'topic_ids': [],
      'status': new_status,
      'reported_subforum_id': subforum_id,
      'unreport_other_releases_in_subforum': false,
      'copy_status_to_subforum': true,
    }),
    function(set_status_response) {
      li_element = $(`li[subforum_id=${subforum_id}]`)
      if (new_status) {
        api_request(
          `/keeper/${target_user_id}/list_subforums`,
          {subforum_id: subforum_id},
          function(list_subforums_response) {
            kept_entry = column_response_to_objects(list_subforums_response)[0]
            if (kept_entry.subforum_id != subforum_id) {
              console.log('Unexpected response on list_subforums!', list_subforums_response)
              return
            }
            decorate_li_element(li_element, kept_entry, target_user_id)
          }
        )
      } else {
        decorate_li_element(li_element, Object(), target_user_id)
        revise_forum_li_classes()
      }
    },
    method='POST',
  )
}


function draw_subforum_kept_check(target_user_id, kept_subforums) {
  $(`li[subforum_id]`).each(function() {
    let subforum_id = parseInt($(this).attr('subforum_id'))
    let status = kept_subforums[subforum_id]
    $('<input>', {type: 'checkbox', checked: Boolean(status), title: 'вкл/выкл отметку хранения подраздела'})
      .attr('subforum_id', subforum_id)
      .attr('current_status',
        (status & ignore_non_reported_in_subforum)
          ? 'only-reported' : ((status & reported_by_api) ? 'include-seeding' : 'none')
      )
      .prop('indeterminate', Boolean(status & ignore_non_reported_in_subforum))
      .addClass('kept-subforum-switch')
      .click(function(){set_subforum_kept_status(target_user_id, subforum_id)})
      .insertBefore($(this).find('span > a:first'))
      .hide()
  })
}


function column_response_to_objects(response, result_key = 'result') {
  result_entries = []
  for (const kept_entry of response[result_key]) {
    var kept_entry_obj = Object.assign(...response['columns'].map((k, i) => ({[k]: kept_entry[i]})))
    result_entries.push(kept_entry_obj)
  }
  return result_entries
}


function revise_forum_li_classes() {
  // Remove not-kept class from all forums that have kept subforums, so the branch is shown on the page.
  $('li[subforum_id] > span.b').parent().each(function() {
    if ($(this).find('.api-result').length) $(this).removeClass('not-kept');
  })
}


function wrap_in_spoiler(element, title) {
  return $('<div>', {class: 'sp-wrap'}).append(
    $('<div>', {class: 'sp-head folded'}).append($('<span>').text(title)),
    $('<div>', {class: 'sp-body'}).append(element),
  )
}


function format_percent(percent) {
  if (percent == 0.0) return '--'

  percent *= 100

  var sign = (percent > 0.0) ? '+' : ''
  if (percent > 1000 || percent < -1000) return sign + '1000%+'
  var percent_str = sign + percent.toFixed(1) + '%'
  return percent_str
}


function hexToRgb(hex) {
  hex = hex.replace(/^#/, '');
  let bigint = parseInt(hex, 16);
  let r = (bigint >> 16) & 255;
  let g = (bigint >> 8) & 255;
  let b = bigint & 255;
  return { r, g, b };
}

function rgbToHex(r, g, b) {
  const toHex = n => n.toString(16).padStart(2, '0');
  return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
}


function interpolate_color(percent, startHex, endHex) {
  percent = Math.max(0, Math.min(100, percent));

  const startColor = hexToRgb(startHex);
  const endColor = hexToRgb(endHex);

  const interpolate = (start, end, factor) => Math.round(start + (end - start) * factor);
  const factor = percent / 100;
  const r = interpolate(startColor.r, endColor.r, factor);
  const g = interpolate(startColor.g, endColor.g, factor);
  const b = interpolate(startColor.b, endColor.b, factor);
  return rgbToHex(r, g, b);
}


function draw_kept_subforum_changes_table(
    changes_responses, hide_column, keepers_user_data, forum_names, report_topics
) {
  var date_subforum_keeper = {}
  var columns = [
    'Дата', 'Подраздел', 'Юзер',
    'Разница\nкол-во', 'Всего\nкол-во', 'Разница\nобъем', 'Всего\nобъем',
    'Разница\n⤓ кол-во', 'Всего\n⤓ кол-во', 'Разница\n⤓ объем', 'Всего\n⤓ объем',
  ]
  if (hide_column == 'subforum') columns.splice(1, 1)
  else if (hide_column == 'user') columns.splice(2, 1)

  var big_change_row_keys = []
  var entries = []
  for (response of changes_responses) {
    entries.push(...column_response_to_objects(response))
  }
  for (const entry of entries) {
    if (entry.update_date === '2024-06-11') continue
    if (!entry.diff_count && !entry.diff_size && !entry.diff_count_percent && !entry.diff_size_percent) continue
    key = `${entry.update_date}-${entry.subforum_id}-${entry.user_id}`
    if (!(key in date_subforum_keeper)) {
      date_subforum_keeper[key] = []
    }
    date_subforum_keeper[key][0] = entry.update_date
    date_subforum_keeper[key][1] = entry.subforum_id
    date_subforum_keeper[key][2] = entry.user_id

    var diff_count_percent = format_percent(entry.diff_count_percent)
    var diff_size_percent = format_percent(entry.diff_size_percent)
    var diff_count = ''
    if (entry.diff_count) {
      diff_count = ((entry.diff_count > 0) ? '+' : '') + entry.diff_count
      if (!entry.status) diff_count += ` (${diff_count_percent})`
    }
    var diff_size = `${human_file_size(entry.diff_size, false, 1, true)}`
    if (!entry.status) diff_size += ` (${diff_size_percent})`

    var offset = (entry.status) ? 4 : 0
    date_subforum_keeper[key][3 + offset] = diff_count
    date_subforum_keeper[key][4 + offset] = entry.total_count
    date_subforum_keeper[key][5 + offset] = diff_size
    date_subforum_keeper[key][6 + offset] = `${human_file_size(entry.total_size, false, 1, false)}`

    offset = (entry.status) ? 0 : 4
    if (!date_subforum_keeper[key][3 + offset]) {
      date_subforum_keeper[key][3 + offset] = ''
      date_subforum_keeper[key][4 + offset] = ''
      date_subforum_keeper[key][5 + offset] = ''
      date_subforum_keeper[key][6 + offset] = ''
    }

    if (
      Math.abs(entry.diff_count) >= 10
      || Math.abs(entry.diff_size) >= 100 * (1024 ** 3)
    ) big_change_row_keys.push(key)
  }

  var sorted_keys = Object.keys(date_subforum_keeper).sort()
  sorted_keys.reverse()
  var sorted_rows = []
  var big_change_row_indexes = []
  var cur_ind = 0
  for (const key of sorted_keys) {
    let row = date_subforum_keeper[key].map(element => (element === undefined ? 0 : element))
    if (hide_column == 'subforum') row.splice(1, 1)
    else if (hide_column == 'user') row.splice(2, 1)
    sorted_rows.push(row)
    if (big_change_row_keys.includes(key)) {
      big_change_row_indexes.push(cur_ind)
    }
    cur_ind++
  }
  sorted_rows.unshift(columns)

  $('#kept-subforum-changes').tablesorter({
    theme: 'blue',
    widgets: ['zebra'],
    widgetOptions: {
      build_type: 'array',
      build_source: sorted_rows,
      build_headers: {rows: 1},
      build_footers: {rows: 0},
    }
  })

  for (percent_column of ['Разница\nкол-во', 'Разница\nобъем']) {
    let index = columns.indexOf(percent_column)
    let column_query = `table.tablesorter tbody tr[role="row"] td:nth-child(${Number(index) + 1})`
    $('#kept-subforum-changes').find(column_query).each(function() {
      var percent = $(this).text().match(/\-?[\d\.]+(?=%)/)
      if (!percent) return
      percent = Number(percent)
      if (percent > 0.0) $(this).css('background', interpolate_color(percent, '#f5f5f5', '#4fff87'))
      else if (percent < 0.0) $(this).css('background', interpolate_color(-percent, '#f5f5f5', '#ff4f4f'))
    })
  }

  subforum_column_ind = columns.indexOf('Подраздел')
  if (subforum_column_ind != -1) {
    $('#kept-subforum-changes')
    .find(`table.tablesorter tbody tr:visible td:nth-child(${Number(subforum_column_ind) + 1})`)
    .each(function() {
        let subforum_name = forum_names[$(this).text()]
        if (subforum_name) {
          subforum_name = `(${$(this).text()}) ${subforum_name}`
        } else {
          subforum_name = `(${$(this).text()})`
        }
        let report_topic_id = report_topics[$(this).text()]
        if (report_topic_id) {
          $(this).html($('<a>', {href: `viewtopic.php?t=${report_topic_id}`}).text(subforum_name))
        } else {
          $(this).text(subforum_name)
        }
      }
    )
  }

  user_column_ind = columns.indexOf('Юзер')
  if (user_column_ind != -1) {
    $('#kept-subforum-changes')
    .find(`table.tablesorter tbody tr:visible td:nth-child(${Number(user_column_ind) + 1})`)
    .each(function() {
        let user_nick = keepers_user_data[$(this).text()]
        if (user_nick) {
          user_nick = user_nick[0]
        } else {
          user_nick = $(this).text()
        }
        $(this).html($('<a>', {href: `profile.php?mode=viewprofile&u=${$(this).text()}`}).text(user_nick))
      }
    )
  }

  cur_ind = 0
  $('#kept-subforum-changes')
    .find(`table.tablesorter tbody tr[role="row"]`)
    .each(function() {
      if (!big_change_row_indexes.includes(cur_ind)) {
        $(this).addClass('small_change')
      }
      cur_ind++
    })
  $('#kept-subforum-changes .small_change').hide()

  $('#kept-subforum-changes').before($('<label>')
    .text('Скрыть незначительные изменения (меньше 100Гб и 10шт за раз)')
    .prepend(
        $('<input>', {
          type: 'checkbox',
          checked: true,
          name: 'kept-subforum-changes-controls',
          value: true,
      })
      .click(function() {$('#kept-subforum-changes .small_change').toggle(!$(this).prop('checked'))})
    ))

  $('#kept-subforum-changes-status').text('Готово.').hide()
}


function draw_kept_subforum_changes(subforum_ids, keeper_ids, hide_column=null) {
  var urls = []
  if (subforum_ids) {
    for (subforum_id of subforum_ids) urls.push(`/subforum/${subforum_id}/changes`)
  } else {
    for (keeper_id of keeper_ids) urls.push(`/keeper/${keeper_id}/changes`)
  }

  var keepers_user_data
  var forum_names
  var kept_changes = []
  var report_topics

  var promises = [
    api_request_promise('/proxy_api/v1%2Fstatic%2Fkeepers_user_data', {},).then(response => {
      keepers_user_data = response['result']
      status_text.text('Ники хранителей загружены...')
    }),
    api_request_promise('/proxy_api/v1%2Fstatic%2Fcat_forum_tree', {},).then(response => {
      forum_names = response['result']['f']
      status_text.text('Названия подразделов загружены...')
    }),
    api_request_promise('/subforum/report_topics', {},).then(response => {
      report_topics = response
      status_text.text('Ссылки на топики загружены...')
    }),
  ]

  var cur_ind = 0
  for (url of urls) {
    promises.push(api_request_promise(url, {},).then(response => {
      kept_changes.push(response)
      cur_ind++
      status_text.text(`Загружено ${cur_ind}/${urls.length} изменений в подразделах...`)
    }))
  }
  $.when.apply($, promises).then(function () {
    status_text.text('Все данные загружены. Отрисовка таблицы...')
    draw_kept_subforum_changes_table(kept_changes, hide_column, keepers_user_data, forum_names, report_topics)
  })
}


function init_draw_kept_subforum_changes_spoiler(target_div, subforum_ids, keeper_ids, hide_column=null, skip_bb_init=false) {
  var kept_subforum_changes = $('<div>', {id: "kept-subforum-changes", class: "forumline"})
  kept_subforum_changes = wrap_in_spoiler(kept_subforum_changes, 'Последние изменения в хранимом')
  kept_subforum_changes.insertAfter(target_div).attr('id', 'kept-subforum-changes-spoiler')

  status_text = $('<span>', {id: 'kept-subforum-changes-status'}).text('загружается...')
  $('#kept-subforum-changes').before(status_text)
  if (!skip_bb_init) {
    BB.initSpoilers($('#kept-subforum-changes-spoiler'))
  }

  $('#kept-subforum-changes-spoiler.sp-wrap:not(.sp-opened)').click(function() {
    if (!status_text.text().includes('загружается...')) return
    status_text.text('начало загрузки...')
    draw_kept_subforum_changes(subforum_ids, keeper_ids, hide_column)
  })
}

function draw_kept_profile_seeding_table(seeding_history) {
  $('#kept-profile-history-table').empty();
  $('#kept-profile-history-table').tablesorter({
    theme: 'blue',
    widgets: ['zebra'],
    widgetOptions: {
      build_type: 'array',
      build_source: seeding_history,
      build_headers: {rows: 1},
      build_footers: {rows: 0},
    }
  });

  $('#kept-profile-history-status').text('Готово.').hide()
}

function draw_kept_profile_history(keeper_id, start_date, end_date) {
  const url = `/keeper/${keeper_id}/profile_history`;

  // Отнимаем 1 день, чтобы получить данные за рассчёта разницы отданого в первый день выбранного диапазона
  let date = new Date(start_date);
  date.setDate(date.getDate() - 1);
  start_date = date.toISOString().split('T')[0];
  // Прибавляем 1 день, чтобы получить данные за рассчёта разницы отданого в последний день выбранного диапазона
  date = new Date(end_date);
  date.setDate(date.getDate() + 1);
  end_date = date.toISOString().split('T')[0];

  api_request_promise(url, {start_date, end_date}).then(response => {
    $('#kept-profile-history-status').text('Все данные загружены. Отрисовка таблицы...')

    // Собираем все уникальные даты и сортируем их
    const allDates = Array.from(new Set([
      ...Object.keys(response.usual_seeding_history),
      ...Object.keys(response.rare_seeding_history),
      ...Object.keys(response.max_seeding_count_history),
      ...Object.keys(response.average_seeding_count_history),
    ])).sort();

    let prevUsual = null; // Предыдущее значение для usual_seeding_history
    let prevRare = null;  // Предыдущее значение для rare_seeding_history

    const seeding_history = allDates.map((date, index) => {
      const dateForSeeding = allDates[index + 1] ?? date;

      const usual = response.usual_seeding_history[dateForSeeding] ?? null;
      const rare = response.rare_seeding_history[dateForSeeding] ?? null;

      // Вычисляем разницу (если есть предыдущее значение)
      const usualDiff = usual !== null && prevUsual !== null ? usual - prevUsual : null;
      const rareDiff = rare !== null && prevRare !== null ? rare - prevRare : null;

      // Обновляем предыдущее значение
      if (usual !== null) prevUsual = usual;
      if (rare !== null) prevRare = rare;

      return [
        date,
        human_file_size(usualDiff),
        human_file_size(rareDiff),
        response.max_seeding_count_history[date] || 'Нет данных',
        response.average_seeding_count_history[date] || 'Нет данных',
      ];
    });

    seeding_history.shift(); // Мы брали статистику на один день назад, чтобы посчитать разницу для последнего отображаемого дня, уберём первую строку
    seeding_history.pop(); // Мы брали статистику за один день вперёд из-за сдвига информации об отданном, по этому удалим последнюю строку
    seeding_history.unshift(['Дата', 'Отдано', 'На редких', 'Сидируемые макс.', 'Сидируемые средн.']);

    draw_kept_profile_seeding_table(seeding_history)
  })
}

function init_draw_kept_profile_history_spoiler(target_div, keeper_id, skip_bb_init=false) {
  // All credits to our new dev!
  let kept_profile_history = $('<div>', {id: "kept-profile-history", class: "forumline"})
  kept_profile_history = wrap_in_spoiler(kept_profile_history, 'Статистика отданного')
  kept_profile_history.insertAfter(target_div).attr('id', 'kept-profile-history-spoiler')


  const history_status_text = $('<span>', {id: 'kept-profile-history-status'}).text('загружается...')
  $('#kept-profile-history').before(history_status_text)

  // Получаем первый и последний день текущего месяца
  const now = new Date();
  const userTimezoneOffset = now.getTimezoneOffset();

  let firstDay = new Date(now.getFullYear(), now.getMonth(), 1);
  firstDay.setMinutes(firstDay.getMinutes() - userTimezoneOffset);
  firstDay = firstDay.toISOString().split('T')[0];

  let lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);
  lastDay.setMinutes(lastDay.getMinutes() - userTimezoneOffset);
  lastDay = lastDay.toISOString().split('T')[0];

  const date_range_form = $('<div>', {id: 'kept-profile-history-date-range'});
  date_range_form.append($('<label>').text('Начало периода:'));
  date_range_form.append($('<input>', {id: 'kept-profile-history-start-date', type: 'date', value: firstDay}));
  date_range_form.append($('<span>').text('\t'));
  date_range_form.append($('<label>').text('Конец периода:'));
  date_range_form.append($('<input>', {id: 'kept-profile-history-end-date', type: 'date', value: lastDay}));
  date_range_form.append($('<br><br>'));
  $('#kept-profile-history').before(date_range_form)

  // Обработчик изменения дат
  $('#kept-profile-history-date-range input').on('change', function() {
    firstDay = $('#kept-profile-history-start-date').val();
    lastDay = $('#kept-profile-history-end-date').val();
    $('#kept-profile-history-status').text('загружается...')
    draw_kept_profile_history(keeper_id, firstDay, lastDay)
  });

  const kept_profile_history_table = $('<div>', {id: "kept-profile-history-table", class: "forumline"})
  $('#kept-profile-history').before(kept_profile_history_table)

  if (!skip_bb_init) {
    BB.initSpoilers($('#kept-profile-history-spoiler'))
  }

  $('#kept-profile-history-spoiler.sp-wrap:not(.sp-opened)').click(function() {
    if (!$('#kept-profile-history-status').text().includes('загружается...')) return
    $('#kept-profile-history-status').text('начало загрузки...')

    draw_kept_profile_history(keeper_id, firstDay, lastDay)
  })
}

function draw_user_curator_elements() {
  var target_user_id = $('#profile-uname').attr('data-uid')
  var no_curator_id = -1

  function set_initial_curator_text() {
    $('#curator-name-link-div').html('').text('загрузка...')
  }

  function update_curator_label() {
    api_request(
      `/keeper/${target_user_id}/user_info/get_curator`,
      {},
      function(response) {
        if (response['curator_id'] == no_curator_id || response['curator_id'] == null) {
          $('#curator-name-link-div').html('').text('Отсутствует')
        } else {
          $('#curator-name-link-div')
            .html('')
            .append($('<a>', {
              href: `profile.php?mode=viewprofile&u=${response['curator_id']}`,
              title: `Примерная дата начала: ${response['event_date']}`
                     + `\nУстановлено пользователем ${response['updated_by']}`
                     + `\nДата обновления: ${response['update_time']}`
            }).text(response['curator_nick']))
        }
      }
    )
  }

  function save_new_curator() {
    var curator = $('#curator-name').val() || no_curator_id
    var params = new URLSearchParams({curator: curator}).toString()
    set_initial_curator_text()
    api_request(
      `/keeper/${target_user_id}/user_info/set_curator?${params}`,
      {},
      function(response) {
        update_curator_label()
      },
      'post',
      function (xhr, status, error) {
        if (xhr.status == 422) {
          $('#curator-name-link-div').text(`Невозможно установить ${curator}!`)
          setTimeout(function () {$("#ajax-error").hide();}, 1000)
        }
      }
    )
  }

  $('.user_details > tbody')
    .append($('<tr>').append(
      $('<th>').text('Куратор:'),
      $('<td>', {class: 'med'}).append(
        $('<span>', {id: 'curator-name-link-div'}),
        '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;',
        $('<input>', {id: 'curator-name', type: 'text', placeholder: 'ник/айди для замены'}),
        $('<input>', {id: 'curator-name-save', type: 'button', value: 'Сохранить'}),
        $('<input>', {id: 'curator-name-set-self', type: 'button', value: 'Установить себя'}),
      ),
    ))

  set_initial_curator_text()
  update_curator_label()

  $('#curator-name-save').click(save_new_curator)
  $('#curator-name-set-self').click(function() {
    $('#curator-name').val($('#logged-in-username').text())
    save_new_curator()
  })
  $('#curator-name').keydown(function (e) {
    if (e.key === "Enter") save_new_curator()
  })
}


function draw_user_events(user_info_response) {
  var user_info = user_info_response

  /* Define enumerations and their user-friendly labels */
  var groupOptions = {
    "": "(пусто)",
    "-1": "Вне групп",
    "0":  "Кандидат",
    "1":  "Хранитель"
  };

  var reasonOptions = {
    "":              "(пусто)",
    "voluntarily":   "Добровольно",
    "rule_violation":"Несоблюдение условий",
    "to_candidate":  "В кандидаты",
    "no_reason":     "Без причины",
    "distinction_voluntarily":   "Добровольно (в заслуженные)",
    "distinction_rule_violation":"Несоблюдение условий (в заслуженные)",
    "distinction_to_candidate":  "В кандидаты (в заслуженные)",
    "distinction_no_reason":     "Без причины (в заслуженные)",
  };

  /* Fields we want to display/edit in each row (in the order you prefer) */
  var fieldsOrder = [
    "from_group",
    "to_group",
    "group_leader_id",
    "curator_id",
    "related_subforum_ids",
    "reason",
    "comment"
  ];

  /**
   * Build a <select> element for enumerated fields.
   * @param {object} optionsMap - e.g. groupOptions or reasonOptions.
   * @param {string|null} value - The current value from the data (may be null).
   * @param {string} fieldName - The JSON key (e.g., "from_group").
   * @returns jQuery <select> element.
   */
  function buildSelect(optionsMap, value, fieldName) {
    var $select = $('<select/>')
      .addClass('field-input')
      .attr('title', fieldName)
      .attr('placeholder', fieldName);

    // Populate <select> with <option>s
    $.each(optionsMap, function(val, label) {
      var $option = $('<option/>')
        .val(val)      // send val (e.g. -1, 0, 1)
        .text(label);  // show label (e.g. "No group", "Candidate")
      $select.append($option);
    });

    // Convert `null` to "" if needed
    if (value === null) value = "";
    $select.val(String(value));

    return $select;
  }

  /**
   * Build an <input type="text"> for non-enumerated fields.
   * @param {string|number|null} value - The current value (may be null).
   * @param {string} fieldName - The JSON key.
   * @param {boolean} readOnly - Whether the input is read-only
   * @returns jQuery <input> element.
   */
  function buildTextInput(value, fieldName, readOnly = false) {
    var $input = $('<input type="text"/>')
      .addClass('field-input')
      .attr('title', fieldName)
      .attr('placeholder', fieldName);

    // Convert null to empty
    if (value === null) value = "";
    $input.val(value);

    if (readOnly) {
      $input.prop('readonly', true);
      // You could also style it differently or set disabled if desired
    }

    return $input;
  }

  /**
   * Create one row (<tr>) for a given event date and eventData.
   * @param {string} eventDate
   * @param {object} eventData
   * @param {boolean} isNew - Whether this is a newly added event (true => date is editable).
   * @returns jQuery <tr> element
   */
  function buildRow(eventDate, eventData, isNew=false) {
    var $tr = $('<tr/>');

    /* ========== Date cell ========== */
    var dateTitle = "Updated by: " + eventData.event_updated_by 
                  + " | Update time: " + eventData.event_update_time;

    var $dateInput = buildTextInput(eventDate, "event_date", !isNew);
    $dateInput.attr('title', dateTitle);

    // If new, there's no update info yet—could omit or set a placeholder
    if (isNew) {
      $dateInput.removeAttr('title');
    }

    // store original date in a data-attr for change detection
    $dateInput.data('originalValue', eventDate);

    var $dateTd = $('<td/>').append($dateInput);
    $tr.append($dateTd);

    /* ========== Other fields ========== */
    fieldsOrder.forEach(function(fieldName) {
      var originalValue = eventData[fieldName];

      // Build select if it's one of the enum fields, otherwise build text input
      var $fieldEl;
      if (fieldName === "from_group" || fieldName === "to_group") {
        $fieldEl = buildSelect(groupOptions, originalValue, fieldName);
      } else if (fieldName === "reason") {
        $fieldEl = buildSelect(reasonOptions, originalValue, fieldName);
      } else {
        // Could handle related_subforum_ids as a comma-separated or JSON if you like
        // But for now, treat it as plain text
        $fieldEl = buildTextInput(originalValue, fieldName);
      }

      // Store original value so we can detect changes
      $fieldEl.data('originalValue', originalValue);

      $fieldEl.on('change input', function() {
        var $fld = $(this);
        var original = $fld.data('originalValue');
        var current = $fld.val();

        // Convert null -> "" if needed for comparison
        if (original === null) original = "";

        if (current !== String(original)) {
          $fld.addClass('changed');
        } else {
          $fld.removeClass('changed');
        }
      });

      var $td = $('<td/>').append($fieldEl);
      $tr.append($td);
    });

    /* ========== Save Button ========== */
    var $saveTd = $('<td/>');
    var $saveBtn = $('<input type="button" value="Save"></input>');

    function tryConvertToInt(str) {
      if (/^[+-]?\d+$/.test(str)) {
        return parseInt(str, 10);
      }
      if (!str.length) {
        return null;
      }
      return str;
    }

    $saveBtn.on('click', function() {
      // Gather all changed fields
      var changes = [];
      var dateVal = $dateInput.val().trim();
      var dateOrig = $dateInput.data('originalValue');

      // If you ever allow date editing, you'd detect changes here
      // For now, let's just let it pass; if changed, we treat it the same as others.
      if (dateVal !== dateOrig) {
        // Potentially handle date changes
      }

      // For each field, compare current vs. original
      $(this).closest('tr').find('.field-input').each(function() {
        var $fld = $(this);
        var fieldName = $fld.attr('title');
        var original = $fld.data('originalValue');

        // For selects, always string. For text, also string. Convert null->"" logic is handled above
        var current = $fld.val();

        // If there's a difference, queue for POST
        // Note: handle empty string => null if needed
        var diff = (current !== (original === null ? "" : String(original)));
        if (diff) {
          // single-field update
          var payload = {
            event_date: dateVal,
            event_key: fieldName,
            event_value: tryConvertToInt(current),
          };
          changes.push({ $el: $fld, payload: payload });
        }
      });

      // Fire one POST per changed field
      if (changes.length === 0) {
        alert("No changes to save.");
        return;
      }

      changes.forEach(function(changeObj) {
        api_request(
          `/keeper/${user_info.user_id}/user_info/update_event`,
          JSON.stringify(changeObj.payload),
          function(response) {
            // Mark field green
            changeObj.$el.css('background-color', 'lightgreen');
            // Clear changed marker
            changeObj.$el.data('originalValue', changeObj.$el.val());
          },
          'post',
          function(xhr) {
            // Mark field red, show error message
            changeObj.$el.css('background-color', 'lightcoral');
            changeObj.$el.attr('title', xhr.responseText || "Error saving");
          },
        )
      });
    });

    $saveTd.append($saveBtn);
    $tr.append($saveTd);

    return $tr;
  }

  /**
   * Initialize the table of existing events
   */
  function initEventsEditor() {
    var $container = $('#events-editor');
    var $table = $('<table/>');
    var $thead = $('<thead/>');
    var $tbody = $('<tbody/>');

    // Build a header row if you want
    var $headerRow = $('<tr/>');
    $headerRow.append($('<th>Date</th>'));
    fieldsOrder.forEach(function(f) {
      $headerRow.append($('<th/>').text(f));
    });
    $headerRow.append($('<th>Actions</th>'));
    $thead.append($headerRow);

    // Build a row for each existing event
    var eventsObj = user_info.events;
    // The keys in eventsObj are ISO date strings
    Object.keys(eventsObj).sort().forEach(function(dateKey) {
      var eventData = eventsObj[dateKey];
      var $row = buildRow(dateKey, eventData, false);
      $tbody.append($row);
    });

    $table.append($thead).append($tbody);
    $container.prepend($table);
  }

  /**
   * Add a new event row with today's date and blank fields
   */
  function addNewEventRow() {
    var $table = $('#events-editor table');
    var $tbody = $table.find('tbody');

    // If table not present yet, create it
    if (!$table.length) {
      initEventsEditor();
      $table = $('#events-editor table');
      $tbody = $table.find('tbody');
    }

    var today = new Date().toISOString().slice(0,10); // 'YYYY-MM-DD'

    // Build an empty data object
    var emptyData = {
      from_group: null,
      to_group: null,
      group_leader_id: null,
      curator_id: null,
      related_subforum_ids: null,
      reason: null,
      comment: null,
      event_updated_by: null,
      event_update_time: null
    };

    var $newRow = buildRow(today, emptyData, true);
    $tbody.append($newRow);
  }

  $('<div>', {id: "events-editor", class: "w100 row1"})
    .insertAfter('.user_profile')
    .append(
      $('<input id="add-new-event" type="button" value="Add new event">')
      .on('click', addNewEventRow)
    )

  $('head style:first()').before(
    $('<style>', {id: 'events-editor-styles'}).html(`
    #events-editor table {
      border-collapse: collapse;
      width: 100%;
      margin-bottom: 1em;
    }
    #events-editor th, #events-editor td {
      border: 1px solid #ccc;
      padding: 4px;
      text-align: left;
    }
    .field-input {
      /*width: 100px;*/ /* adjust as needed */
    }
    .changed {
      outline: 1px solid orange; /* highlight changed fields, optional */
    }
    `)
  )

  initEventsEditor();
}


function init_profile_page_elements() {
  var target_user_id = $('#profile-uname').attr('data-uid')
  var reusing_existing_spoiler = false

  $('#api-reports-init-button').hide()
  var kept_subforums_control = $('#kept-subforums-control-spoiler')
  if (!kept_subforums_control.length) {
    kept_subforums_control = $('<table>', {id: "kept-subforums-control", class: "w100"})
    kept_subforums_control = wrap_in_spoiler(kept_subforums_control, 'Карта хранимых подразделов')
    kept_subforums_control.insertAfter('.user_profile').attr('id', 'kept-subforums-control-spoiler')
    $('head style:first()').before($('<style>', {id: 'fmap-li-style'}))
  } else {
    kept_subforums_control.find('.sp-body').prepend($('<table>', {id: "kept-subforums-control", class: "w100"}))
    reusing_existing_spoiler = true
  }

  if (!$('#kept-subforum-changes').length) {
    init_draw_kept_subforum_changes_spoiler($('#kept-subforums-control-spoiler'), null, [target_user_id], 'user')
  }

  if (!$('#kept-profile-history').length) {
    init_draw_kept_profile_history_spoiler($('#kept-subforum-changes-spoiler'), target_user_id)
  }

  $.get('/forum/index.php?map', function(data) {
    var map_el = $(data).find("#categories-wrap")
    var map_el_script = $('script', map_el)
    map_el_script.text(quicksearch_script.toString() + '; quicksearch_script();')
    $("#kept-subforums-control").append(map_el);
  }).then(function() {
    $('#f-map').prepend($('<style>'))

    api_request(
      `/keeper/${target_user_id}/list_subforums`,
      {
        only_subforums_marked_as_kept: only_subforums_marked_as_kept,
        include_count: false,
        only_reported_releases: only_reported_releases,
        last_seeded_limit_days: last_seeded_limit_days,
        last_update_limit_days: last_update_limit_days,
      },
      function(response) {
        kept_entries = column_response_to_objects(response)
        response_subforum_ids = []
        kept_subforums = {}
        for (const kept_entry of kept_entries) {
          li_element = $(`li[subforum_id=${kept_entry.subforum_id}]`).addClass('api-result')
          response_subforum_ids.push(kept_entry.subforum_id)
          if (kept_entry.subforum_marked_as_kept && kept_entry.status)
          kept_subforums[kept_entry.subforum_id] = kept_entry.status
        }
        // Mark all subforums not in the result as not kept.
        $(`*[subforum_id]`).each(function() {
          if (!response_subforum_ids.includes(parseInt($(this).attr('subforum_id')))) $(this).addClass('not-kept')
        })
        // Mark all root categories as not kept is no kept subforums are found.
        $(`ul.tree-root`).each(function() {
          if (!$(this).find('.api-result').length) $(this).addClass('not-kept');
        })
        revise_forum_li_classes()
        update_fmap_li_style()
        $('#f-map').show()

        api_request(
          `/subforum/report_topics`, {},
          function(response) {
            $(`*[subforum_id]`).each(function() {
              report_topic_id = response[$(this).attr('subforum_id')]
              if (report_topic_id) {
                $(this).find('a:first()')
                  .before($('<a>', {href: `viewtopic.php?t=${report_topic_id}`}).text('📋'))
              }
            })
          }
        )

        var init_function = function() {
          if (!$('#f-load .loading-1').text().includes('загружается...')) return
          $('#f-load .loading-1').text('начало загрузки...')
          var total_subforums = response_subforum_ids.length
          var loaded_count_items = 0

          function process_entry(entry) {
            li_element = $(`li[subforum_id=${entry.subforum_id}]`).addClass('api-result')
            decorate_li_element(li_element, entry, target_user_id)

            const {total_entry_count, total_entry_size} = getKeptEntryTotalInfo(entry)

            function getComponent(className, label) {
              return $(`<span></span>`).addClass(className).attr('title', label).css({float: 'right'})
            }

            function update(el, total_entry_count, total_entry_size) {
              el.data('count', (el.data('count') || 0) + total_entry_count)
              el.data('size', (el.data('size') || 0) + total_entry_size)
              return el.text(`${el.data('count')}шт, ${human_file_size(el.data('size'))}`)
            }

            const forumListItem = li_element.parents('.tree-root')
            let forumTotal = forumListItem.find(".forum_total_info_container")
            if (!forumTotal.length) {
              forumTotal = getComponent('forum_total_info_container', 'Всего по форуму')
              forumTotal.appendTo(forumListItem.children('li').children('span').children('span'))
            }
            update(forumTotal, total_entry_count, total_entry_size)

            let widgetContainer = $('#f-map')
            let globalTotal = widgetContainer.children('.total_info_container')
            if (!globalTotal.length) {
              globalTotal = $(`<div class="total_info_container">Итого: <span title="Всего"></span></div>`).css({
                textAlign: 'right',
                marginBottom: '18px',
                fontWeight: 'bold',
                fontSize: '12px'
              })
              globalTotal.prependTo($('#f-map'))
            }
            
            globalTotal.data('count', (globalTotal.data('count') || 0) + total_entry_count)
            globalTotal.data('size', (globalTotal.data('size') || 0) + total_entry_size)
            globalTotal.find('span').text(`${globalTotal.data('count')}шт, ${human_file_size(globalTotal.data('size'))}`)

            $('#f-load .loading-1').text(`загружается ${++loaded_count_items}/${total_subforums}`)
            if (loaded_count_items >= total_subforums) {
              $('#f-load .loading-1').text('готово.')
              $('#f-load').hide()
            }
          }

          api_request(
            `/keeper/${target_user_id}/list_subforums`,
            {
              only_subforums_marked_as_kept: only_subforums_marked_as_kept,
              include_count: true,
              only_reported_releases: only_reported_releases,
              last_seeded_limit_days: last_seeded_limit_days,
              last_update_limit_days: last_update_limit_days,
            },
            function(list_response) {
              var response_subforums = []
              for (entry of column_response_to_objects(list_response)) {
                process_entry(entry)
                response_subforums.push(entry['subforum_id'])
              }
              if (!list_response['complete_result']) {
                response_subforum_ids = response_subforum_ids.filter(n => !response_subforums.includes(n))
                for (const subforum_id of response_subforum_ids) {
                  api_request(
                    `/keeper/${target_user_id}/subforum/${subforum_id}/info`,
                    {
                      only_reported_releases: only_reported_releases,
                      last_seeded_limit_days: last_seeded_limit_days,
                      last_update_limit_days: last_update_limit_days,
                    },
                    function(info_response) {
                      let entry = column_response_to_objects(info_response)[0]
                      process_entry(entry)
                    }
                  )
                }
              }
              if (!total_subforums) {
                $('#f-load .loading-1').text('В API пусто.').removeClass('loading-1')
              }
            }
          )
        }

        if (reusing_existing_spoiler) {
          init_function()
        } else {
          $('#kept-subforums-control-spoiler.sp-wrap:not(.sp-opened)').click(init_function)
        }

        api_request(
          `/keeper/${target_user_id}/check_full_permissions`,
          {},
          function(response) {
            if (response['result']) {
              draw_all_seeded_switch()
              draw_subforum_kept_check(target_user_id, kept_subforums)
            }
            BB.initSpoilers(kept_subforums_control)
            if (response['is_curator']) {
              draw_user_curator_elements()
            }
            if (response['is_admin']) {
              api_request(`/keeper/${target_user_id}/user_info`, {}, draw_user_events)
            }
          }
        )
      }
    )
  })
}


// -----------------------------------------------------------------------------
//     Keeper report service functions: Subforum reports page
// -----------------------------------------------------------------------------

function create_post_imitation(user_id, keepers_user_data_item) {
  var result = $(`
    <tbody class="report-by-api" id="report-${user_id}">
      <tr>
        <td class="poster_info td1 hide-for-print">
          <a id="${user_id}"></a>
          <p class="nick"><a href="#" onclick="return false;">${user_id}</a></p>
        </td>
        <td class="message td2" rowspan="2">
          <div class="post_head">
            <div class="clear"></div>
          </div>
          <div class="post_wrap">
            <div class="post_body"></div>
          </div>
        </td>
      </tr>
      <tr>
        <td class="poster_btn td3 hide-for-print">
          <div style="padding: 2px 6px 4px;" class="post_btn_2">
            <a class="txtb" href="profile.php?mode=viewprofile&amp;u=${user_id}">[Профиль]</a>&nbsp;
            <a class="txtb" href="privmsg.php?mode=post&amp;u=${user_id}">[ЛС]</a>&nbsp;
          </div>
        </td>
      </tr>
    </tbody>
  `)
  $.get({
    url: `profile.php?mode=viewprofile&u=${user_id}`,
    success: function(response) {
      result.find('p.nick a').text($('#profile-uname', response).text())
      result.find('p.nick')
        .after(
          $('<p>', {class: "rank_img"}).html($('img.user-rank', response)),
          $('<p>', {class: "avatar"}).html($('#avatar-img > img', response)),
          $('<p>', {class: "joined"}).append(
            '<em>Стаж:</em> ', $('tr > th:contains("Стаж:")', response).next().text().trim()),
          $('<p>', {class: "posts"}).append(
            '<em>Сообщений:</em> ', $('#posts a:first b', response).text()),
          $('<p>', {class: "posts"}).append(
            '<em>Сидируемых:</em> ',
            '<b>' + $('th:contains("Сидируемых:")', response).next('td').text().trim() + '</b>'),
          $('<p>', {class: "posts"}).append(
            '<em>Посл. активность:</em> ', $('th:contains("Посл. активность:")', response).next('td')),
          $('<p>', {class: "flag"}).html($('img.poster-flag', response)),
        )

    },
  })

  var user_role_element = false
  if (keepers_user_data_item) {
    if (keepers_user_data_item[1]) {
      result.addClass('user-candidate')
      user_role_element = `<span class="seedmed post-box-right">кандидат</span>`
    } else {
      result.addClass('user-member')
    }
  } else {
    result.addClass('user-excluded')
    user_role_element = `<span class="leechmed post-box-right">не в группе</span>`
  }
  if (user_role_element) {
    result.find('.post_head').prepend(user_role_element)
  }

  return result
}


function get_date_color(date_obj) {
  days_delta = (new Date() - date_obj) / (1000 * 60 * 60 * 24)
  if (days_delta <= 3) return 'limegreen'
  if (days_delta <= 7) return 'green'
  if (days_delta <= 14) return 'chocolate'
  if (days_delta <= 30) return 'salmon'
  return 'red'
}


function color_wrap(text, color) {
  return `<span class="p-color" style="color: ${color};">${text}</span>`
}


function count_size_line(count, size) {
  return `${count} шт. / ${human_file_size(size)}`
}


function get_date_element(date_obj, use_color=true) {
  if (String(new Date(null)) == String(date_obj))
    date_str = 'никогда!'
  else
    date_str = format_date(date_obj, '.')

  if (use_color) date_str = color_wrap(date_str, get_date_color(date_obj))

  return $(`<span class="post-b">${date_str}</span>`)
}


function get_keeping_status_elements(status) {
  result_elements = []
  if (status & reported_by_api) {
    result_elements.push($('<span>', {style: ''})
      .text('⚡').attr('title', 'Информация о статусе хранения получена через API'))
  }
  if (status & downloading) {
    result_elements.push($('<span>', {style: 'color: DarkGoldenRod;'})
      .text('⤓').attr('title', 'Заявлено скачиваемым через API'))
  }
  if (status & imported_from_forum) {
    result_elements.push($('<span>', {style: 'color: MediumVioletRed;'})
      .text('⎘').attr('title', 'Импортировано ботом из списков старого образца'))
  }
  if (status & keeping_prio_mask) {
    keeping_prio = (status & keeping_prio_mask) >> 8
    result_elements.push($('<span>', {style: 'color: MediumVioletRed;'})
      .text(`<${keeping_prio}>`).attr('title', 'Приоритет хранения раздачи'))
  }
  return result_elements
}


function initialize_report_post(post_element, kept_entry) {
  var last_update_time = new Date(kept_entry.last_update_time)
  var last_seeded_time = new Date(kept_entry.last_seeded_time)
  var first_update_time
  var first_update_time_text
  var actual_date = new Date(Math.max(last_update_time, last_seeded_time))

  if (kept_entry.first_update_time) {
    first_update_time = new Date(kept_entry.first_update_time).getTime()
    first_update_time_text = format_date(new Date(kept_entry.first_update_time))
  } else {
    first_update_time = 8640000000000
    first_update_time_text = 'N/A (по сидированию)'
  }

  detailed_lines = []
  if (kept_entry.reported_not_seeded_count)
    detailed_lines.push(color_wrap('Заявлены хранимым и не сидировались: '
      + count_size_line(kept_entry.reported_not_seeded_count, kept_entry.reported_not_seeded_size),
      'indigo'))
  if (kept_entry.reported_not_seeded_fresh_count)
    detailed_lines.push(color_wrap('Недавно скачаны и не сидировались: '
      + count_size_line(kept_entry.reported_not_seeded_fresh_count, kept_entry.reported_not_seeded_fresh_size),
      'slategray'))
  if (kept_entry.reported_seeded_count)
    detailed_lines.push(color_wrap('Заявлены хранимым и сидировались за месяц: '
      + count_size_line(kept_entry.reported_seeded_count, kept_entry.reported_seeded_size),
      'green'))
  if (kept_entry.only_seeded_count)
    detailed_lines.push(color_wrap('Только сидировались: '
      + count_size_line(kept_entry.only_seeded_count, kept_entry.only_seeded_size),
      'royalblue'))

  var downloading_count_line = ''
  if (kept_entry.downloading_count) {
    downloading_count_line = `<br>Всего скачиваемых раздач в подразделе:`
     + ` ${count_size_line(kept_entry.downloading_count, kept_entry.downloading_size)}`
  }

  var total_count =
    kept_entry.reported_not_seeded_count
    + kept_entry.reported_not_seeded_fresh_count
    + kept_entry.reported_seeded_count
    + kept_entry.only_seeded_count
  var total_size =
    kept_entry.reported_not_seeded_size
    + kept_entry.reported_not_seeded_fresh_size
    + kept_entry.reported_seeded_size
    + kept_entry.only_seeded_size

  if (!total_count && !kept_entry.downloading_count) {
    post_element.addClass('empty-report')
    if (!kept_entry.first_update_time) first_update_time_text = 'N/A (пустой отчет)'
  }

  var comment_line = ''
  if (kept_entry.comment) {
    comment_line = `<br>Комментарий: ${kept_entry.comment}`
  }

  post_element.find('.post_body').append(
    'Актуально на: ', get_date_element(actual_date), '<br>',
    'Последнее сидирование: ', get_date_element(last_seeded_time),
    ' | Последний отчет по API: ', get_date_element(last_update_time, false),
    '<span class="post-br"><br></span>',
    `Всего хранимых раздач в подразделе: ${count_size_line(total_count, total_size)}`, '<br>',
    'Из них: ', detailed_lines.join(' | '),
    downloading_count_line,
    comment_line,
    '<span class="post-br"><br></span>',
    $('<div>', {class: 'report-table-container'}),
  )

  $('<input>', {
    class: 'report-post-init-button',
    value: 'Показать отчет',
    type: 'button'
  })
    .click(function() {draw_report(
      $(`#report-${kept_entry.keeper_id} .report-table-container`), kept_entry.keeper_id, subforum_id)})
    .css({'white-space': 'normal'})
    .appendTo(post_element.find('.post_body .report-table-container'))

    post_element.find('.post_head').prepend(`
      <p class="post-time">
        <span class="hl-scrolled-to-wrap">
          <img src="https://static.rutracker.cc/templates/v1/images/icon_minipost.gif" class="icon1">
          <a class="p-link small" href="#report-${kept_entry.keeper_id}" title="Первое появление в подразделе">
            ${first_update_time_text}</a></span></p>`)
      .attr('first_update_time', first_update_time)
}


requestcolumns_label = {
  'topic_id': 'ID',  // KeptReleases
  'tor_size_bytes': 'размер',  // Pvc
  'seeders': 'текущие сиды',  // Pvc
  'average_seeds_count,average_seeds_sum': 'средние сиды',  // Pvc
  'leechers': 'текущие личи',  // Pvc
  'last_seeded_time': 'последнее сидирование',  // KeptReleases
  'seeding_history': 'история сидирования',  // KeptReleases
  'last_update_time': 'последний отчет по API',  // KeptReleases
  'first_update_time': 'первый отчет по API',  // KeptReleases
  'topic_title': 'название раздачи',  // Pvc
  'reg_time': 'дата регистрации торрента',  // Pvc
  'status': 'статус хранения',  // KeptReleases
  'keeping_priority': 'приоритет',  // Pvc
  'info_hash': 'хэш раздачи',  // Pvc
  'tor_status': 'статус раздачи',  // Pvc

  // 'seeder_last_seen': '',  // Pvc
  // 'topic_poster': '',  // Pvc
}

additionalcolumns_label = {
  'keepers_number': 'кол-во других хранителей',
  'keepers_nicks': 'ники других хранителей',
  'dl_keepers_number': 'кол-во качающих хранителей',
  'dl_keepers_nicks': 'ники качающих хранителей',
}

size_type_columns = ['tor_size_bytes']
date_type_columns = ['first_update_time', 'last_update_time', 'last_seeded_time', 'reg_time']

var selected_columns = GM_getValue('selected_report_columns', [])
var selected_additional_columns = GM_getValue('selected_additional_columns', [])

function draw_column_checkboxes(target_element, target_user_id, refresh_callback) {
  for (const column_key in requestcolumns_label) {
    if (column_key == 'topic_id') continue
    var column_title = requestcolumns_label[column_key]
      target_element.append(
        $('<label>')
        .text(column_title)
        .prepend(
          $('<input>', {
            type: 'checkbox',
            checked: selected_columns.includes(column_key),
            name: 'table-columns',
            value: column_key,
        }))
      )
  }

  additional_columns_taget_element = target_element.after($('<div>', {class: 'table-columns-checkboxes'}))
  for (const column_key in additionalcolumns_label) {
    var column_title = additionalcolumns_label[column_key]
      target_element.append(
        $('<label>')
        .text(column_title)
        .prepend(
          $('<input>', {
            type: 'checkbox',
            checked: selected_additional_columns.includes(column_key),
            name: 'additional-columns',
            value: column_key,
        }))
      )
  }

  $('<input>', {value: 'Применить', type: 'button', id: `apply-${target_user_id}`})
    .click(function() {
      selected_columns = []
      target_element.find('input:checkbox:checked[name="table-columns"]').each(function() {
        selected_columns.push($(this).val());
      })
      selected_additional_columns = []
      target_element.find('input:checkbox:checked[name="additional-columns"]').each(function() {
        selected_additional_columns.push($(this).val());
      })
      refresh_callback()
    })
    .css({'white-space': 'normal'})
    .appendTo(target_element)
  $('<input>', {value: 'Запомнить выбор', type: 'button'})
    .click(function() {
      to_save = []
      target_element.find('input:checkbox:checked[name="table-columns"]').each(function() {
        to_save.push($(this).val());
      })
      GM_setValue('selected_report_columns', to_save)
      to_save = []
      target_element.find('input:checkbox:checked[name="additional-columns"]').each(function() {
        to_save.push($(this).val());
      })
      GM_setValue('selected_additional_columns', to_save)
    })
    .css({'white-space': 'normal'})
    .appendTo(target_element)
  $('<input>', {value: 'Скачать показанные .torrent файлы', type: 'button', class: 'torrent-download-button'})
    .click(function(e) {
      e.preventDefault()
      download_count = 0
      torrent_files = 0
      zip = null
      var parent_element = $($(this).parents()[1])
      var selector = $(parent_element.find('.tablesorter tr:not(.filtered) td:first-child:not([data-column])'))
      var number = selector.length
      $('#unreg-down').attr('disabled', true);
      selector.each(function(i, el){
        setTimeout(function(){
          var url = 'dl.php?t=' + $(el).text();
          console.log('url: ' + url);
          add_torrent(url, number, parent_element);
        }, 100 + ( i * 100 ));
      });
    })
    .css({'white-space': 'normal'})
    .appendTo(target_element)

  api_request(
    `/keeper/${target_user_id}/check_full_permissions`,
    {},
    function(response) {
      if (!response['result']) return
      var limit_days_fieldset = get_limit_days_fieldset()
      limit_days_fieldset.find('select').on("change", function(){
        radio_val = limit_days_fieldset.find("select[name='last_seeded_limit_days'] option:selected").val()
        radio_val = parseInt(radio_val)
        last_seeded_limit_days = radio_val

        radio_val = limit_days_fieldset.find("select[name='last_update_limit_days'] option:selected").val()
        radio_val = parseInt(radio_val)
        last_update_limit_days = radio_val
      })

      var only_reported_releases_fieldset = get_only_reported_releases_fieldset()
      only_reported_releases_fieldset.find('input').on("change", function(){
        radio_val = $("input:radio[name='only_reported_releases']:checked").val()
        radio_val = parseInt(radio_val)
        only_reported_releases = radio_val
      })

      $(`#apply-${target_user_id}`).before(
        $('</br>'),
        $('<div>', {class: 'w40', style: 'display:inline-block'}).append(only_reported_releases_fieldset),
        '  ',
        $('<div>', {class: 'w40', style: 'display:inline-block'}).append(limit_days_fieldset),
        $('</br>'),
      )
      limit_days_fieldset.find(`select[name='last_seeded_limit_days'] option[value='${last_seeded_limit_days}']`)
        .prop('selected', 'selected')
      limit_days_fieldset.find(`select[name='last_update_limit_days'] option[value='${last_update_limit_days}']`)
        .prop('selected', 'selected')
    }
  )
}

function merge_average_seeds(count, sum) {
  let result = []
  for (let i = 0; i < count.length; i++) {
    if (count[i]) {
      result[i] = sum[i] / count[i]
    } else {
      result[i] = null
    }
  }
  return result
}

function period_average_seeds(count, sum, days) {
  var total_count = 0
  var total_sum = 0
  for (let i = 0; i < Math.min(count.length, days); i++) {
    if (count[i]) {
      total_count += count[i]
      total_sum += sum[i]
    }
  }
  if (total_count) {
    return total_sum / total_count
  }
  return null
}

function prepare_average_seeds_details(average_seeds) {
  var total_average = average_seeds.filter(x => x)
  total_average = total_average.reduce((a, b) => a + b, 0) / total_average.length
  if (!total_average) total_average = 0.0
  var total_average_str = total_average.toFixed(2)

  total_average = (total_average * 10000).toFixed(0)

  var detail_line = ''
  for (index in average_seeds) {
    var day_average_seeds = average_seeds[index]
    detail_line += (typeof day_average_seeds === 'number') ? day_average_seeds.toFixed(1) : '?'
    detail_line += (index && ! ((Number(index) + 1) % 7)) ? '\n' : ' '
  }
  return [total_average, total_average_str, detail_line]
}


function color_average_seeds(seeds_str) {
  seeds = parseFloat(seeds_str)
  var color
  if (seeds <= 0.1) color = 'black'
  else if (seeds <= 0.5) color = 'red'
  else if (seeds <= 1.0) color = 'orange'
  else if (seeds <= 3.0) color = 'green'
  else if (seeds <= 10.0) color = 'dodgerblue'
  else color = 'DarkTurquoise'
  average_str = `<span style="color: ${color};">${seeds_str}</span>`

  return average_str
}


function seeding_history_element_to_string(seeding_history_element) {
  var seeding_history_day = seeding_history_element & 0b00000000_11111111_11111111_11111111
  seeding_history_day = seeding_history_day.toString(2).padStart(24, '0')
  return seeding_history_day
}


function get_seeded_hours_of_day(seeding_history_element) {
  var seeding_history_day = seeding_history_element_to_string(seeding_history_element)
  var seeded_hours = 0
  for (var i=0; i < 24; seeded_hours+=+('1' === seeding_history_day[i++]));
  return seeded_hours
}


function prepare_seeding_history(seeding_history) {
  var result = ''

  for (index in seeding_history) {
    var date = new Date()
    date.setDate(date.getDate() - index)
    date = date.toLocaleDateString('ru')
    var availability = get_seeded_hours_of_day(seeding_history[index]) / 24
    var percent = Math.floor(availability * 100)
    var seeding_history_day = seeding_history_element_to_string(seeding_history[index])

    seeding_history_day = seeding_history_day.split('').reverse().join('')
    seeding_history_day = seeding_history_day.replace(/(.{6})/g, '$1 ')

    seeding_block = `
      <div class="sh-bar-wrap" title="${date}: ${seeding_history_day}">
      <div class="sh-block" style="height: ${percent}%;"></div>
      </div>`
    result += seeding_block
  }
  result = `<div style="display: flex; height: 16px; align-items: flex-end; border-bottom: 1px solid;">
    <style>
      .sh-block { background-color: DarkTurquoise; border: 0px solid black; width: 8px; }
      .sh-bar-wrap { height: 100%; align-content: flex-end; }
    </style>
    ${result}</div>
  `
  return result
}

function prepare_seeding_history_str(seeding_history_str) {
  var seeding_history = seeding_history_str.split(',').map(parseFloat)
  return prepare_seeding_history(seeding_history)
}

function draw_report(target_element, user_id, subforum_id) {
  target_element.children().remove()
  checkboxes_element = $('<div>', {class: 'table-columns-checkboxes'}).prependTo(target_element)
  draw_column_checkboxes(
    checkboxes_element, user_id, function() {draw_report(target_element, user_id, subforum_id)})

  var keeper_reports_response
  var subforum_reports_response = []
  var keepers_user_data = {}

  var promises = [
    api_request_promise(
      `/keeper/${user_id}/reports`,
      {
        subforum_id: subforum_id,
        columns: selected_columns.join(','),
        only_reported_releases: only_reported_releases,
        last_seeded_limit_days: last_seeded_limit_days,
        last_update_limit_days: last_update_limit_days,
      },
    ).then(response => {
      keeper_reports_response = response[0]
    }),
  ]

  if (selected_additional_columns.length) {
    promises.push(api_request_promise(
      `/subforum/${subforum_id}/reports`,
      {
        columns: 'status',
        only_reported_releases: only_reported_releases,
        last_seeded_limit_days: last_seeded_limit_days,
        last_update_limit_days: last_update_limit_days,
      },
    ).then(response => {
      subforum_reports_response = response
    }))
    promises.push(api_request_promise(
      '/proxy_api/v1%2Fstatic%2Fkeepers_user_data', {},).then(response => {
      keepers_user_data = response['result']
      // status_text.text('Ники хранителей загружены...')
    }))
  }

  $.when.apply($, promises).then(function () {
      if (keeper_reports_response['subforum_id'] != subforum_id) {
        console.log('Unexpected response on reports!', keeper_reports_response)
        return
      }
      var data_frame = keeper_reports_response['kept_releases']
      var keeper_release_ids = new Set()

      var average_seeds_sum_column_index = keeper_reports_response['columns'].indexOf('average_seeds_sum')
      var topic_id_column_index = keeper_reports_response['columns'].indexOf('topic_id')

      // Fix dates: place them already formatted in data.
      // Fix average seeds: calculate values and store in-place in `average_seeds_sum`.
      for (column_index in keeper_reports_response['columns']) {
        column_key = keeper_reports_response['columns'][column_index]
        if (date_type_columns.includes(column_key)) {
          for (row_index in data_frame) {
            date_value = data_frame[row_index][column_index]
            formatted_date = ''
            if (date_value) {
              formatted_date = date_value
                .replace(/^(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d).*/, '$3.$2.$1 $4:$5')
            }
            data_frame[row_index][column_index] = formatted_date
          }
        } else if (column_key == 'average_seeds_count') {
          for (row_index in data_frame) {
            merged = merge_average_seeds(
              data_frame[row_index][column_index], data_frame[row_index][average_seeds_sum_column_index])
            as_text = prepare_average_seeds_details(merged).join(' ;; ')
            data_frame[row_index][average_seeds_sum_column_index] = as_text
          }
        } else if (selected_additional_columns.length && column_key == 'topic_id') {
          for (row_index in data_frame) {
            keeper_release_ids.add(data_frame[row_index][column_index])
          }
        }
      }

      // Fix average seeds: remove `average_seeds_count`, so it is not rendered.
      if (average_seeds_sum_column_index != -1) {
        var average_seeds_count_column_index = keeper_reports_response['columns'].indexOf('average_seeds_count')
        for (row_index in data_frame) {
          data_frame[row_index].splice(average_seeds_count_column_index, 1)
        }
        keeper_reports_response['columns'].splice(average_seeds_count_column_index, 1)
      }

      var topicid_keepers_number = new Map()
      var topicid_keepers_ids = new Map()
      var topicid_dl_keepers_number = new Map()
      var topicid_dl_keepers_ids = new Map()
      for (const keeper_report of subforum_reports_response) {
        if (keeper_report['keeper_id'] == user_id)
          continue
        if (!keepers_user_data[keeper_report['keeper_id']] || keepers_user_data[keeper_report['keeper_id']][1])
          continue

        for (const topic_info of column_response_to_objects(keeper_report, 'kept_releases')) {
          topic_id = topic_info.topic_id
          if (!keeper_release_ids.has(topic_id))
            continue
          if (topic_info.status & downloading) {
            topicid_dl_keepers_number.set(topic_id, (topicid_dl_keepers_number.get(topic_id) || 0) + 1)
            if (!topicid_dl_keepers_ids.has(topic_id)) {
              topicid_dl_keepers_ids.set(topic_id, [])
            }
            topicid_dl_keepers_ids.get(topic_id).push(keeper_report['keeper_id'])
          } else {
            topicid_keepers_number.set(topic_id, (topicid_keepers_number.get(topic_id) || 0) + 1)
            if (!topicid_keepers_ids.has(topic_id)) {
              topicid_keepers_ids.set(topic_id, [])
            }
            topicid_keepers_ids.get(topic_id).push(keeper_report['keeper_id'])
          }
        }
      }

      var columns_to_show = [...keeper_reports_response['columns']]
      var user_nick_id = new Map()

      if (selected_additional_columns.length) {
        columns_to_show.push(...selected_additional_columns)
        function get_nicks_line(user_ids) {
          if (!user_ids || !user_ids.length)
            return ''
          var nicks = []
          for (other_keeper_id of user_ids) {
            if (keepers_user_data[other_keeper_id]) {
              nicks.push(keepers_user_data[other_keeper_id][0])
              user_nick_id.set(keepers_user_data[other_keeper_id][0], other_keeper_id)
            } else {
              nicks.push(`!${other_keeper_id}`)
            }
          }
          return nicks.join(' ;; ')
        }

        for (row_index in data_frame) {
          var topic_id = data_frame[row_index][topic_id_column_index]
          var values_to_append = []
          if (selected_additional_columns.includes('keepers_number'))
            values_to_append.push((topicid_keepers_number.get(topic_id) || 0))
          if (selected_additional_columns.includes('keepers_nicks'))
            values_to_append.push(get_nicks_line(topicid_keepers_ids.get(topic_id)))
          if (selected_additional_columns.includes('dl_keepers_number'))
            values_to_append.push((topicid_dl_keepers_number.get(topic_id) || 0))
          if (selected_additional_columns.includes('dl_keepers_nicks'))
            values_to_append.push(get_nicks_line(topicid_dl_keepers_ids.get(topic_id)))

            data_frame[row_index].push(...values_to_append)
        }
      }

      let column_label = {...requestcolumns_label, ...additionalcolumns_label}
      column_label['average_seeds_sum'] = 'средние сиды'

      var column_names = columns_to_show.map(x => (column_label[x] || x))
      console.log('column_names:', column_names)
      data_frame.unshift(column_names)
      var table_element = $('<div>', {class: 'forumline', user_id: user_id, subforum_id: subforum_id})
        .appendTo(target_element)
      table_element
        .tablesorter({
          widgets: ['filter', 'pager'],
          dateFormat: 'ddmmyyyy',
          textAttribute: 'data-ts_text',
          widgetOptions: {
            build_type: 'array',
            build_source: data_frame,
            build_headers: {rows: 1},
            build_footers: {rows: 0},
          }
        })

      table_element.prepend($('<style>').html(
        `.tablesorter-default { width: 100%; }
        .tablesorter-default td { width: 1%; white-space: nowrap; }
        .tablesorter-default .title-column {
          max-width: 0;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
          width: 30%;
        }
        `))
      table_element.find('tbody tr').addClass('hl-tr')
      table_element.find('table:first')
        .before($(pager_html))
        .after($(pager_html))
        .tablesorterPager(pager_options())
        .bind('sortEnd filterEnd pageMoved pagerChange', function() {
          for (column_index in columns_to_show) {
            column_key = columns_to_show[column_index]
            column_query = `table.tablesorter tbody tr:visible td:nth-child(${Number(column_index) + 1}):not(.rendered)`
            if (column_key == 'topic_id') {
              table_element.find(column_query).each(function() {
                $(this).html(`<a href="viewtopic.php?t=${$(this).text()}" class="postLink">${$(this).text()}</a>`)
                  .addClass('rendered')
              })
            } else if (size_type_columns.includes(column_key)) {
              table_element.find(column_query).each(function() {
                bytes = parseInt($(this).text())
                $(this).attr('data-ts_text', bytes).text(human_file_size(bytes))
                  .addClass('rendered')
              })
            } else if (column_key == 'tor_status') {
              table_element.find(column_query).each(function() {
                $(this).text(release_statuses[parseInt($(this).text())])
                  .addClass('rendered')
              })
            } else if (column_key == 'status') {
              table_element.find(column_query).each(function() {
                status_code = parseInt($(this).text())
                // status_hex = '0x' + status_code.toString(16).padStart(8, '0')
                $(this).html('').append(...get_keeping_status_elements(status_code))
                  .attr('data-ts_text', status_code)
                  .addClass('rendered')
              })
            } else if (column_key == 'average_seeds_sum') {
              table_element.find(column_query).each(function() {
                var [total_average, total_average_str, detail_line] = $(this).text().split(' ;; ')
                $(this)
                  .attr({'data-ts_text': total_average, 'title': detail_line})
                  .html(color_average_seeds(total_average_str))
                  .addClass('rendered')
              })
            } else if (column_key == 'seeding_history') {
              table_element.find(column_query).each(function() {
                $(this)
                  .html(prepare_seeding_history_str($(this).text()))
                  .attr('style', 'background-color: black !important; color: DarkTurquoise; text-align: left; white-space: nowrap;')
                  .addClass('rendered')
              })
            } else if (column_key == 'topic_title') {
              table_element.find(column_query).each(function() {
                $(this).addClass('title-column').attr('title', $(this).text())
              })
            } else if (column_key == 'keepers_nicks' || column_key == 'dl_keepers_nicks') {
              table_element.find(column_query).each(function() {
                var final_html_parts = $(this).text().split(' ;; ').map(x =>
                  $('<a>', {'href': `profile.php?mode=viewprofile&u=${user_nick_id.get(x)}`}).text(x)[0].outerHTML
                )
                $(this)
                  .html(final_html_parts.join(', '))
                  .addClass('rendered')
              })
            }
          }
        })
        .trigger("sortEnd")
    }
  )
}


function toggle_all_user_type_posts() {
  $('.user-member').toggle($('#member-reports-switch').prop('checked'))
  $('.user-candidate').toggle($('#candidate-reports-switch').prop('checked'))
  $('.user-excluded').toggle($('#excluded-reports-switch').prop('checked'))
  $('.empty-report').toggle($('#empty-reports-switch').prop('checked'))
}

function make_mode_switch() {
  mode_switch = $(`
    <tbody class="row7"><tr>
      <td><div id="f-load" style="padding: 6px;"><i class="loading-1">загружается...</i></div></td>
      <td id="mode-switch"></td>
    </tr></tbody>`
  )
  mode_switch.find('#mode-switch').append(
    $('<span>', {class: 'report-by-api'}).text('Показ отчетов: '),
    $('<label>', {class: 'report-by-api'})
      .text('хранители')
      .prepend(
        $('<input>', {type: 'checkbox', id: 'member-reports-switch', checked: true})
          .click(function() {$('.user-member').toggle($(this).prop('checked'))})),
    $('<label>', {class: 'report-by-api'})
      .text('кандидаты')
      .prepend(
        $('<input>', {type: 'checkbox', id: 'candidate-reports-switch', checked: false})
          .click(function() {$('.user-candidate').toggle($(this).prop('checked'))})),
    $('<label>', {class: 'report-by-api'})
      .text('покинувшие группу')
      .prepend(
        $('<input>', {type: 'checkbox', id: 'excluded-reports-switch', checked: false})
          .click(function() {$('.user-excluded').toggle($(this).prop('checked'))})),
    $('<label>', {class: 'report-by-api'})
      .text('пустые отчеты')
      .prepend(
        $('<input>', {type: 'checkbox', id: 'empty-reports-switch', checked: false})
          .click(function() {$('.empty-report').toggle($(this).prop('checked'))})),
  )
  return mode_switch
}


function render_rss_generator_in_popup(subforum_id) {
  bb_alert('')
  $('#bb-alert-box').css({top: '5%', left: '5%', 'max-width': '90%', 'max-height': '90vh', 'margin-top': 0, 'margin-left': 0})
  $('#bb-alert-msg').css({'min-width': '99%', 'max-height': '90vh', 'margin': '0%'})
  target_element = $('<div>', {class: 'report-table-container'}).appendTo($('#bb-alert-msg').text(''))
  with_creds(function(krs_url, user_id, api_key, bt_key){
    draw_rss_generator(target_element, subforum_id, krs_url, user_id, api_key, bt_key)
  })
}


function draw_rss_generator(target_element, subforum_id, krs_url, user_id, api_key, bt_key) {
  target_element.children().remove()

  // Create the main container and split it into two parts: settings and RSS link generation
  const container = $('<div>', {class: 'rss-container', css: {'display': 'flex', 'gap': '20px', 'width': '70vw'}}).appendTo(target_element);

  // Create the settings section (left side)
  const checkboxes_element = $('<div>', {class: 'rss-checkboxes', css: {'text-align': 'left', 'display': 'inline-block', 'flex': '1'}}).appendTo(container);

  // Create a vertical separator
  $('<div>', {class: 'vertical-separator', css: {'width': '1px', 'background-color': '#c3cbd1', 'margin': '0 10px'}}).appendTo(container);

  // Create the RSS link generation section (right side)
  const rss_link_section = $('<div>', {class: 'rss-link-section', css: {'flex': '3'}}).appendTo(container);

  const inputCss = {'margin-left': '10px'};

  function toggleInputState() {
    const label = $(this).parent();
    const input = $(this).next('input');
    input.prop('disabled', !this.checked);
    label.css('color', this.checked ? '' : 'grey');
    compileRSSLink();
  }

  function initializeInputState(label, checkbox, input) {
    input.prop('disabled', !checkbox.prop('checked'));
    label.css('color', checkbox.prop('checked') ? '' : 'grey');
  }

  function saveState() {
    const radioGroups = {};
    checkboxes_element.find('input').each(function() {
      const name = $(this).attr('name') || $(this).attr('type') + '_' + $(this).index();
      if ($(this).attr('type') === 'radio') {
        if ($(this).is(':checked')) {
          radioGroups[name] = $(this).val();
        }
      } else if ($(this).attr('type') === 'checkbox') {
        set_value(name, $(this).is(':checked'));
      } else {
        set_value(name, $(this).val());
      }
    });
    // Save all radio group values
    for (const groupName in radioGroups) {
      set_value(groupName, radioGroups[groupName]);
    }
  }

  function loadState() {
    checkboxes_element.find('input').each(function() {
      if ($(this).attr('type') === 'button') {
        return; // Skip buttons when loading state
      }
      const name = $(this).attr('name') || $(this).attr('type') + '_' + $(this).index();
      const savedValue = get_value(name);
      if (savedValue === null) {
        return
      }
      if ($(this).attr('type') === 'radio') {
        $(this).prop('checked', savedValue == $(this).val() && savedValue !== null);
      } else if ($(this).attr('type') === 'checkbox') {
        $(this).prop('checked', savedValue === true);
      } else {
        if (savedValue === '' && $(this).attr('type') === 'number') {
          return
        }
        $(this).val(savedValue);
      }
      $(this).change();
    });
  }

  function compileRSSLink() {
    const params = {};
    checkboxes_element.find('input').each(function() {
      const name = $(this).attr('name');
      if (!name) return;

      if ($(this).attr('type') === 'radio') {
        if ($(this).is(':checked')) {
          params[name] = !!Number($(this).val());
        }
      } else if ($(this).attr('type') === 'checkbox') {
        if ($(this).is(':checked')) {
          params[name] = true;
        }
      } else {
        if (!$(this).prop('disabled')) {
          params[name] = $(this).val();
        }
      }
    });

    // Compile the URL using the parameters
    var url_params = {}

    url_params['exclude_low_prio'] = params['exclude_low_prio']
    url_params['exclude_self_kept'] = !params['self_kept']

    if (params['max_keepers_enabled'])        url_params['max_keepers'] = params['max_keepers']
    if (params['max_average_seeds_enabled'])  url_params['max_average_seeds'] = params['max_average_seeds']
    if (params['max_seeders_enabled'])        url_params['max_seeders'] = params['max_seeders']
    if (params['min_days_old_enabled'])       url_params['min_days_old'] = params['min_days_old']
    if (params['exact_keeper_id_enabled'])    url_params['exact_keeper_id'] = params['exact_keeper_id']

    url_params['user_id'] = user_id
    url_params['api_key'] = api_key
    url_params['bt_key'] = bt_key

    var url = (params['custom_krs_url_enabled']) ? params['custom_krs_url'] : krs_url

    const queryString = Object.keys(url_params).map(
      key => `${encodeURIComponent(key)}=${encodeURIComponent(url_params[key])}`).join('&');
    const rssUrl = `${url}/rss/subforum/${subforum_id}?${queryString}`;
    rssLinkInput.val(rssUrl);
  }

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
        .append('Выдавать уже хранимое мной: ')
      .append($('<label>').text('да').prepend(
        $('<input>', {type: 'radio', name: 'self_kept', value: 1, change: compileRSSLink})
      ))
      .append($('<label>').css(inputCss).text('нет').prepend(
        $('<input>', {type: 'radio', name: 'self_kept', value: 0, checked: true, change: compileRSSLink})
      ))
  ).appendTo(checkboxes_element);

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
      .append('Исключить низкий приоритет: ')
      .append($('<label>').text('да').prepend(
        $('<input>', {type: 'radio', name: 'exclude_low_prio', value: 1, checked: true, change: compileRSSLink})
      ))
      .append($('<label>').css(inputCss).text('нет').prepend(
        $('<input>', {type: 'radio', name: 'exclude_low_prio', value: 0, change: compileRSSLink})
      ))
  ).appendTo(checkboxes_element);

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
      .append($('<input>', {type: 'checkbox', name: 'max_keepers_enabled', change: toggleInputState}))
      .append('Макс. количество хранителей')
      .append($('<input>', {
        name: 'max_keepers',
        type: 'number', value: 3, min: 0, max: 99, css: inputCss, disabled: true, change: compileRSSLink
      }))
  ).appendTo(checkboxes_element);
  initializeInputState(checkboxes_element.find('label.optional-label').last(), 
                      checkboxes_element.find('input[type="checkbox"]').last(), 
                      checkboxes_element.find('input[type="number"]').last());

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
      .append($('<input>', {type: 'checkbox', name: 'max_average_seeds_enabled', change: toggleInputState}))
      .append('Макс. средний сид')
      .append($('<input>', {
        name: 'max_average_seeds',
        type: 'number', value: 5.0, min: 0.0, max: 99.9, step: 0.5, css: inputCss, disabled: true, change: compileRSSLink
      }))
  ).appendTo(checkboxes_element);
  initializeInputState(checkboxes_element.find('label.optional-label').last(), 
                      checkboxes_element.find('input[type="checkbox"]').last(), 
                      checkboxes_element.find('input[type="number"]').last());

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
      .append($('<input>', {type: 'checkbox', name: 'max_seeders_enabled', change: toggleInputState}))
      .append('Макс. сиды')
      .append($('<input>', {
        name: 'max_seeders',
        type: 'number', value: 10, min: 0, max: 99, css: inputCss, disabled: true, change: compileRSSLink
      }))
  ).appendTo(checkboxes_element);
  initializeInputState(checkboxes_element.find('label.optional-label').last(), 
                      checkboxes_element.find('input[type="checkbox"]').last(), 
                      checkboxes_element.find('input[type="number"]').last());

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
      .append($('<input>', {type: 'checkbox', name: 'min_days_old_enabled', change: toggleInputState}))
      .append('Мин. дней с регистрации')
      .append($('<input>', {
        name: 'min_days_old',
        type: 'number', value: 3, min: 0, max: 365, css: inputCss, disabled: true, change: compileRSSLink
      }))
  ).appendTo(checkboxes_element);
  initializeInputState(checkboxes_element.find('label.optional-label').last(), 
                      checkboxes_element.find('input[type="checkbox"]').last(), 
                      checkboxes_element.find('input[type="number"]').last());

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
      .append($('<input>', {type: 'checkbox', name: 'exact_keeper_id_enabled', change: toggleInputState}))
      .append('Только если есть у хранителя (ID)')
      .append($('<input>', {
        name: 'exact_keeper_id',
        type: 'text', css: inputCss, disabled: true, change: compileRSSLink
      }))
  ).appendTo(checkboxes_element);
  initializeInputState(checkboxes_element.find('label.optional-label').last(), 
                      checkboxes_element.find('input[type="checkbox"]').last(), 
                      checkboxes_element.find('input[type="text"]').last());

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {class: 'optional-label'})
      .append($('<input>', {type: 'checkbox', name: 'custom_krs_url_enabled', change: toggleInputState}))
      .append('Свой домен API')
      .append($('<input>', {
        name: 'custom_krs_url',
        type: 'text', css: inputCss, disabled: true, value: krs_url, change: compileRSSLink
      }))
  ).appendTo(checkboxes_element);
  initializeInputState(checkboxes_element.find('label.optional-label').last(), 
                      checkboxes_element.find('input[type="checkbox"]').last(), 
                      checkboxes_element.find('input[type="text"]').last());

  $('<div>', {class: 'form-group'}).append(
    $('<input>', {
      value: 'Сохранить установки',
      type: 'button',
      click: function() {
        saveState();
        messageLabel.text('Установки сохранены');
        setTimeout(function() {
          messageLabel.text('');
        }, 3000);
      },
      css: {'margin-top': '10px', 'display': 'block'}
    })
  ).appendTo(checkboxes_element);

  $('<div>', {class: 'form-group'}).append(
    $('<label>', {text: 'Сгенерированная RSS ссылка:', css: {'display': 'block', 'margin-bottom': '5px'}})
  ).appendTo(rss_link_section);
  const rssLinkInput = $('<textarea>', {
    readonly: true,
    css: {'width': '100%', 'height': '80px', 'margin-bottom': '10px', 'resize': 'none'}
  }).appendTo(rss_link_section);

  $('<div>', {
    html: 'Вставьте эту ссылку в RSS ленту вашего торрент-клиента для авто-загрузки раздач и/или сведений о них.'
          + '<br>Важно: эта ссылка содержит ваш секретный ключ и <b>предназначена только для вас</b>!',
    css: {'font-size': '11px', 'color': '#333', 'display': 'block', 'margin-bottom': '10px'}
  }).appendTo(rss_link_section);

  $('<input>', {
    value: 'Скопировать',
    type: 'button',
    click: function() {
      rssLinkInput.select();
      document.execCommand('copy');
      messageLabel.text('Ссылка скопирована в буфер обмена');
      setTimeout(function() {
        messageLabel.text('');
      }, 3000);
    },
    css: {'display': 'block', 'margin-bottom': '10px'}
  }).appendTo(rss_link_section);

  // Add a message label for showing feedback
  const messageLabel = $('<div>', {class: 'message-label', css: {'margin-bottom': '10px', 'color': '#007bff'}}).appendTo(rss_link_section);

  // Load by default
  loadState()
  compileRSSLink()
}


function init_subforum_report_page_elements() {
  var self_user_id = GM_getValue('user_id')
  subforum_id = $('a[href^="tracker.php?f="]:first').attr('href').split('f=')[1].split('&')[0]
  if (!subforum_id) {
    console.log('Subforum ID is not found on the page!')
    return
  }
  anchor_post = $('#topic_main tbody.row1:first')

  anchor_post.find('.post_body').prepend(
    $('<span>', {style: 'float: right'}).append(
      $('<img src="https://static.rutracker.cc/templates/v1/images/feed_1.png" alt="feed">'),
      $('<a href="#" style="font-size: 18px; margin: 3px; color: chocolate;">RSS генератор</a>'),
    ).click(function(){render_rss_generator_in_popup(subforum_id)})
  )

  var old_fashion_reports = anchor_post.nextAll('tbody').add('#post-msg-form').add('#pagination').addClass('old-fashion-reports')
  var mode_switch = make_mode_switch().insertAfter(anchor_post)
  anchor_post = mode_switch

  if (!$('#kept-subforum-changes').length) {
    init_draw_kept_subforum_changes_spoiler($('.signature:first'), [subforum_id], null, 'subforum')
  }

  old_fashion_reports.hide()

  do_ajax_gm(
    'https://api.rutracker.cc/v1/static/keepers_user_data',
    function() {
      var keepers_user_data = JSON.parse(this.responseText)['result']
      api_request(
        `/subforum/${subforum_id}/list_keepers`,
        {only_subforums_marked_as_kept: only_subforums_marked_as_kept, include_count: false},
        function(response) {
          kept_entries = column_response_to_objects(response)
          var loaded_count_items = 0
          var messages_to_show = []
          var self_message = null
          for (let kept_entry of kept_entries.reverse()) {
            let message = create_post_imitation(kept_entry.keeper_id, keepers_user_data[kept_entry.keeper_id])
            if (kept_entry.keeper_id == self_user_id) {
              self_message = message
            } else {
              messages_to_show.push(message)
            }
            api_request(
              `/keeper/${kept_entry.keeper_id}/subforum/${subforum_id}/info`,
              {},
              function(count_response) {
                let entry = column_response_to_objects(count_response)[0]
                initialize_report_post(message, entry)
                $('#f-load .loading-1').text(`загружается ${++loaded_count_items}/${kept_entries.length}`)
                if (loaded_count_items == kept_entries.length) {
                  $('#f-load .loading-1').text('готово.')
                  messages_to_show.sort((a, b) =>
                    a.find('.post_head').attr('first_update_time') - b.find('.post_head').attr('first_update_time'))
                  if (self_message) {
                    messages_to_show.unshift(self_message)
                  }

                  var post_imitation_count = 1
                  for (message of messages_to_show) {
                    var post_row = (post_imitation_count++) % 2 + 1
                    message.addClass(`row${post_row}`)
                  }
                  anchor_post.after(...messages_to_show)

                  old_fashion_reports.hide()
                  if (window.location.hash.startsWith('#report-')) {
                    $('html, body').animate({scrollTop: $(window.location.hash).offset().top}, 300);
                    $(window.location.hash).addClass('scrolled-to-post')
                  }

                  $('#f-load').hide()
                  toggle_all_user_type_posts()
                }
              }
            )
          }
          if (!kept_entries.length) {
            $('#f-load .loading-1').text('В API пусто.').removeClass('loading-1')
          }
        }
      )
    },
    'application/json', 'GET',
  )
}


function init_category_curator_page_elements() {
  api_request(
    `/proxy_api/v1%2Fstatic%2Fcat_forum_tree`, {},
    function(response) {
      const forumId = document.querySelector('span.p-color span.post-align a')?.getAttribute('href').split('=')[1];

      var subforum_ids = [
        ...Object.keys(response.result.tree[forumId])?.map(id => Number(id)),
        ...Object.values(response.result.tree[forumId])?.flat()
      ];
      init_draw_kept_subforum_changes_spoiler(
        $('table#topic_main .row1:first .post_body span:first'), subforum_ids, null, null, true)
    }
  )
}


function check_keeper_in_group_and_init_profile_page_elements() {
  api_request(
    `/proxy_api/v1%2Fstatic%2Fkeepers_user_data`, {},
    function(response) {
      var target_user_id = $('#profile-uname').attr('data-uid')
      if (target_user_id in response['result']) {
        init_profile_page_elements()
      } else {
        $('#user-contacts tbody').append(
          `<tr id="api-reports-init-button"><th></th>
          <td><a href="#">Показать отчеты хранимого</a></td></tr>`)
        .click(init_profile_page_elements)
      }
    }
  )
}


(function() {
  'use strict';
  if ($('script[src$="/report_lens.js"]').length) {
    console.log('KP feature of the web-site is enabled. Terminating the user-script.')
    return
  }

  if ($('.dl-topic').length && $('td:contains("Приоритет хранения:")').length) {
    init_release_page_elements();
  } else if (window.location.href.includes('viewforum.php?f=')) {
    init_subforum_page_elements();
  } else if (
      window.location.href.includes('/forum/profile.php?mode=viewprofile')
  ) {
    if ($('th:contains("Хранительские ключи:")').length) {
      init_profile_page_elements();
    } else {
      check_keeper_in_group_and_init_profile_page_elements()
    }
  } else if ($('#topic-title').text().startsWith('[Список]')) {
    draw_initialization_button();
    init_subforum_report_page_elements();
  } else if ($('#topic-title').text().includes('] Подразделы форума и их хранители')) {
    init_category_curator_page_elements();
  } else if (window.location.href.includes('viewtopic.php?')) {
    init_usual_topic_page_elements();
  }
})();
