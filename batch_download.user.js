// ==UserScript==
// @name         RuTracker.org Batch torrent downloader
// @version      0.2.1
// @description  Batch download all torrents on page (forum view, tracker search, unregistered releases, red book)
// @description:ru Массовая закачка .torrent файлов со страницы подраздела, поиска по трекеру, перечня разрегистрированных раздач, красной книги
// @author       Keepers Team
// @namespace    https://gitlab.com/qusielle/keeper-userscripts
// @updateURL    https://gitlab.com/qusielle/keeper-userscripts/-/raw/master/batch_download.user.js
// @supportURL   https://rutracker.org/forum/viewtopic.php?t=5680754
// @include      https://rutracker.*/forum/viewforum.php?f=*
// @include      https://rutracker.*/forum/tracker.php*
// @include      https://rutracker.*/forum/topics.php?page=unregistered_torrents*
// @include      https://rutracker.*/forum/viewtopic.php?t=6579518*
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.8/FileSaver.min.js
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

var FORBIDDEN_STATUSES = '!Dx∑∏'

this.$ = this.jQuery = jQuery.noConflict(true);
var zip_file, count, files;
var zip = new JSZip();
var start_from_page_url = ''
var suitable_releases_count = 0

// Work with cookies -----------
function setCookie(name,value,days=90) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
function eraseCookie(name) {
  document.cookie = name+'=; Max-Age=-99999999;';
}

function set_value(name, value) {
  GM_setValue(name, value);
}

function get_value(name, default_value=null) {
  return GM_getValue(name, default_value);
}
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function do_ajax(url, event_handler, responseType) {
  var handler_wrapper = function() {
    if (this.readyState == XMLHttpRequest.DONE) {
      event_handler.call(this);
    }
  }
  var req = new XMLHttpRequest();
  req.onreadystatechange = handler_wrapper;
  req.responseType = responseType;
  req.open('POST', url, true);
  req.send();
}

function add_torrent(url, number){
  event_handler = function() {
    count++;
    $('#unreg-down').val('Загрузка… ' + (number - count));
    var tor_id = url.split('=')[1];

    resp_text = (new TextDecoder('windows-1251')).decode(this.response);

    if (this.status == 200 && !resp_text.startsWith('<center>')) {
        console.log('Success: ' + tor_id);
        files++;
        zip.file(tor_id + '.torrent', this.response, {binary: true});
        if ($('#unreg-down-remember-checkbox').is(':checked')) {
          var textarea = $('#unreg-down-textarea');
          textarea.val(textarea.val() + ' +' + tor_id);
          if (!is_red_book_page()) {
            set_ignored_checkboxes_normal()
          }
          set_value('unreg-down-ignored', textarea.val());
        }
    } else {
      console.log('Fail: ' + tor_id + ' ' + this.status + ' ' + resp_text);
      $('tr[data-topic_id="unreg-down-' + tor_id + '"] .vf-col-t-title')
      .append(document.createTextNode(
          tor_id + ': Error - ' + this.status + ' ' + resp_text.replace(/(<.+?>)+/g, ' ') + '\n'
      ));
    }

    if (count == number) {
      save_zip()
      if (is_red_book_page()) {
        set_ignored_links_red_book(true)
      }
    }
  }
  do_ajax(url, event_handler, 'arraybuffer');
}

function save_zip(){
  var add = '';
  var file_name = $('#title-search').attr('value') || 'torrents';
  var page = $('.bottom_info > .nav > p:eq(1) > b').text();
  if (page) add = ' #' + page;
  zip.generateAsync({type:'blob'}).then(function(zip_file) {
    saveAs(zip_file, file_name + add + ' [' + files + '].zip');
    $('#unreg-down').attr('disabled', false);
    show_selected_amount();
  });
  $('#unreg-down').text('Генерация архива...');
}

function set_ignored_checkboxes_normal() {
  ignore_checkbox_state = $('#unreg-down-ignore-checkbox').is(':checked');
  priority_select_state = $('#unreg-down-priorities').val();
  self_seeding_select_state = $('#unreg-down-self-seeding').val();
  all_ignored_ids = $('#unreg-down-textarea').val().match(/\d+/g) || '';

  $('.unreg-down-tor-checkbox').each(function() {
    if (
        (ignore_checkbox_state && all_ignored_ids.indexOf($(this).attr('id').replace('unreg-down-', '')) > -1)
        || ($($(this).parents()[1]).find('img[src*="lowest.svg"]').length && priority_select_state == 'no-low')
        || (!$($(this).parents()[1]).find('img[src*="highest.svg"]').length && priority_select_state == 'high-only')
        ||  ($($(this).parents()[1]).find('img.user-is-seeding-icon').length && self_seeding_select_state == 'without-self-seeding')
        || (!$($(this).parents()[1]).find('img.user-is-seeding-icon').length && self_seeding_select_state == 'with-self-seeding')
    )
      $(this).attr("disabled", true).attr('checked', false);
    else
      $(this).attr("disabled", false);
  });
  show_selected_amount();
}

function set_ignored_links_red_book(only_selected = false) {
  ignore_checkbox_state = $('#unreg-down-ignore-checkbox').is(':checked');
  priority_select_state = $('#unreg-down-priorities').val();
  self_seeding_select_state = $('#unreg-down-self-seeding').val();
  all_ignored_ids = $('#unreg-down-textarea').val().match(/\d+/g) || '';

  let checked_selector = (only_selected) ? ':checked' : ''
  $(`.unreg-down-tor-checkbox${checked_selector}`).each(function(i, el){
    let data_release_count = $(el).data('release_count')
    if (data_release_count) {
      $(this).parent().find('a[href^="viewtopic.php?t="]').each(function() {
        let topic_id = $(this).attr('href').split('=')[1]
        if (
            (ignore_checkbox_state && all_ignored_ids.indexOf(topic_id) > -1)
        )
          $(this).attr("disabled", true).css('color', 'gray');
        else
          $(this).attr("disabled", false).css('color', '');
      });
    }
  });
  show_selected_amount();
}

function set_ignored_checkboxes() {
  if (is_red_book_page()) {
    set_ignored_links_red_book()
  } else {
    set_ignored_checkboxes_normal()
  }
}

function load_all_next_pages(e) {
  e.preventDefault();
  var topics_table_selector = '.vf-table.vf-gen,#tor-tbl,.vf-table.vf-tor,.forumline.topics-list.with-torrent';
  var pages_count = $('a.pg:contains("След.")').prev().first().text()
  var should_stop = false;

  if (!pages_count) {
    $('#unreg-down-load-next-pages-status').text('Нечего загружать!');
    return;
  }

  function stop_load() {
    should_stop = true;
  }

  $('#unreg-down-load-next-pages').val('Остановить').off('click').click(stop_load);

  function set_current_page_number(page_text) {
    current_page_number = $('.nav p b', page_text).last().text();
    $('#unreg-down-load-next-pages-status').text('['+ current_page_number + '/' + pages_count + '] Загрузка…');
  }

  function load_next_page(current_page_source) {
    if (start_from_page_url) {
      next_page_url = start_from_page_url;
      start_from_page_url = '';
    } else {
      // Extract `next_page_url` only when it is not saved from a previous run.
      try {
        var next_page_url = $('a.pg:contains("След.")', current_page_source).attr('href')
      } catch (error) {
        var next_page_url = ''
      }
    }

    if (!should_stop && next_page_url) {
      console.log("Load " + next_page_url)
      do_ajax(next_page_url, loaded_page_handler, 'text')
    } else {
      $('#unreg-down-load-next-pages-status').text('Страницы подгружены.');
      $('#unreg-down-load-next-pages').off('click').click(load_all_next_pages);
      if (next_page_url) {
        $('#unreg-down-load-next-pages').val('Догрузить');
        start_from_page_url = next_page_url;
      } else {
        $('#unreg-down-load-next-pages').val('Все загружено.').attr("disabled", true);
      }
    }
  }

  function loaded_page_handler() {
    if (this.status == 404) {
      console.log('Returned 404: ' + this.responseText);
      load_next_page(this.responseText);
    }
    let topics_table = $(topics_table_selector, this.responseText);
    $('.row2.med.pad_4.tCenter', topics_table).remove();
    $('th.vf-col-t-title,th:contains("Тема")', topics_table)
      .text($('.nav p:contains("Страница"),.title-pagination b', this.responseText).text())
      .wrapInner("<strong />");
    $('#unreg-down-next-pages-anchor').before(topics_table);

    draw_checkboxes();
    set_ignored_checkboxes();
    set_current_page_number(this.responseText);
    load_next_page(this.responseText);
  }

  if (!$('#unreg-down-next-pages-anchor').length) {
    $(topics_table_selector).after('<div id="unreg-down-next-pages-anchor"></div>');
    set_current_page_number($('body'));
  }
  load_next_page($('body'));
}

function show_selected_amount() {
  function humanFileSize(size) {
    var i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
    return (size / Math.pow(1024, i)).toFixed(2) * 1 + ['B', 'kB', 'MB', 'GB', 'TB'][i];
  }

  function get_all_size(selector) {
    let minimal_size = 0
    let size_sum = selector.map(function() {
      let data_release_size = $(this).data('release_size')
      let size
      if (data_release_size) {
        size = Number(data_release_size)
        let ignored_count = $(this).parent().find('a[disabled="disabled"]').length
        let release_count = Number($(this).data('release_count'))
        if (ignored_count == release_count) {
          size = 0
        } else if (ignored_count) {
          minimal_size += data_release_size * ((release_count - ignored_count) / release_count)
        }
      } else {
        size = Number($($(this).parents()[1]).find('.tor-size').attr('data-ts_text'))
        minimal_size += size
      }
      return size
    }).toArray().reduce((a, b) => a + b, 0)

    if (minimal_size && minimal_size != size_sum) {
      return `~${humanFileSize(minimal_size)}-${humanFileSize(size_sum)}`
    }
    return humanFileSize(size_sum)
  }

  function get_all_count(selector) {
    let count_sum = selector.map(function() {
      let data_release_count = $(this).data('release_count')
      if (data_release_count) {
        let ignored_count = $(this).parent().find('a[disabled="disabled"]').length
        return Number(data_release_count - ignored_count)
      } else {
        return 1
      }
    }).toArray().reduce((a, b) => a + b, 0)
    return count_sum
  }

  let all_available = $('.unreg-down-tor-checkbox[disabled!="disabled"]');
  let all_available_count = get_all_count(all_available)
  let all_available_size = get_all_size(all_available)
  let all_selected = $('.unreg-down-tor-checkbox:checked');
  let all_selected_count = get_all_count(all_selected)
  let all_selected_size = get_all_size(all_selected)


  var size_line = ''
  if (typeof all_available_size === 'string' || all_available_size instanceof String)
    size_line = ' : ' + all_selected_size + '/' + all_available_size

  $('#unreg-down').val(
    'Загрузить выделенное'
    + ' (' + all_selected_count + '/' + all_available_count + size_line + ')'
  );
}

function draw_checkboxes_usual(dry_run) {
  $('.hl-tr').each(function(){
    let topic_link = $(this).find('.topictitle.tt-text,.torTopic.tt-text,a.torTopic.bold');
    if (!topic_link.length) return;
    let topic_id =  topic_link.attr('href').split('t=')[1];
    if (!topic_id || $('#unreg-down-' + topic_id, $(this)).length) return;

    let prepend_text = '';
    if (!$(this).find('img[src*="icon_attach.gif"],.seedmed,.dl-stub').length) return;
    let moderation_status = $(this).find('.tor-icon').text()
    if (moderation_status && FORBIDDEN_STATUSES.includes(moderation_status)) return;

    prepend_text = '<input class="unreg-down-tor-checkbox" id="unreg-down-' + topic_id + '" value="0" type="checkbox">';
    if (!$(this).find('.tor-icon').length) return;

    if (dry_run)
      suitable_releases_count++;
    else
      $(this).find('.tor-icon').parent().prepend(prepend_text);
  })
}

function draw_checkboxes_tracker_search(dry_run) {
  $('.tCenter.hl-tr').each(function(){
    var tor_id = $(this).find('.tr-dl.dl-stub').attr('href');
    var replace_text = '';
    if (tor_id)
      replace_text = '<input class="unreg-down-tor-checkbox" id="' + tor_id.split('=')[1] + '" value="0" type="checkbox">';
    if (dry_run)
      suitable_releases_count++;
    else
      $(this).find('.row1.t-ico img,.row1.t-ico p *').replaceWith(replace_text);
  })
}

function draw_checkboxes_red_book(dry_run) {
  $('.sp-body ol li').each(function(){
    let element_text = $(this).text()
    let block_size_str = element_text.match(/^([\d\.]+)GB:/)
    let block_release_ids = element_text.match(/^[\d\.]+GB: ([\d ]+)/)
    if (!block_size_str || !block_release_ids) {
      return
    }
    block_release_ids = block_release_ids[1].trim().split(' ')

    let checkbox = $('<input>', {
      class: "unreg-down-tor-checkbox",
      // id: `multi-block_size_str[1]-`,
      value: "0",
      type: "checkbox",
      "data-release_ids": block_release_ids.join('-'),
      "data-release_count": block_release_ids.length,
      "data-release_size": Number(block_size_str[1]) * (1024 ** 3),
      "data-selected_count": 0,
    })

    if (dry_run)
      suitable_releases_count += block_release_ids.length
    else
      $(this).prepend(checkbox)
  })

  $('.sp-head').each(function() {
    let spoiler = $(this).parent()
    if (!$(this).text().startsWith('Риск порядка') || spoiler.find('> input').length) {
      return
    }
    $(this).before($('<input>', {
        class: "unreg-down-tor-checkbox-spoiler",
        value: "0",
        type: "checkbox",
        style: "margin: 5px; position: absolute;",
      })
      .click(function() {
        spoiler.find('.unreg-down-tor-checkbox').prop('checked', $(this).prop('checked'))
        set_ignored_links_red_book(true)
        show_selected_amount()
      })
    )
  })
}

function is_red_book_page() {
  return $('#topic-title').text() == 'Красная книга раздач трекера'
}

function draw_checkboxes(dry_run = false) {
  if (window.location.href.includes('tracker.php')) {
    draw_checkboxes_tracker_search(dry_run)
  } else if (is_red_book_page()){
    draw_checkboxes_red_book(dry_run)
  } else {
    draw_checkboxes_usual(dry_run)
  }

  if (!dry_run) {
    $('.unreg-down-tor-checkbox').change(show_selected_amount);
    show_selected_amount();
  }
}

function draw_controls_tracker_search() {
  $('td.row4 .fieldsets').parent().after('<td class="row4" style="padding: 4px; vertical-align:top; width: min-content;"><div id="unreg-down-button"></div><div id="unreg-down-ignored"></div><div id="unreg-down-status"></div></td>');
  $('.thHead').after('<th class="thHead">Массовая загрузка</th>');
  $('#unreg-down-button')
    .append('<input type="button" id="unreg-down" class="bold" style="width: 100%; font-family: Verdana,sans-serif; font-size: 11px;" value="Загрузить выделенное"></input></br>')
    .append('<input type="button" id="unreg-down-load-next-pages" class="bold" style="width: 100%; font-family: Verdana,sans-serif; font-size: 11px;" value="Загрузить след. страницы"></input></br>')
    .append('<span id="unreg-down-load-next-pages-status">&#8288;</span></br>')
    .append('<a id="unreg-down-select-all" class="bold" style="margin: 5px; width: 165px; font-family: Verdana,sans-serif; font-size: 11px;">Выделить все</a>')
    .append('<a id="unreg-down-unselect-all" class="bold" style="margin: 5px; width: 165px; font-family: Verdana,sans-serif; font-size: 11px;">Снять все выделение</a>')
    .append('</br><a id="unreg-down-ignore-selected" class="bold" style="white-space:nowrap; margin: 5px; width: 165px; font-family: Verdana,sans-serif; font-size: 11px;">Игнорировать выделенное</a>')
    .append('<select id="unreg-down-priorities">\
               <option value="no-low">Без низкого приоритета</option>\
               <option value="all">Все приоритеты</option>\
               <option value="high-only">Только высокий приоритет</option>\
             </select>')
    .append('<select id="unreg-down-self-seeding">\
               <option value="without-self-seeding">Без сидируемого мной</option>\
               <option value="all">Сидируемое + не сидируемое</option>\
               <option value="with-self-seeding">Только сидируемое мной</option>\
             </select>')
  $('#unreg-down-ignored').append('\
    <p class="chbox"><label><input id="unreg-down-ignore-checkbox" value="0" type="checkbox">Список игнорируемых</label>\
      <label><input id="unreg-down-remember-checkbox" value="0" type="checkbox">Запоминать загруженное</label>\
    </p><textarea id="unreg-down-textarea" rows="15"  onfocus="storeCaret(this);" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" style="display: none; width: 100%;"></textarea>')
}

function draw_controls_usual(anchor_only = false) {
  if (!$('#unreg-down-base').length) {
    $('td.w100.vBottom.pad_2').after('<div id="unreg-down-base"></div>');
  }
  if (anchor_only) return;

  $('#unreg-down-base').html('\
      <td style="padding: 4px; vertical-align:top">\
      <input type="button" id="unreg-down-load-next-pages" class="bold" style="width: 100%; font-family: Verdana,sans-serif; font-size: 11px;" value="Загрузить след. страницы"></input>\
      <span id="unreg-down-load-next-pages-status">&#8288;</span>\
      <select id="unreg-down-priorities">\
        <option value="no-low">Без низкого приоритета</option>\
        <option value="all">Все приоритеты</option>\
        <option value="high-only">Только высокий приоритет</option>\
      </select>\
      <select id="unreg-down-self-seeding">\
        <option value="without-self-seeding">Без сидируемого мной</option>\
        <option value="all">Сидируемое + не сидируемое</option>\
        <option value="with-self-seeding">Только сидируемое мной</option>\
      </select>\
      </td>\
      <td style="padding: 4px; vertical-align:top"><div id="unreg-down-button">\
      <input type="button" id="unreg-down" class="bold" style="width: 100%; font-family: Verdana,sans-serif; font-size: 11px;" value="Загрузить выделенное"></input></br>\
        <a id="unreg-down-select-all" class="bold" style="white-space:nowrap; margin: 5px; width: 165px; font-family: Verdana,sans-serif; font-size: 11px;">Выделить все</a> \
        <a id="unreg-down-unselect-all" class="bold" style="white-space:nowrap; margin: 5px; width: 165px; font-family: Verdana,sans-serif; font-size: 11px;">Снять все выделение</a> \
        <a id="unreg-down-ignore-selected" class="bold" style="white-space:nowrap; margin: 5px; width: 165px; font-family: Verdana,sans-serif; font-size: 11px;">Игнорировать выделенное</a> \
        <p class="chbox"><label><input id="unreg-down-ignore-checkbox" value="0" type="checkbox">Список игнорируемых</label><label><input id="unreg-down-remember-checkbox" value="0" type="checkbox">Запоминать загруженное</label></p> \
      </div><div id="unreg-down-status"></div></td>\
      <td style="padding: 4px; vertical-align:top"><div id="unreg-down-ignored">\
        <textarea id="unreg-down-textarea" rows="5"  onfocus="storeCaret(this);" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" style="display: none;"></textarea>\
      </div></td>\
  ');
}

function draw_controls_unregistered_search(anchor_only = false) {
  if (!$('.w100.vBottom.pad_2').length) {
    $('div.title-block').wrap('<table class="w100"><tr><td class="w100 vBottom pad_2"></td></tr></table>')
  }
  draw_controls_usual(anchor_only)
}

function activate_controls() {
  $('#unreg-down-select-all').click(function(e){e.preventDefault(); $('.unreg-down-tor-checkbox:not([disabled]),.unreg-down-tor-checkbox-spoiler').prop('checked', true); show_selected_amount();});
  $('#unreg-down-unselect-all').click(function(e){e.preventDefault(); $('.unreg-down-tor-checkbox:not([disabled]),.unreg-down-tor-checkbox-spoiler').prop('checked', false); show_selected_amount();});

  $('#unreg-down-textarea').val(get_value('unreg-down-ignored')).focusout(function(){set_value('unreg-down-ignored', $(this).val()); set_ignored_checkboxes();});
  $('#unreg-down-priorities').val(getCookie('unreg-down-priorities-val') || 'no-low');
  $('#unreg-down-self-seeding').val(getCookie('unreg-down-self-seeding-val') || 'all');
  $('#unreg-down-ignore-selected').click(function(e){
    e.preventDefault();
    var textarea = $('#unreg-down-textarea');
    $('.unreg-down-tor-checkbox:checked').each(function() {
      textarea.val(textarea.val() + ' -' + this.id.replace('unreg-down-', ''));
    });
    set_value('unreg-down-ignored', textarea.val());
    set_ignored_checkboxes();
    show_selected_amount();
  });
  $('#unreg-down-ignore-checkbox,#unreg-down-priorities,#unreg-down-self-seeding').change(function(){
    setCookie('unreg-down-ignore-enabled', $('#unreg-down-ignore-checkbox').is(':checked') | 0);
    setCookie('unreg-down-priorities-val', $('#unreg-down-priorities').val());
    setCookie('unreg-down-self-seeding-val', $('#unreg-down-self-seeding').val());
    if ($('#unreg-down-ignore-checkbox').is(':checked')) {
      $('#unreg-down-textarea').show();
    } else {
      $('#unreg-down-textarea').hide();
    }
    set_ignored_checkboxes();
    show_selected_amount();
  });
  $('#unreg-down-remember-checkbox').change(function(){
    setCookie('unreg-down-remember-enabled', $('#unreg-down-remember-checkbox').is(':checked') | 0);
  });

  if (getCookie('unreg-down-ignore-enabled') == '1')
    $('#unreg-down-ignore-checkbox').click();

  if (getCookie('unreg-down-remember-enabled') == '1')
    $('#unreg-down-remember-checkbox').click();

  $('#unreg-down').click(function(e){
    e.preventDefault();
    count = 0;
    files = 0;
    let number = $('.unreg-down-tor-checkbox:checked').map(function() {
      let data_release_count = $(this).data('release_count')
      if (data_release_count) {
        let ignored_count = $(this).parent().find('a[disabled="disabled"]').length
        return Number(data_release_count - ignored_count)
      } else {
        return 1
      }
    }).toArray().reduce((a, b) => a + b, 0)

    if (number) {
      $('#unreg-down').attr('disabled', true);
      $('.unreg-down-tor-checkbox:checked').each(function(i, el){
        let topic_ids
        let data_release_count = $(this).data('release_count')
        if (data_release_count) {
          topic_ids = $(this).parent().find('a[disabled!="disabled"][href^="viewtopic.php?t="]').map(function() {
            return $(this).attr('href').split('=')[1]
          }).toArray()
        } else {
          topic_ids = [$(el).attr('id').replace('unreg-down-', '')]
        }
        for (const topic_id of topic_ids) {
          setTimeout(function(){
            let url = `dl.php?t=${topic_id}`;
            console.log('url: ' + url);
            add_torrent(url, number);
          }, 100 + ( i * 100 ));
        }
      });
    } else {
      $('#unreg-down').val('Нечего загружать!');
      setTimeout(show_selected_amount, 1000);
    }
  });

  if (is_red_book_page()) {
    $('#unreg-down-priorities').hide()
    $('#unreg-down-self-seeding').hide()
    $('#unreg-down-ignore-selected').hide()
    $('#unreg-down-load-next-pages').hide()
  }

  $('#unreg-down-load-next-pages').click(load_all_next_pages);

  show_selected_amount();
}


function draw_all(anchor_only = false) {
  if (window.location.href.includes('tracker.php')) {
    draw_controls_tracker_search()
  } else if (window.location.href.includes('page=unregistered_torrents')) {
    draw_controls_unregistered_search(anchor_only)
  } else {
    draw_controls_usual(anchor_only)
  }

  draw_checkboxes(anchor_only)
  activate_controls()
}


function page_has_suitable_releases() {
  suitable_releases_count = 0
  draw_checkboxes(true)
  return Boolean(suitable_releases_count)
}


if (page_has_suitable_releases()) {
  draw_all()
} else {
  draw_all(true)
  $('#unreg-down-base').html('\
    <input type="button" class="bold" style="width: 100%; font-size: 11px;" value="Активировать кнопки загрузки"></input>\
  ')
  $('#unreg-down-base input').click(function(){draw_all(false)});
}
